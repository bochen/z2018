#!/bin/bash

#change this if you think TOTAL_CORES is not correct
export TOTAL_CORES=`grep -c ^processor /proc/cpuinfo`
echo "The node has $TOTAL_CORES cores"
export GAM_CORES=6
export LGB_CORES=16

export ZILLOW_HOME="$( cd "$(dirname "$0")/.." ; pwd -P )"
echo use zillow home: $ZILLOW_HOME

export PYTHONPATH=$ZILLOW_HOME/python

source activate python27

cd $ZILLOW_HOME/python
python step0a_unzip_data.py || exit -1
python step0b_install_lgb.py || exit -1
python step1_make_parquet_data.py || exit -1
python step2_split_state_data.py || exit -1

if [ -e  $ZILLOW_HOME/input/raw_input/home_attributes_history ];then 
 echo "delete raw files"
 rm -fr $ZILLOW_HOME/input/raw_input/home_attributes_history 
 #rm -fr $ZILLOW_HOME/input/raw_input/saleprice 
 rm -fr $ZILLOW_HOME/input/raw_input/saleprice_2007-01-01to2018-06-30_v4.csv
 rm -fr $ZILLOW_HOME/input/raw_input/saleprice_with_buyer_seller_info_2007-01-01to2018-06-30_v4.json
 #rm -fr $ZILLOW_HOME/input/raw_input/tax_history_2007to2017_v2.csv
 rm -fr $ZILLOW_HOME/input/raw_input/tax_history_2007to2018_v4.csv
fi 
COUNT=$(ls $ZILLOW_HOME/input/parquet/|wc -l)
if [ $COUNT -gt 0 ];then
 echo "delete parquets"
  rm -fr $ZILLOW_HOME/input/parquet/*
fi

python step3_make_features_geo.py || exit -1
python step3_make_features_geo2.py || exit -1
python step3_make_features_geo3.py || exit -1

python step4_make_features_tax.py || exit -1

python step5_make_sale_price.py || exit -1

python step6_make_attrib_features.py || exit -1

python step7_salesratio_adjust_value.py || exit -1

python step8_make_tf_input.py || exit -1

python step9_run_something.py || exit -1

python step10_run_gam.py || exit -1

python step11_make_lgb_input.py || exit -1

python step12_make_lgb_input_ratio.py || exit -1

python step13_make_local_price.py || exit -1

python step14_make_local_price_feature.py || exit -1

python step15_make_local_price_hist.py || exit -1

python step16_run_lgb.py || exit -1

python step17_make_lgb_predict_hist.py  || exit -1

python step20_lgb_chain.py  || exit -1

python step21_merge_lgb_chain_predict.py  || exit -1

python step30_run_lgbraw.py  || exit -1

#python step40_run_lgb_final.py  || exit -1
timeout -s KILL 3d python step40_run_lgb_final.py

rc=$?; if [[ $rc != 0 ]]; then echo "Warning! step40_run_lgb_final.py was killed. If too many tasks failed, reason need to be found and re-run it"; fi

n=`jupyter kernelspec list |grep -c conda-env-python27-py`
if [ $n -lt 1 ];then
  python -m ipykernel install --user --name conda-env-python27-py
fi

cd  $ZILLOW_HOME/3_jupyter
python $ZILLOW_HOME/python/step50_make_submissions.py || exit -1

source deactivate python27 

echo "your submission files is in $ZILLOW_HOME/input"
