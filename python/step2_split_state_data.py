'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils
import dask.dataframe as dd 
import numpy as np 
import os
import logging
from zillow.task import Task
import fastparquet
import gc
import cPickle as pickle 
 

def make_parcel_state_mapping():
    def _fun_(logger):
        
        filename = config.LATEST_HOME_ATTRIB_PARQ_FB_FILE
        logger.info("Start make_parcel_state_mapping with %s", filename)
        assert utils.file_exists(filename), filename 
        df = dd.read_parquet(filename, columns=['rawcensustractandblock']).compute()
        df['rawcensustractandblock'] = df['rawcensustractandblock'].map(lambda u: u.replace(".", ""))
        df['state'] = df['rawcensustractandblock'].map(lambda u: u[:2])
	print set(df['state'])
	#assert df['state'].isin(set(config.STATES)).all()
        mapping = df['state'].to_dict()
        output_filename = config.PARCEL_STATE_MAPPING_FILE
        pickle.dump(mapping, open(output_filename, 'wb'))
        logger.info("End make_parcel_state_mapping with %s records", len(mapping))
    task = Task('make_parcel_state_mapping', _fun_)
    task()
    
def split_home_attrib():
    def _fun_(year, logger):
        filename = config.get_home_attrib_fb(year)
        logger.info("Start split_home_attrib with %s", filename)
        assert utils.file_exists(filename), filename 
        df = dd.read_parquet(filename).compute()
        df['rawcensustractandblock'] = df['rawcensustractandblock'].map(lambda u: u.replace(".", ""))
        df['state'] = df['rawcensustractandblock'].map(lambda u: u[:2])
	print set(df['state'])
	#assert df['state'].isin(set(config.STATES)).all()
	num=0
        for state in config.STATES:
            subdf = df[df['state'] == state]
            num+=subdf.shape[0]
            output_filename = os.path.join(config.get_state_year_folder(state, year), "home_attrib.parq")
            logger.info("write %s, #records=%s", output_filename, len(subdf))
            fastparquet.write (output_filename , subdf, compression='SNAPPY')
        #assert num==df.shape[0]
        logger.info("End split_home_attrib")
    
    for year in config.YEARS:
        task = Task('split_home_attrib'+"-"+str(year), lambda u: _fun_(year, u))
        task()
        
def split_sale_price():
    def _fun_(logger):
        filename = config.SALESPRICE_PARQ_FILE
        logger.info("Start split_sale_price with %s", filename)
        assert utils.file_exists(filename), filename 
        df = dd.read_parquet(filename).compute()
        state_mapping = pickle.load(open(config.PARCEL_STATE_MAPPING_FILE))
	num=0
        for state in config.STATES:
            parcelids = set([k for k, v in state_mapping.items() if v == state ])
            statedf = df[df['parcelid'].isin(parcelids)]
            for year in config.YEARS:
                subdf = statedf[statedf['transactionyear'] == year]
                num+=subdf.shape[0]
                output_filename = os.path.join(config.get_state_year_folder(state, year), "sales_price.parq")
                logger.info("write %s, #records=%s", output_filename, len(subdf))
                fastparquet.write (output_filename , subdf, compression='SNAPPY')
        #assert num==df.shape[0]
        logger.info("End split_sale_price")
    task = Task('split_sale_price', _fun_)
    task()

def split_tax_hist():
    def _fun_(logger):
        filename = config.TAXHIST_PARQ_FILE
        logger.info("Start split_tax_hist with %s", filename)
        assert utils.file_exists(filename), filename 
        df = dd.read_parquet(filename).compute()
        state_mapping = pickle.load(open(config.PARCEL_STATE_MAPPING_FILE))
        num=0
        for state in config.STATES:
            parcelids = set([k for k, v in state_mapping.items() if v == state ])
            statedf = df[df['parcelid'].isin(parcelids)]
            for year in config.YEARS:
                subdf = statedf[statedf['assessmentyear'] == year]
                num+=subdf.shape[0]
                output_filename = os.path.join(config.get_state_year_folder(state, year), "tax_hist.parq")
                logger.info("write %s, #records=%s", output_filename, len(subdf))
                fastparquet.write (output_filename , subdf, compression='SNAPPY')
        #assert num==df.shape[0]
        logger.info("End split_tax_hist")
    task = Task('split_tax_hist', _fun_)
    task()
                        
            
if __name__ == '__main__':
    make_parcel_state_mapping()
    for state in config.STATES:
        for year in config.YEARS +['share']:
            utils.create_dir_if_not_exists(config.get_state_year_folder(state, year))
    split_home_attrib()
    split_sale_price()
    split_tax_hist()
