'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils, features
import os
import sys
import glob
from multiprocessing import Pool
import hashlib
from zillow.task import Task

 
def shell_run_and_wait(command):
    command = command.split(" ")
    import subprocess
    # process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process = subprocess.Popen(command)
    process.wait()
    return process.returncode


def run_gam_single(cmd):

    def f(cmd, logger):
        logger.info(cmd)
        code = shell_run_and_wait(cmd)
        assert code == 0, (code, cmd)

    hashid = hashlib.sha1(cmd).hexdigest()
    task = Task('run_gam_single-{}'.format(hashid), lambda u: f(cmd, u))
    task()

    
def run_R_gam(logger):
    assert 'ZILLOW_HOME' in os.environ
    assert 'GAM_CORES' in os. environ 
    files = glob.glob(os.path.join(config.HOME, 'python', 'R', 'script_single') + '/*.sh')
    
    if 0: #seq run 
        for cmd in files:
            logger.info("cmd: %s", cmd)
            run_gam_single(cmd)
    else:
        assert 'TOTAL_CORES' in os. environ
        n_jobs = int(int(os.environ['TOTAL_CORES']) / int(os.environ['GAM_CORES']))
        logger.info("use %d jobs", n_jobs) 
        p = Pool(n_jobs)
        p.map(run_gam_single, files)
        p.close()
        p.join()

       
def run():
    task = Task('run_gam_single-all', lambda u: run_R_gam(u))
    task()


if __name__ == '__main__':
        run()
