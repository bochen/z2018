'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils
import os
from zillow.task import Task
import pandas as pd
import sys
import step20_a_lgb_chain
import dask.dataframe as dd 
import fastparquet
import numpy as np 
import cPickle as pickle 
 
def show_all(df):
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(df)     


param_hashes = step20_a_lgb_chain.param_hashes

 
def merge_lgb_chain_prediction(state, year, logger):
    def read_predict():
        df = []
        for i in range(1, 6):
            a = '{}_{}-{}-{}.param-f{}.parq'.format('lgbchain', state, year, param_hashes[state], i)
            fname = os.path.join(config.get_state_year_folder2(state, year), a)
            df.append(dd.read_parquet(fname).compute())
        lst = []
        for col in df[0].columns:
            s = pd.concat([u[col] for u in df], axis=1)
            s = s.mean(axis=1)
            s.name = col
            lst.append(s)
        ret = pd.concat(lst, axis=1)
        logger.info("head for year %d\n%s", year, ret.head())
        return ret
    df = read_predict()
    outfile = os.path.join(config.get_state_year_folder2(state, year), '{}_prediction_merged.parq'.format('lgbchain'))
    fastparquet.write(outfile, df, compression='SNAPPY') 
        

def make_lgb_chain_none_outliers(state,year,logger):
    outfile = os.path.join(config.get_state_year_folder2(state, year), '{}_prediction_merged.parq'.format('lgbchain'))
    df=dd.read_parquet(outfile).compute()['error'].dropna()
    df=np.abs(df)
    logger.info("nonoutlier ratio: %s", np.mean(df<0.4))
    df=df[df<0.4]
    logger.info("nonoutlier count: %s", len(df))
    outfile = os.path.join(config.get_state_year_folder2(state, year), '{}_nonoutlier.pkl'.format('lgbchain'))
    pickle.dump( set(df.index),open(outfile,'wb'))
                                                           
if __name__ == '__main__':
    for year in config.YEARS:
        for state in config.STATES:
            task = Task('merge_lgb_chain_prediction-{}-{}'.format(state, year), lambda u: merge_lgb_chain_prediction(state, year , u))
            task()

    for year in config.YEARS:
        for state in config.STATES:
            task = Task('make_lgb_chain_none_outliers-{}-{}'.format(state, year), lambda u: make_lgb_chain_none_outliers(state, year , u))
            task()
