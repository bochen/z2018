'''
Created on Apr 6, 2018

@author: bo
'''
import dask.dataframe as dd 
import numpy as np 
import pandas as pd 
import os
import logging
import fastparquet
import gc
import cPickle as pickle 
import geopandas as gpd

from  zillow import config, utils, features, info
from zillow.task import Task


def make_change_ratio():
    
    def _fun_(state, year, logger):        

# 	if state=='06' and year==2018:
# 		logger.info("Ignore state {} year {}".format(state,year))
# 		return
        this_av = features.get("totalassessedvalue", state, year)
        prior_av = features.get("totalassessedvalue", state, year - 1)
        df = pd.concat([this_av, prior_av], axis=1)
        
        df.columns = ['this_av', 'prior_av']
        df['ratio'] = df['this_av'] / df['prior_av'] - 1
        df = df[~df['ratio'].isnull()]
        df = df[df['ratio'] <> 0]
        med = df['ratio'].median()
        if not np.isnan(med):
            assert not np.isnan(med), med
            logger.info ("median ratio is %s, len(df)=%s", med, len(df))
            df['ratio_median'] = float(med)
            df['ratio_median'] = df['ratio_median'].astype(np.float32) 
            df['big_change'] = (df['ratio'] > max(0.02, med + 0.01)) | (df['ratio'] < min(0.0, med - 0.01))
            logger.info("$bigchange=%s\n%s", df['big_change'].mean(), df.head())
        else:
            df=pd.DataFrame()
        outfile = os.path.join(config.get_state_year_folder(state, year), 'av_change_ratio.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in ['06']:
        for year in config.YEARS[1:]:
            task = Task('make_change_ratios-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def main():
    make_change_ratio()        
                                    
if __name__ == '__main__':
    main()
