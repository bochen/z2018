'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils, features
import os
import hashlib
from zillow.task import Task

 
def shell_run_and_wait(command):
    command = command.split(" ")
    import subprocess
    # process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process = subprocess.Popen(command)
    process.wait()
    return process.returncode


def install_lgb():

    def f(cmd, logger):
        logger.info(cmd)
        code = shell_run_and_wait(cmd)
        assert code == 0, (code, cmd)
        import lightgbm
        logger.info("lgb version: %s", lightgbm.__version__)
        logger.info("lgb path   : %s", lightgbm.__file__)

    assert "ZILLOW_HOME" in os.environ
    cmd = os.path.join(config.HOME, 'python','step0b_install_lgb.sh')
    hashid = hashlib.sha1(cmd).hexdigest()
    task = Task('install_lgb-{}'.format(hashid), lambda u: f(cmd, u))
    task()

    
       
def run():
    install_lgb()


if __name__ == '__main__':
        run()
