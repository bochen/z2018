'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils, info
import numpy as np 
import os
import gc
from zillow.task import Task
import fastparquet
from zillow import features 
import pandas as pd
import traceback
from zillow.pmean import col_median

 
def show_all(df):
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(df)     


def make_lgb_raw_input():
    
    def _fun_(state, year, logger):

        def get_raw_features_other():
            names = ['poolsizesum',
         'poolcnt',
         'fullbathcnt',
         'bathroomcnt',
         'calculatedbathnbr',
         'effectiveyearbuilt',
         'yearbuilt',
         'yearremodelled',
         'roomcnt',
         'unitcnt',
         'garagecarcnt',
         'bedroomcnt',
         'fireplacecnt',
         'numberofstories',
         'buildingconditiontypeid',
         'poolmaintypeid',
         'lotsiteappeals',
         'architecturalstyletypeid',
         'roofstructuretypeid',
         'decktypeid',
         'heatingorsystemtypeid',
         'typeconstructiontypeid',
         'watertypeid',
         'airconditioningtypeid',
         'lotsizetopographytypeid',
         'foundationtypeid',
         'sewertypeid',
         'roofcovertypeid',
         'storytypeid']
            return features.get_features(names, state, year)

        def get_raw_features_tax():
            names = [ 
            'totalmarketvalue', 'totalassessedvalue', 'scaledtotalassessedvalue', 'tax_rate']
            return features.get_features(names, state, year)
        
        def get_raw_features_squarefeet():
            names = ['finishedsquarefeet', 'lotsizesquarefeet', 'yardbuildingsqft', 'garagetotalsqft', 'garagedetachedsqft', 'garageattachedsqft']
            return features.get_features(names, state, year).astype(np.float32)

        def get_raw_features_geo():
            names = ['longi_lati', 'county', 'zip5', 'rawcensustractandblock'] \
            + ['block_x', 'block_y', 'block_area', 'tract_x', 'tract_y', 'tract_area',
              'zip5_x', 'zip5_y', 'zip5_area', 'zip3_x', 'zip3_y', 'zip3_area']
            df = features.get_features(names, state, year)  # .astype(np.float32)
            df['tract'] = df['rawcensustractandblock'].map(lambda u: float(u[5:11]))
            df['xplusy'] = df['longitude'] + df['latitude']
            df['xminusy'] = df['longitude'] - df['latitude']
            df['block_area'] = df['block_area'] * 1e6
            df['tract_area'] = df['tract_area'] * 1e5
            df['zip5_area'] = df['zip5_area'] * 1e5
            df['zip3_area'] = df['zip3_area'] * 1e2
            
            def f(u):
                cols = [u + "_x", u + "_y"]
                a, b = df[cols].values.T
                c = u + "_xplusy"
                df[c] = a + b
                c = u + "_xminusy"
                df[c] = a - b

            for u in ['block', 'tract', 'zip5', 'zip3']: f(u)
            df.drop("rawcensustractandblock", inplace=True, axis=1)
            return df.astype(np.float32)
        
        lst = [get_raw_features_geo(), get_raw_features_other(), get_raw_features_squarefeet(), get_raw_features_tax()]
        df = pd.concat(lst, axis=1)
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        assert df.shape[1] == len(set(df.columns))
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        print "df null mean"
        show_all(df.isnull().mean())
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'lgb_raw_features.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_lgb_raw_input-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def make_lgb_av_sr_adj_hist():
    
    def _fun_(state, year, logger):

        def get_av_sr_adj_hist():
            if state in ['06', '36']:
                return pd.DataFrame()
            years, _ = info.get_salesratio(state)
            years = [int(u) for u in years if int(u) <= year]
            years = sorted(list(years))
            print years
            df = []
            for y in years:
                names = ['av_sr_adj']
                tmpdf = features.get_features(names, state, y)
                tmpdf.columns = ['av_sr_adj_' + str(y)]
                df.append(tmpdf)
            if len(df) == 0: return pd.DataFrame()
            df = pd.concat(df, axis=1)
            df2 = features.hpi_discount(df, state, years, year, 'tract')
            if len(years) > 1:
                for i in range(2, min(6, len(years))):
                    df['med{}_hpi_av_sr_adj'.format(i)] = df2[df2.columns[-i:]].median(axis=1)
                    df['max{}_hpi_av_sr_adj'.format(i)] = df2[df2.columns[-i:]].max(axis=1)
                    df['min{}_hpi_av_sr_adj'.format(i)] = df2[df2.columns[-i:]].min(axis=1)
            return pd.concat([df, df2], axis=1).astype(np.float32)
        
        df = get_av_sr_adj_hist()
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        assert df.shape[1] == len(set(df.columns))
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        print "df null mean"
        show_all(df.isnull().mean())
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'lgb_av_sr_adj.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_lgb_av_sr_adj_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def make_get_yb_sale_price_hist():
    
    def _fun_(state, year, logger):

        def get_yb_sale_price_hist():
            years = [u for u in config.YEARS if u < year]
            years = sorted(list(years))
            print years
            df = []
            for y in years:
                names = ['sale_price_zip5']
                tmpdf = features.get_features(names, state, y)
                # tmpdf.columns=['av_sr_adj_'+str(y)]
                df.append(tmpdf)
            if len(df) == 0: return pd.DataFrame()
            df = pd.concat(df, axis=1)
            df2 = features.hpi_discount(df, state, years, year, 'tract')
            df['sales_count'] = (~df2.isnull()).sum(axis=1)
            if len(years) > 1:
                for i in set([2, 3, 5, len(years)]):
                    if i > len(years): continue            
                    df['med{}_hpi_sale_price_zip5'.format(i)] = df2[df2.columns[-i:]].median(axis=1)
                    df['max{}_hpi_sale_price_zip5'.format(i)] = df2[df2.columns[-i:]].max(axis=1)
                    df['min{}_hpi_sale_price_zip5'.format(i)] = df2[df2.columns[-i:]].min(axis=1)
            return pd.concat([df, df2], axis=1).astype(np.float32)
        
        df = get_yb_sale_price_hist()
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        assert df.shape[1] == len(set(df.columns))
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        print "df null mean"
        show_all(df.isnull().mean())
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'yb_sale_price_hist.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_get_yb_sale_price_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def make_get_av_big_change_hist():
    
    def _fun_(state, year, logger):

        def get_av_big_change_hist():
            years = [u for u in config.YEARS if u <= year and u > 2007]
            years = sorted(list(years))
            print years
            df = []
            
            newyears = []
            for y in years:
                tmpdf = features.get_av_bigchange_value(state, value_year=y, curr_year=year, hpi='tract')
                if len(tmpdf) == 0:
                    logger.info("state=%s,year=%d, av ratio is ignored", state, y)
                else:
                    newyears.append(y)
                    df.append(tmpdf)
            years = newyears 
            print newyears 
                        
            if len(df) == 0: return pd.DataFrame()        
            df = pd.concat(df, axis=1)
            df2 = df.copy()
            df['av_bigchange_count'] = (~df.isnull()).sum(axis=1)
            if len(years) > 1:
                for i in set([2, 3, 5, len(years)]):
                    if i > len(years): continue
                    df['med{}_av_bigchange'.format(i)] = df2[df2.columns[-i:]].median(axis=1)
                    df['max{}_av_bigchange'.format(i)] = df2[df2.columns[-i:]].max(axis=1)
                    df['min{}_av_bigchange'.format(i)] = df2[df2.columns[-i:]].min(axis=1)
            return df.astype(np.float32)
        
        df = get_av_big_change_hist()
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        assert df.shape[1] == len(set(df.columns))
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        print "df null mean"
        show_all(df.isnull().mean())
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'av_big_change_hist.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_get_av_big_change_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()

                                    
def make_lgb_mv_big_change_hist():
    
    def _fun_(state, year, logger):

        def get_mv_big_change_hist():
            years = [u for u in config.YEARS if u <= year and u > 2007]
            years = sorted(list(years))
            if state == '06': return pd.DataFrame()
            if state == '19': years = [u for u in years if u not in [2008, 2010, 2018]]
            print years
            df = []
            newyears = []
            for y in years:
                tmpdf = features.get_mv_bigchange_value(state, value_year=y, curr_year=year, hpi='tract')
                if len(tmpdf) == 0:
                    logger.info("state=%s,year=%d, mv ratio is ignored", state, y)
                else:
                    newyears.append(y)
                    df.append(tmpdf)
            years = newyears 
            print newyears 
            if len(df) == 0: return pd.DataFrame()        
            df = pd.concat(df, axis=1)
            df2 = df.copy()
            df['mv_bigchange_count'] = (~df.isnull()).sum(axis=1)
            if len(years) > 1:
                for i in set([2, 3, 5, len(years)]):
                    if i > len(years): continue
                    df['med{}_mv_bigchange'.format(i)] = df2[df2.columns[-i:]].median(axis=1)
                    df['max{}_mv_bigchange'.format(i)] = df2[df2.columns[-i:]].max(axis=1)
                    df['min{}_mv_bigchange'.format(i)] = df2[df2.columns[-i:]].min(axis=1)
            return df.astype(np.float32)
        
        df = get_mv_big_change_hist()
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        assert df.shape[1] == len(set(df.columns))
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        print "df null mean"
        show_all(df.isnull().mean())
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'lgb_mv_big_change_hist.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_lgb_mv_big_change_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()

             
def make_get_change_ratio_hist():
    
    def _fun_(state, year, logger):

        def get_change_ratio_hist():
            years = [u for u in config.YEARS if u <= year and u > 2007]
            years = sorted(list(years))
            print years
            df = []
            for y in years:
                if state == '06' and y  in [2018]: continue
                names = ['av_ratio']
                tmpdf = features.get_features(names, state, y)
                tmpdf.columns = ["av_ratio_" + str(y)]
                df.append(tmpdf)
            for y in years:
                if state == '06' : continue
                if state == '19' and y  in [2008, 2010, 2018]: continue                
                names = ['mv_ratio']
                tmpdf = features.get_features(names, state, y)
                tmpdf.columns = ["mv_ratio" + str(y)]
                df.append(tmpdf)
            
            if len(df) == 0: return pd.DataFrame()        
            df = pd.concat(df, axis=1)
            return df.astype(np.float32)
        
        df = get_change_ratio_hist()
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        assert df.shape[1] == len(set(df.columns))
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        print "df null mean"
        show_all(df.isnull().mean())
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'change_ratio_hist.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_get_change_ratio_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task() 


def make_get_gam_single_hist():
    
    def _fun_(state, year, logger):

        def get_gam_single_hist():
            years = [u for u in config.YEARS if u < year]
            years = sorted(list(years))
            print years
            df = []
            for y in years:
                names = ['gam_single']
                tmpdf = features.get_features(names, state, y)
                print tmpdf.loc[tmpdf['pred'] > 1e10]
                tmpdf.loc[tmpdf['pred'] > 1e10, 'pred'] = np.nan
                tmpdf = tmpdf[['pred']]
                tmpdf.columns = ["gam_{}_{}".format(u, y) for u in tmpdf.columns]
                df.append(tmpdf)
            if len(df) == 0: return pd.DataFrame()        
            df = pd.concat(df, axis=1)
            df2 = features.hpi_discount(df, state, years, year, 'tract')
            if len(years) > 1:
                for i in set([2, 3, 5, len(years)]):
                    if i > len(years): continue
                    df['med{}_gam_pred'.format(i)] = df2[df2.columns[-i:]].median(axis=1)
                    df['max{}_gam_pred'.format(i)] = df2[df2.columns[-i:]].max(axis=1)
                    df['min{}_gam_pred'.format(i)] = df2[df2.columns[-i:]].min(axis=1)
                    if i >= 5:
                        df['min{}_gam_std'.format(i)] = np.log(df2[df2.columns[-i:]] + 1).std(axis=1)
        
            return pd.concat([df, df2], axis=1).astype(np.float32)
        
        df = get_gam_single_hist()
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        assert df.shape[1] == len(set(df.columns))
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        print "df null mean"
        show_all(df.isnull().mean())
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'gam1_pred_hist.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_get_gam_single_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task() 


def make_get_gam_5fold_hist():
    
    def _fun_(state, year, logger):

        def get_gam_5fold_hist():
            years = [u for u in config.YEARS if u <= year and u > 2012]
            years = sorted(list(years))
            print years
            df = []
            for y in years:
                names = ['gam_5fold']
                tmpdf = features.get_features(names, state, y)
                print tmpdf.loc[tmpdf['pred'] > 1e10]
                tmpdf.loc[tmpdf['pred'] > 1e10, 'pred'] = np.nan
                tmpdf = tmpdf[['pred']]
                tmpdf.columns = ["gam4_{}_{}".format(u, y) for u in tmpdf.columns]
                df.append(tmpdf)
            if len(df) == 0: return pd.DataFrame()        
            df = pd.concat(df, axis=1)
            df2 = features.hpi_discount(df, state, years, year, 'tract')
            if len(years) > 1:
                for i in set([2, 3, 5, len(years)]):
                    if i > len(years): continue
                    df['med{}_gam4_pred'.format(i)] = df2[df2.columns[-i:]].median(axis=1)
                    df['max{}_gam4_pred'.format(i)] = df2[df2.columns[-i:]].max(axis=1)
                    df['min{}_gam4_pred'.format(i)] = df2[df2.columns[-i:]].min(axis=1)
                    if i >= 5:
                        df['min{}_gam4_std'.format(i)] = np.log(df2[df2.columns[-i:]] + 1).std(axis=1)
        
            return pd.concat([df, df2], axis=1).astype(np.float32)
        
        df = get_gam_5fold_hist()
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        assert df.shape[1] == len(set(df.columns))
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        print "df null mean"
        show_all(df.isnull().mean())
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'gam5_pred_hist.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_get_gam_5fold_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task() 

            
def make_get_tax_rate_hist():
    
    def _fun_(state, year, logger):

        def get_tax_ratio_hist():
            years = [u for u in config.YEARS if u <= year]
            years = sorted(list(years))
            print years
            df = []
            for y in years:
                names = ['tax_rate']
                tmpdf = features.get_features(names, state, y)
		tmpdf.loc[tmpdf['tax_rate']>0.1,'tax_rate']=0.1
                tmpdf.columns = ["tax_rate_" + str(y)]
                df.append(tmpdf)
            
            if len(df) == 0: return pd.DataFrame()        
            df = pd.concat(df, axis=1)
            df = pd.concat([df.median(axis=1).to_frame(), df.min(axis=1).to_frame(), df.max(axis=1).to_frame()], axis=1)
            df.columns = ['med_tax_rate', 'min_tax_rate', 'max_tax_rate']
            return df.astype(np.float32)
        
        df = get_tax_ratio_hist()
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        # assert df.shape[1] == len(set(df.columns))
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        print "df null mean"
        show_all(df.isnull().mean())
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'tax_rate_hist.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_get_tax_rate_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()

            
def make_get_tax_value_hist():
    
    def _fun_(state, year, logger):

        def get_tax_value_hist():
            years = [u for u in config.YEARS if u <= year]
            years = sorted(list(years))
            print years
            df = []
            names = ['landtaxvaluedollarcnt', 'structuretaxvaluedollarcnt', 'taxvaluedollarcnt', 'taxratecodearea']
            for y in years:
                tmpdf = features.get_features(names, state, y)
                tmpdf['landratio'] = tmpdf['landtaxvaluedollarcnt'] / (tmpdf['structuretaxvaluedollarcnt'] + tmpdf['landtaxvaluedollarcnt'])
                tmpdf.columns = [u + str(y) for u in tmpdf.columns]
                df.append(tmpdf)
            
            if len(df) == 0: return pd.DataFrame()        
            # for u in df: assert len(df)>0
            df = pd.concat(df, axis=1)

            if len(years) > 1:            
                for name in names + ['landratio']:
                    cols = [name + str(y) for y in years]
                    tmpdf = df[cols]
                    for i in set([2, 3, 5, len(years)]):
                        if i > len(years): continue
                        df['med{}_{}'.format(i, name)] = col_median(tmpdf[tmpdf.columns[-i:]])
                        df['max{}_{}'.format(i, name)] = tmpdf[tmpdf.columns[-i:]].max(axis=1)
                        df['min{}_{}'.format(i, name)] = tmpdf[tmpdf.columns[-i:]].min(axis=1)
			gc.collect()
               
            dropped = [u for u in df.columns if 'taxratecodearea' in u]
            droppeddf = df[dropped]
            df = df.drop(dropped, axis=1)
            df['mean_taxratecodearea'] = droppeddf.mean(axis=1)
            df = df.astype(np.float32)
            return df
        
        df = get_tax_value_hist()
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        # assert df.shape[1] == len(set(df.columns))
        print "df null mean"
        dfmean = (df.isnull().mean())
        show_all(dfmean)
        df = df[df.columns[dfmean < 1]]
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'tax_value_hist.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_get_tax_value_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()            

                                    
def run(f):
    try:
        f()
    except:
        traceback.print_exc()
        raise

                  
if __name__ == '__main__':
    for state in config.STATES:
        for year in config.YEARS + ['share']:
            utils.create_dir_if_not_exists(config.get_state_year_folder2(state, year))

    functions = [
        make_lgb_raw_input,
        make_lgb_av_sr_adj_hist,
        make_get_yb_sale_price_hist,
        make_get_av_big_change_hist,
        make_lgb_mv_big_change_hist,
        make_get_change_ratio_hist,
        #make_get_gam_5fold_hist,
        #make_get_gam_single_hist,
        make_get_tax_rate_hist,
        make_get_tax_value_hist] 
    
    if 0:
        [run(f) for f in functions]
    else:
        from joblib import Parallel, delayed
        Parallel(n_jobs=len(functions))(delayed(f)() for f in functions)

