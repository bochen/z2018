'''
Created on Apr 6, 2018

@author: bo
'''
import dask.dataframe as dd 
import numpy as np 
import pandas as pd 
import os
import logging
import fastparquet
import gc
import cPickle as pickle 
import geopandas as gpd

from  zillow import config, utils, features, info
from zillow.task import Task
from numpy import dtype


def make_avroll_data():
    
    def _fun_(state, year, logger):     
        filename = os.path.join(config.INPUT_PATH, 'third2', 'nyc', 'avroll_{}.txt'.format(year + 1))
        assert utils.file_exists(filename), filename 

        df = pd.read_csv(filename, usecols=['BBLE', 'B', 'BLOCK', 'LOT', 'ZIP', 'TAXCLASS', 'LTFRONT', 'LTDEPTH',
                                      'STORIES', 'BLDFRONT', 'BLDDEPTH', 'FULLVAL', 'AVLAND','AVTOT'],
                      dtype={'B': dtype('int16'),
                             'BBLE': dtype('O'),
                             'BLDDEPTH': dtype('float32'),
                             'BLDFRONT': dtype('float32'),
                             'BLOCK': dtype('int32'),
                             'FULLVAL': dtype('float32'),
                             'AVLAND': dtype('float32'),
                             'AVTOT': dtype('float32'),
                             'LOT': dtype('int32'),
                             'LTDEPTH': dtype('float32'),
                             'LTFRONT': dtype('float32'),
                             'STORIES': dtype('float32'),
                             'TAXCLASS': dtype('O'),
                             'ZIP':dtype('float32')})
        df['BBLE'] = df['BBLE'].astype(np.str)
        df['TAXCLASS'] = df['TAXCLASS'].astype(np.str)
        df['TAXCLASS2'] = df['TAXCLASS'].map(lambda u: u[0]).astype(np.int16)
        
        logger.info("len(df)=%s", len(df))
        logger.info("vc by taxclass\n%s", df['TAXCLASS2'].value_counts())
        logger.info("vc by borough\n%s", df['B'].value_counts())
        
        df = df[df['TAXCLASS2'] < 3]
        df = df[df['FULLVAL'] > 0]
        df = df.set_index("BBLE")
        logger.info("len(df)=%s", len(df))
        logger.info("\n%s", df.head())
        outfile = os.path.join(config.get_state_year_folder(state, year), 'avroll.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')
                   
    for state in ['36']:
        for year in [2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,2018]:
            task = Task('make_avroll_data-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def make_pluto_data():
    
    import pyproj
    NYSP1983 = pyproj.Proj(init="esri:102718", preserve_units=True)

    def nysp_to_gps(x, y):
        if np.isnan(x) or np.isnan(y): return (np.nan, np.nan)
        y, x = NYSP1983(x, y, inverse=True)
        return x, y
    
    def _fun_(state, year, logger): 
        county_fips = {'BX':'005', 'BK':'047', 'MN':'061', 'QN':'081', 'SI':'085'}
        lst = []
        for county in county_fips:
            filename = os.path.join(config.INPUT_PATH, 'third2', 'nyc', 'PLUTO17v1.1', '{}2017V11.csv'.format(county))
            assert utils.file_exists(filename), filename 
    
            dtypes = {
                'BBL': dtype('O'),
                'Borough':dtype('O'),
             'BldgArea': dtype('float32'),
             'BldgDepth': dtype('float32'),
             'BldgFront': dtype('float32'),
             'Block': dtype('int32'),
             'CB2010': dtype('float64'),
             'CT2010': dtype('float64'),
             'GarageArea': dtype('float32'),
             'Lot': dtype('int32'),
             'LotArea': dtype('float32'),
             'LotDepth': dtype('float32'),
             'LotFront': dtype('float32'),
             'NumBldgs': dtype('int32'),
             'NumFloors': dtype('float32'),
             'Tract2010': dtype('O'),
             'UnitsRes': dtype('int32'),
             'UnitsTotal': dtype('int32'),
             'XCoord': dtype('float64'),
             'YCoord': dtype('float64'),
             'YearBuilt': dtype('int32'),
             'ZipCode': dtype('float32')}
            pluto = pd.read_csv(filename, usecols=dtypes.keys(), dtype=dtypes)
    
            def f(u):
                if np.isnan(u): 
                    return str(u)
                else:
                    return str(int(u * 100)).zfill(6)

            pluto['censustract'] = (pluto['CT2010']).map(f)

            def g(u):
                if np.isnan(u): 
                    return str(u)
                else:
                    return str(int(u)).zfill(4)

            pluto['censusblock'] = (pluto['CB2010']).map(g)
            pluto['censustractandblock'] = pluto.apply(lambda u: '36' + county_fips[u['Borough']] + u['censustract'] + u['censusblock'], axis=1)
            pluto.loc[pluto['CT2010'].isnull(), 'censustractandblock'] = 'nan'
            pluto.loc[pluto['CB2010'].isnull(), 'censustractandblock'] = 'nan'
            pluto['XY'] = pluto.apply(lambda u: nysp_to_gps(u['XCoord'], u['YCoord']), axis=1)
            pluto['X'] = (pluto['XY']).map(lambda u: u[0]).astype(np.float32)
            pluto['Y'] = (pluto['XY']).map(lambda u: u[1]).astype(np.float32)
            pluto = pluto.drop(['CT2010', 'censustract', 'CB2010', 'censusblock', 'Tract2010', 'XCoord', 'YCoord', 'XY'], axis=1)
            pluto = pluto.set_index("BBL")
            logger.info("\n%s", pluto.head())
            lst.append(pluto)

        df = pd.concat(lst)
        logger.info("len(df)=%s", len(df))
        logger.info("\n%s", df.head())
        outfile = os.path.join(config.get_state_year_folder(state, year), 'pluto.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')
                   
    for state in ['36']:
        for year in ['share']:
            task = Task('make_pluto_data-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def make_avroll_value():
    def read_pluto(state, year):
        a=dd.read_parquet(os.path.join(config.get_state_year_folder(state,'share'), 'pluto.parq'),columns=[u'X', u'Y','TAXCLASS2']).compute()
        b=dd.read_parquet(os.path.join(config.get_state_year_folder(state,year), 'avroll.parq'),
                          columns=["B",'Block', 'GarageArea','BldgArea','LotArea','NumBldgs','NumFloors'
                                   ,'UnitsRes','UnitsTotal', 'FULLVAL','AVTOT','AVLAND']).compute()
        df2=pd.concat([b,a],1)
        df2=df2[~df2['B'].isnull()]
        df2=df2[~df2['Block'].isnull()]    
        df2=df2[~df2['X'].isnull()]    
        df2=df2[~df2['Y'].isnull()]    
        return df2.drop(['B','Block'],axis=1)
    
    def query(tree, df2, XY, n=1):
        assert n==1
        distance, ii = tree.query(XY, n)
        t= df2.iloc[ii.reshape(-1), :][['FULLVAL','AVTOT','AVLAND']]
        return distance.astype(np.float32) , t
    
    def _fun_(state, year, logger):
        df=features.get_features(['longi_lati'],state,year)
        assert df.isnull().sum().sum()==0
        df2=read_pluto(state,year)
        assert              df2.isnull().sum().sum()==0
        XY=df[['longitude','latitude']].values
        
        from scipy.spatial import cKDTree
        tree = cKDTree(df2[[ 'Y', 'X']].values)
        a,b=query(tree,df2,XY)
        df['avroll_fullval']=b['FULLVAL'].values
        df['avroll_avtot']=b['AVTOT'].values
        df['avroll_avland']=b['AVLAND'].values
        df['avroll_dis']=a
        df=df[['avroll_dis','avroll_fullval','avroll_avtot','avroll_avland']].astype(np.float32)


        logger.info("len(df)=%s", len(df))
        logger.info("\n%s", df.head())
        outfile = os.path.join(config.get_state_year_folder(state, year), 'avroll_val.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')
                   
    for state in ['36']:
        for year in [2018]:
            task = Task('make_avroll_value-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def main():
    if 1:        
        make_pluto_data()
        make_avroll_data()  
        make_avroll_value()      
        
                                    
if __name__ == '__main__':
    main()
                                        
