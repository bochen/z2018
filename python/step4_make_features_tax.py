'''
Created on Apr 6, 2018

@author: bo
'''
import dask.dataframe as dd 
import numpy as np 
import pandas as pd 
import os
import logging
import fastparquet
import gc
import cPickle as pickle 
import geopandas as gpd

from  zillow import config, utils, features, info
from zillow.task import Task


def make_county_dict(state):
        feat_county = features.get("county", state)['county'].to_dict()
        if state=='37':
                feat_county.update({172976938: '37081',
 172976945: '37081',
 172976947: '37081',
 172976950: '37081',
 172976951: '37081',
 172976952: '37081',
 172976953: '37081',
 172976961: '37081',
 172976962: '37081',
 172976964: '37081',
 172976968: '37081',
 172976969: '37081',
 172977021: '37081',
 172977023: '37081',
 172977204: '37081',
 172977391: '37081',
 172977419: '37081',
 172977421: '37081',
 172977423: '37081',
 172977588: '37081',
 172977647: '37001',
 172978182: '37081',
 172978183: '37081',
 172978197: '37081',
 172978274: '37081',
 172978294: '37081'})
	return feat_county
def make_tax_rate_info():
    
    def g():
        for i, state in enumerate(config.STATES):
            for year in config.YEARS:
            # for year in [2017]:
                task = Task('make_tax_rate_info-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
                task()
            # if i + 1 > 3: af
        
    def _fun_(state, year, logger):   
        abnormals = { 
            2013: {"06097"}
            }     
        abnormals = set([]) if year not in abnormals else abnormals[year]
        fs04ctst = info.get_county_fs04ctst()['county_name'].to_dict()
        filename = os.path.join(config.get_state_year_folder(state, year), "tax_hist.parq")
        assert utils.file_exists(filename), filename 
        df = dd.read_parquet(filename, columns=['parcelid', "taxamount", "totalassessedvalue"]).compute().set_index('parcelid')
        logger.info("\n%s", df.head())
        feat_county = make_county_dict(state) 
	#print list(df.index.difference(feat_county.keys()))
        df['county'] = df.index.map(lambda u: feat_county[u]) 
        assert (df['county'].isnull().sum() == 0)
        df['tax_rate'] = 1.0 * df['taxamount'] / df['totalassessedvalue']
        logger.info("get %s records", len(df))
        logger.info("#record by county:")
        logger.info("%s\t%s\t%s\t%s\t%s", 'fips', '#record', '#!niltax', 'med_r', 'county')
        for county in set(df['county']):
            a = ~df[df['county'] == county]['tax_rate'].isnull()
            logger.info("%s\t%s\t%s\t%s\t%s", county, len(a), a.sum(), round(a.mean(), 4), fs04ctst[county])
        df = df.drop(["taxamount", "totalassessedvalue"], axis=1).dropna()
        state_r = df[~df['county'].isin(abnormals)]['tax_rate'].median()
        logger.info("state(%s) rate: %s", state, state_r)
        rates = {state:state_r}
        for county in set(df['county']):
            a = df[df['county'] == county]['tax_rate']
            if len(a) < 100:
                logger.warning('county %s has too few records. use state median instead', county)
                rates[county] = state_r
            else:
                r = a.median()
                if r > 2 * state_r or r < state_r / 4.0:
                    logger.warning("county %s has abnormal rate %s, use state median instead", county, r)
                    r = state_r 
                rates[county] = r
        logger.info('%s\t%s\t%s', 'fips', 'rate', 'name')                
        for k, v in sorted (rates.items(), key=lambda u: u[1]):
            logger.info('%s\t%s\t%s', k, round(v, 4), state if k == state else fs04ctst[k])
        logger.info("get %s records", len(rates))
        pickle.dump(rates, open(os.path.join(config.INFO_PATH, '{}_{}_tax_rate.pkl'.format(state, year)), 'wb'))
        
    g()


def make_tax_rate_feat():
    
    def _fun_(state, year, logger):        
        filename = os.path.join(config.get_state_year_folder(state, year), "tax_hist.parq")
        assert utils.file_exists(filename), filename 
        df = dd.read_parquet(filename, columns=['parcelid', "taxamount", "totalassessedvalue"]).compute().set_index('parcelid')
        logger.info("\n%s", df.head())
        feat_county = make_county_dict(state) 
	#print list(df.index.difference(feat_county.keys()))
        df['county'] = df.index.map(lambda u: feat_county[u]) 
        assert (df['county'].isnull().sum() == 0)
        df['tax_rate'] = 1.0 * df['taxamount'] / df['totalassessedvalue']
        logger.info("get %s records", len(df))
        df = df[['tax_rate']].astype(np.float32)
        logger.info("\n%s", df.head())
        outfile = os.path.join(config.get_state_year_folder(state, year), 'tax_rate.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
#         for year in [2017]:
            task = Task('make_tax_rate_feat-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def make_totalassessedvalue_feat():
    
    def _fun_(state, year, logger):        
        filename = os.path.join(config.get_state_year_folder(state, year), "tax_hist.parq")
        assert utils.file_exists(filename), filename 
        df = dd.read_parquet(filename, columns=['parcelid', "taxamount", "totalassessedvalue"]).compute().set_index('parcelid')
        logger.info("\n%s", df.head())
        logger.info("len(df)=%s", len(df))
        assert len(df) == len(set(df.index))
        taxrates = info.get_tax_rates(state, year)
        logger.info("taxrates: %s", str(taxrates))
        if state == '36':
            taxrates, taxrates2 = taxrates
        feat_county = make_county_dict(state)
        #print list(df.index.difference(feat_county.keys()))
        df['county'] = df.index.map(lambda u: feat_county[u])
        feat_unitcnt = features.get('unitcnt', state, year) 
        df['unitcnt'] = feat_unitcnt.loc[df.index]
        df['class2'] = df['unitcnt'] > 3
        assert (df['county'].isnull().sum() == 0)
        olddf = df

        ind = (df['totalassessedvalue'].isnull()) & (~df['taxamount'].isnull()) 
        count1 = (~df['totalassessedvalue'].isnull()).sum()
        df1 = df[~ind]
        df2 = df[ind]
        if state == '36':
            df2.loc[~df2['class2'], 'tax_rate'] = df2.loc[~df2['class2'], 'county'].map(lambda u: taxrates[year])
            df2.loc[df2['class2'], 'tax_rate'] = df2.loc[df2['class2'], 'county'].map(lambda u: taxrates2[year])
        else:
            df2['tax_rate'] = df2['county'].map(lambda u: taxrates[state] if u not in taxrates else taxrates[u])
        df2.loc[:, 'totalassessedvalue'] = df2['taxamount'] / df2['tax_rate']
        logger.info("fix %s/(%s-%s) records", len(df2), len(df), len(df1))
        df = pd.concat([df1[['totalassessedvalue']], df2[['totalassessedvalue']]], axis=0)
        assert df.shape[0] == olddf.shape[0], str(df.shape) + " " + str(olddf.shape)
        df = df.loc[olddf.index]
        count2 = (~df['totalassessedvalue'].isnull()).sum()
        df['totalassessedvalue']=df['totalassessedvalue'].astype(np.float32)
        logger.info("changes %s -> %s", count1, count2)
        outfile = os.path.join(config.get_state_year_folder(state, year), 'totalassessedvalue.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_totalassessedvalue_feat-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()

            
def make_scaledtotalassessedvalue_feat():
    
    def _fun_(state, year, logger):        
        filename = os.path.join(config.get_state_year_folder(state, year), 'totalassessedvalue.parq')
        assert utils.file_exists(filename), filename 
        df = dd.read_parquet(filename).compute()
        logger.info("\n%s", df.head())
        logger.info("len(df)=%s", len(df))
        assert len(df) == len(set(df.index))
        if state == '06':
            # CA only has no mv
            pass
        elif state == '19':
            # IA mv and av are almost always same
            pass 
        elif state == '37':
            pass
        elif state == '39': 
            df['totalassessedvalue'] = (df['totalassessedvalue'] / 0.35).astype(np.float32)
        elif state == '36':
            feat_unitcnt = features.get('unitcnt', state, year) 
            df['unitcnt'] = feat_unitcnt.loc[df.index]
            df['class2'] = df['unitcnt'] > 3
            df.loc[:, 'totalassessedvalue'] = (df['class2'] * df['totalassessedvalue'] / 0.45 \
                    + (1 - df['class2']) * df['totalassessedvalue'] / 0.06).astype(np.float32)
        else:
            raise Exception("unkown "+state )
        df["scaledtotalassessedvalue"] = df['totalassessedvalue'].astype(np.float32)
        df = df[['scaledtotalassessedvalue']]
        outfile = os.path.join(config.get_state_year_folder(state, year), 'scaledtotalassessedvalue.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_scaledtotalassessedvalue_feat-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def make_totalmarketvalue_feat():
    
    def _fun_(state, year, logger):        
        filename = os.path.join(config.get_state_year_folder(state, year), "tax_hist.parq")
        assert utils.file_exists(filename), filename 
        df = dd.read_parquet(filename, columns=['parcelid', "totalmarketvalue"]).compute().set_index('parcelid')
        logger.info("\n%s", df.head())
        logger.info("len(df)=%s, null ratio=%s", len(df), df['totalmarketvalue'].isnull().mean())
        assert len(df) == len(set(df.index))
        
        outfile = os.path.join(config.get_state_year_folder(state, year), 'totalmarketvalue.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_totalmarketvalue_feat-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()

                                    
if __name__ == '__main__':
    if 1:        
        make_tax_rate_info()
        make_tax_rate_feat()
        make_totalassessedvalue_feat()
        make_scaledtotalassessedvalue_feat()    
        make_totalmarketvalue_feat()        
        
