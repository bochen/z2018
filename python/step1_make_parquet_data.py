'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils
import dask.dataframe as dd 
import numpy as np 
import os
import logging
from zillow.task import Task
import fastparquet
import gc
 
def home_attrib_to_parquet():
    def _fun_(logger):
        dtypes = {'actualbathnbr': 'float32',
         'airconditioningtypeid': 'float32',
         'architecturalstyletypeid': 'float32',
         'assessmentyear': 'float32',
         'basementsqft': 'float32',
         'basementtypeid20': 'float32',
         'basementtypeid21': 'float32',
         'basementtypeid22': 'float32',
         'basementtypeid23': 'float32',
         'basementtypeid25': 'float32',
         'basementtypeid26': 'float32',
         'bathplumbingfixturecnt': 'float32',
         'bathroomcnt': 'float32',
         'bedroomcnt': 'float32',
         'boatdocktypeid': 'float32',
         'buildingclasstypeid': 'float32',
         'buildingconditiontypeid': 'float32',
         'buildingqualitytypeid': 'float32',
         'calculatedbathnbr': 'float32',
         'censustract': 'O',
         'censustractandblock': 'O',
         'censustractandblockgroupid': 'O',
         'decktypeid': 'float32',
         'drivewaytypeid': 'float32',
         'effectiveyearbuilt': 'float32',
         'elevatortypeid': 'float32',
         'fencetypeid': 'float32',
         'finishedbasementsqft': 'float32',
         'finishedfloor1squarefeet': 'float32',
         'finishedsquarefeet': 'float32',
         'finishedsquarefeet10': 'float32',
         'finishedsquarefeet11': 'float32',
         'finishedsquarefeet12': 'float32',
         'finishedsquarefeet13': 'float32',
         'finishedsquarefeet15': 'float32',
         'finishedsquarefeet50': 'float32',
         'finishedsquarefeet6': 'float32',
         'finishedsquarefeet7': 'float32',
         'finishedsquarefeet8': 'float32',
         'finishedsquarefeet9': 'float32',
         'fips': 'float32',
         'fireplacecnt': 'float32',
         'fireplaceflag': 'float32',
         'foundationtypeid': 'float32',
         'fullbathcnt': 'float32',
         'garageattachedsqft': 'float32',
         'garagecarcnt': 'float32',
         'garagedetachedsqft': 'float32',
         'garagetotalsqft': 'float32',
         'hashottuborspa': 'float32',
         'heatingorsystemtypeid': 'float32',
         'latestdataatyear': 'float32',
         'latitude': 'float32',
         'longitude': 'float32',
         'lotsiteappeals': 'float32',
         'lotsizedepthfeet': 'float32',
         'lotsizefrontagefeet': 'float32',
         'lotsizeirregularity': 'O',
         'lotsizesquarefeet': 'float32',
         'lotsizetopographytypeid': 'float32',
         'numberofstories': 'float32',
         'otherextrafeatureidone': 'float32',
         'otherextrafeaturetypeid1': 'float32',
         'otherextrafeaturetypeid10': 'float32',
         'otherextrafeaturetypeid11': 'float32',
         'otherextrafeaturetypeid12': 'float32',
         'otherextrafeaturetypeid24': 'float32',
         'otherextrafeaturetypeid39': 'float32',
         'otherextrafeaturetypeid4': 'float32',
         'otherextrafeaturetypeid40': 'float32',
         'otherextrafeaturetypeid41': 'float32',
         'otherextrafeaturetypeid42': 'float32',
         'otherextrafeaturetypeid43': 'float32',
         'otherextrafeaturetypeid44': 'float32',
         'otherextrafeaturetypeid45': 'float32',
         'otherextrafeaturetypeid46': 'float32',
         'otherextrafeaturetypeid47': 'float32',
         'otherextrafeaturetypeid48': 'float32',
         'otherextrafeaturetypeid49': 'float32',
         'otherextrafeaturetypeid50': 'float32',
         'otherextrafeaturetypeid51': 'float32',
         'otherextrafeaturetypeid52': 'float32',
         'otherextrafeaturetypeid53': 'float32',
         'otherextrafeaturetypeid54': 'float32',
         'otherextrafeaturetypeid55': 'float32',
         'otherextrafeaturetypeid56': 'float32',
         'otherextrafeaturetypeid57': 'float32',
         'otherextrafeaturetypeid58': 'float32',
         'otherextrafeaturetypeid59': 'float32',
         'otherextrafeaturetypeid60': 'float32',
         'otherextrafeaturetypeid61': 'float32',
         'otherextrafeaturetypeid62': 'float32',
         'otherextrafeaturetypeid63': 'float32',
         'otherextrafeaturetypeid64': 'float32',
         'otherextrafeaturetypeid65': 'float32',
         'otherextrafeaturetypeid67': 'float32',
         'otherextrafeaturetypeid68': 'float32',
         'otherextrafeaturetypeid69': 'float32',
         'otherextrafeaturetypeid7': 'float32',
         'otherextrafeaturetypeid8': 'float32',
         'otherextrafeaturetypeid9': 'float32',
         'parcelid': 'int32',
         'poolcnt': 'float32',
         'poolmaintypeid': 'float32',
         'poolsizesum': 'float32',
         'pooltypeid1': 'float32',
         'pooltypeid10': 'float32',
         'pooltypeid2': 'float32',
         'pooltypeid3': 'float32',
         'pooltypeid4': 'float32',
         'pooltypeid5': 'float32',
         'pooltypeid6': 'float32',
         'pooltypeid7': 'float32',
         'pooltypeid8': 'float32',
         'propertycountylandusecode': 'O',
         'propertylandusetypeid': 'float32',
         'propertystatelandusecode': 'O',
         'propertyzoningdesc': 'O',
         'rawcensustractandblock': 'O',
         'regionidcity': 'float32',
         'regionidcounty': 'float32',
         'regionidmsa': 'float32',
         'regionidneighborhood': 'float32',
         'regionidstate': 'float32',
         'regionidzip': 'float32',
         'roofcovertypeid': 'float32',
         'roofstructuretypeid': 'float32',
         'roomcnt': 'float32',
         'sewertypeid': 'float32',
         'storytypeid': 'float32',
         'threequarterbathnbr': 'float32',
         'timesharetypeid': 'float32',
         'typeconstructiontypeid': 'float32',
         'unfinishedbasementsqft': 'float32',
         'unitcnt': 'float32',
         'waterfrontstructures': 'float32',
         'watertypeid': 'float32',
         'yardbuildingsqft1': 'float32',
         'yardbuildingsqft10': 'float32',
         'yardbuildingsqft11': 'float32',
         'yardbuildingsqft12': 'float32',
         'yardbuildingsqft13': 'float32',
         'yardbuildingsqft14': 'float32',
         'yardbuildingsqft16': 'float32',
         'yardbuildingsqft17': 'float32',
         'yardbuildingsqft18': 'float32',
         'yardbuildingsqft19': 'float32',
         'yardbuildingsqft2': 'float32',
         'yardbuildingsqft20': 'float32',
         'yardbuildingsqft21': 'float32',
         'yardbuildingsqft22': 'float32',
         'yardbuildingsqft23': 'float32',
         'yardbuildingsqft24': 'float32',
         'yardbuildingsqft25': 'float32',
         'yardbuildingsqft26': 'float32',
         'yardbuildingsqft27': 'float32',
         'yardbuildingsqft28': 'float32',
         'yardbuildingsqft29': 'float32',
         'yardbuildingsqft3': 'float32',
         'yardbuildingsqft30': 'float32',
         'yardbuildingsqft31': 'float32',
         'yardbuildingsqft4': 'float32',
         'yardbuildingsqft5': 'float32',
         'yardbuildingsqft6': 'float32',
         'yardbuildingsqft7': 'float32',
         'yardbuildingsqft8': 'float32',
         'yardbuildingsqft9': 'float32',
         'yearbuilt': 'float32',
         'yearremodelled': 'float32'}
        
        filenames = config.HOME_ATTRIB_CSV_FILES
        logger.info("Start transform home attrib to parquet")
        for name in filenames:
            output_filename = os.path.join(config.PARQUET_INPUT_PATH, utils.basename(name).replace(".csv", '.parq'))
            if os.path.exists(output_filename): 
		try:
			dd.read_parquet(output_filename)
                	logger.info("%s exists. skip", output_filename)
                	continue 
		except:
                	logger.info("%s exists but is bad. redo", output_filename)
            assert utils.file_exists(name), name            
            logger.info("%s to %s", name, output_filename)
            df = dd.read_csv(name, assume_missing=True, dtype=dtypes, header=0) 
            for col in df.dtypes[df.dtypes == np.object].index:
                df[col] = df[col].astype(np.str)
            df.to_parquet(output_filename, compression='snappy', object_encoding='bytes')
        logger.info("End transform home attrib to parquet")
    task = Task('home_attrib_to_parquet', _fun_)
    task()
        
 
def saleprice_to_parquet():
    def _fun_(logger):
        dtypes = {'concurrentloanamount': 'float32',
         'concurrentloancount': 'float32',
         # 'createdate': 'O',
         'dataclasstypeid': 'float32',
         'deedsloanamount': 'float32',
         'derivedloanamount': 'float32',
         'derivedloancount': 'float32',
         'documenttypeid': 'float32',
         'inclusionruleidzestimate': 'float32',
         'legalrecordingid': 'int32',
         'loanamount': 'float32',
         'loancount': 'float32',
         'parcelid': 'int32',
         'partialinteresttransferpercent': 'float32',
         'partialinteresttransfertypeid': 'float32',
         # 'recordingdate': 'O',
         'regionidcounty': 'int32',
         'saleprice': 'float32',
         'transactionyear': 'int32'}
        
        filename = config.SALESPRICE_CSV_FILE
        logger.info("Start transform sale price to parquet")
        assert utils.file_exists(filename), filename 
        df = dd.read_csv(filename, assume_missing=True, header=0, dtype=dtypes, parse_dates=['createdate', 'recordingdate'])
        output_filename = os.path.join(config.PARQUET_INPUT_PATH, utils.basename(filename).replace(".csv", '.parq'))
        df.to_parquet(output_filename, compression='snappy')
        
        logger.info("End transform sale price to parquet")
    task = Task('saleprice_to_parquet', _fun_)
    task()
        
def tax_historys_to_parquet():
    def _fun_(logger):
        dtypes = {'assessmentyear': 'int16',
             'edition': 'int16',
             'improvementappraisedvalue': 'float32',
             'improvementassessedvalue': 'float32',
             'improvementmarketvalue': 'float32',
             'landappraisedvalue': 'float32',
             'landassessedvalue': 'float32',
             'landmarketvalue': 'float32',
             'landtaxvaluedollarcnt': 'float32',
             'parcelid': 'int32',
             'structuretaxvaluedollarcnt': 'float32',
             'taxamount': 'float32',
             'taxdelinquencyamount': 'float32',
             'taxdelinquencyyear': 'float32',
             'taxratecodearea': 'float32',
             'taxvaluedollarcnt': 'float32',
             'totalappraisedvalue': 'float32',
             'totalassessedvalue': 'float32',
             'totalmarketvalue': 'float32'}
        
        filename = config.TAXHIST_CSV_FILE
        logger.info("Start transform tax history to parquet")
        assert utils.file_exists(filename)
        df = dd.read_csv(filename, assume_missing=False, header=0, dtype=dtypes)
        output_filename = os.path.join(config.PARQUET_INPUT_PATH, utils.basename(filename).replace(".csv", '.parq'))
        df.to_parquet(output_filename, compression='snappy')
        
        logger.info("End transform tax history to parquet")
    task = Task('tax_historys_to_parquet', _fun_)
    task()

def home_attrib_fillna():
    def read_parq_file(fname, logger):
        logger.info("read %s", fname)        
        return dd.read_parquet(fname).compute().set_index("parcelid")
    def read_ff_parq_file(fname, logger):
        fname = fname + ".ff"        
        logger.info("read %s", fname)                
        return dd.read_parquet(fname).compute()
    def write_ff_parq_file(fname, df, logger):
        fname = fname + ".ff"
        logger.info("write %s", fname)
        return fastparquet.write(fname, df, compression='SNAPPY')
    def write_fb_parq_file(fname, df, logger):
        fname = fname + ".fb"        
        logger.info("write %s", fname)
        return fastparquet.write(fname, df, compression='SNAPPY')
    def fillna(src, target, logger):
        assert (src.columns == target.columns).all()
        for col in src.columns:
            s1 = src.index[~src[col].isnull()]
            s2 = target.index[target[col].isnull()]
            idx = s1.intersection(s2)
            logger.info("fillna {} with {} records. src_nil#: {}, target_nil#: {}".format(col, len(idx), len(s1), len(s2)))
            target.loc[idx, col] = src.loc[idx, col]
        return target

    def ff(files, logger):
        logger.warn("Files will be sorted in ascending order. make sure this is on purpose.")
        files = sorted(files)
        logger.info("Start ff")
        this_df = read_parq_file(files[0], logger)
        newfname = files[0]
        write_ff_parq_file(newfname, this_df, logger)
        for i in range(len(files) - 1):
            next_file = files[i + 1]
            logger.info("begin loading %s", next_file)
            next_df = read_parq_file(next_file, logger)
            logger.info("finish loading %s", next_file) 
            next_df = fillna(this_df, next_df, logger)
            write_ff_parq_file(next_file, next_df, logger)
            this_df = next_df
            gc.collect()

    def fb(files, logger): 
        logger.warn("Files will be sorted in descending order. make sure this is on purpose.")
        files = list(reversed(sorted(files))  )
        logger.info("Start fb")        
        this_df = read_ff_parq_file(files[0], logger)
        newfname = files[0]
        write_fb_parq_file(newfname, this_df, logger)
        for i in range(len(files) - 1):
            next_file = files[i + 1]
            logger.info("begin loading %s", next_file)
            next_df = read_ff_parq_file(next_file, logger)
            logger.info("finish loading %s", next_file) 
            next_df = fillna(this_df, next_df, logger)
            write_fb_parq_file(next_file, next_df, logger)
            this_df = next_df
            gc.collect()
    
    def _fun_(logger):
        filenames = config.HOME_ATTRIB_PARQ_FILES
        logger.info("Start home_attrib_fillna")
        
        for filename in filenames: 
            assert utils.file_exists(filename) , filename 
        
        ff(filenames, logger)
        fb(filenames, logger)
        logger.info("End home_attrib_fillna")
    task = Task('home_attrib_fillna', _fun_)
    task()

        
if __name__ == '__main__':
    home_attrib_to_parquet()
    saleprice_to_parquet()
    tax_historys_to_parquet()
    home_attrib_fillna()
