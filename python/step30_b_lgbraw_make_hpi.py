'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils, features
import os
from zillow.task import Task
import pandas as pd
import sys
import step30_a_lgbraw_predict
import fastparquet

 
def show_all(df):
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(df)     


param_hashes = step30_a_lgbraw_predict.param_hashes


def shell_run_and_wait(command):
    command = command.split(" ")
    import subprocess
    #process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process = subprocess.Popen(command)
    process.wait()
    return process.returncode


def run_lgb(state, year, logger):
    cmd = "python {}/python/zillow/lgb_run5_rawpredict.py --prefix lgbraw --param {} --year {} --state {} --seed {} --with_month --use_rawprice".format(
        config.HOME, param_hashes[state], year, state, int(state) * 10000 + year)
    logger.info("cmd: %s", cmd)
    code = shell_run_and_wait(cmd)
    assert code == 0, (code, cmd)
       
def run(state, year ):
    task = Task('make_lgbraw_hpi-{}-{}'.format(state, year), lambda u: run_lgb(state, year , u))
    task()
                                    

def make_sale_price_by_model():
    
    def _fun_(state, year, logger):        
        df = features.get_yb_sales(state, year, month_cut=None , hpi='model')
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        outfile = os.path.join(config.get_state_year_folder(state, year), 'sale_price_modelhpi.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        #for year in [2017,2018]:
        for year in [2018]:            
            task = Task('make_sale_price_by_model-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()
                      
                                                          
if __name__ == '__main__':
    for state in config.STATES:
        #for year in [2017,2018]:
        for year in [2018]:            
            run(state,year)
            
    make_sale_price_by_model()
