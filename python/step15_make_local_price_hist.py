'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config
import numpy as np 
import os
import gc
from zillow.task import Task
import fastparquet
from zillow import features 
import pandas as pd
import traceback
from zillow.pmean import col_median

 
def show_all(df):
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(df)     

         
def make_local_price_hist():
    
    def _fun_(state, year, logger):

        def make_hist(col):
	    ret=_make_hist(col)
            gc.collect()
	    return ret

        def _make_hist(col):
            logger.info("starts {}".format(col))
            years = [u for u in config.YEARS if u <year]
            years = sorted(list(years))
            print years
            df = []
            names = [col]
            for y in years:
                tmpdf = features.get_features(names, state, y)
                tmpdf.columns= [col + str(y)] 
                df.append(tmpdf)
            
            if len(df) == 0: return pd.DataFrame()        
            df = pd.concat(df, axis=1)

            if len(years) > 1:            
                tmpdf = df.copy()
                for i in set([2, 3, 5, len(years)]):
                    if i > len(years): continue
                    name = col
                    df['med{}_{}'.format(i, name)] = col_median(tmpdf[tmpdf.columns[-i:]])
                    df['max{}_{}'.format(i, name)] = tmpdf[tmpdf.columns[-i:]].max(axis=1)
                    df['min{}_{}'.format(i, name)] = tmpdf[tmpdf.columns[-i:]].min(axis=1)
               
            df = df.astype(np.float32)
            logger.info("len(df)=%s\n%s", len(df), df.head())            
            return df

        lst = [
            make_hist('beta1'),
            make_hist('beta2'),
            make_hist('estprice'),
            ]
        df = pd.concat(lst, axis=1).astype(np.float32)
        print "df null mean"
        dfmean = (df.isnull().mean())
        show_all(dfmean)
        df = df[df.columns[dfmean < 1]]
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'local_price_hist.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_local_price_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()            

                                    
def run(f):
    try:
        f()
    except:
        traceback.print_exc()
        raise


def run_null():
    pass 

                  
if __name__ == '__main__':

    functions = [
        make_local_price_hist,
        run_null,
        run_null,
        run_null,
        run_null,
        run_null,
        run_null] 
    
    [run(f) for f in functions]
