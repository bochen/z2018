#!/bin/bash

cd $ZILLOW_HOME/input/third3/

[ -e LightGBM ] && rm -fr LightGBM

tar zxf LightGBM.tar.gz

LGB_HOME=$ZILLOW_HOME/input/third3/LightGBM

cd $LGB_HOME
mkdir -p build
cd build
cmake ..
make

cd $LGB_HOME
cd python-package
python setup.py install

