'''
Created on Apr 6, 2018

@author: bo
'''
import numpy as np 
import os, sys
import fastparquet
import time

from  zillow import config, features
from zillow.task import Task
import traceback
import statsmodels.api as sm
import pandas as pd  

XY = None
tree = None 
sales = None 


def timeit(g):
    
    t0 = time.time()
    ret = g()
    t1 = time.time()
    
    total = t1 - t0
    return ret, total     


def run_model(data):
    y = data['price'].values
    X = data[['lotsizesquarefeet', 'finishedsquarefeet']].values
    rlm_model = sm.RLM(y, X, M=sm.robust.norms.HuberT())
    hub_results = rlm_model.fit()
    if 0:
        print(hub_results.params)
        print(hub_results.bse)
        print(hub_results.summary(yname='y',
                    xname=['var_%d' % i for i in range(len(hub_results.params))]))
    weights = hub_results.weights / np.sum(hub_results.weights)
    return hub_results.params, weights 


def f(j):
    t0 = time.time()
    global XY, tree, sales
    print ("begin batch " + str(j))
    lst = XY[j]
    _, ret = tree.query(lst, k=100)
    distances = []
    for u, l in enumerate(ret):
        data = sales.iloc[l]
        # (beta,weights), tcost = timeit(lambda: run_model(data))
        beta, weights = run_model(data)
        xy = np.sum(data[['longitude', 'latitude']].values * weights.reshape([-1, 1]), axis=0)
        tmpres = np.concatenate([xy, beta])
        # print xy, beta,weights,tmpres
        distances.append(tmpres)
    t1 = time.time()
    print ("end batch {}, cost {} ".format(j, t1 - t0))
    return np.array(distances).astype(np.float32)


def _fun_(state, year, logger):

    def _make_data():
        gam = features.get('gam_single', state, year)['error'].dropna()
        logger.info("big error ratio 40%={},80%={}".format(np.mean(np.abs(gam) > 0.4), np.mean(np.abs(gam) > 0.8))) 
        parcels = gam[np.abs(gam) < 0.8].index
        logger.info('length {}/{}'.format(len(gam), len(parcels)))
        all_sales = features.get_features(['longi_lati', 'lotsizesquarefeet', 'finishedsquarefeet', 'sale_price_tract'], state, year)
        # all_sales=features.get_features(['longi_lati','sale_price_tract'],state,year)
        all_sales['price'] = all_sales['yb_{}_tract_saleprice'.format(year)]
        all_sales = all_sales.drop('yb_{}_tract_saleprice'.format(year), axis=1)

        if state <> '36':
            X = np.array(list(set(np.round(all_sales['longitude'].dropna(), 3))))
            Y = np.array(list(set(np.round(all_sales['latitude'].dropna(), 3))))
        else:
            X = np.array(list(set(np.round(all_sales['longitude'].dropna() * 5.0, 3) / 5.0)))
            Y = np.array(list(set(np.round(all_sales['latitude'].dropna() * 5.0, 3) / 5.0)))

        logger.info("len(X)={},len(Y)={}".format(len(X), len(Y)))
        sales = all_sales.loc[parcels].dropna()
        logger.info("\n%s", sales.head())
        points = []
        for _ in range(int(len(X) * 0.15)):
            for _ in range(int(len(Y) * 0.15)):
                a = X[int(np.random.random() * len(X))]
                b = Y[int(np.random.random() * len(Y))]
                points.append([a, b])        
        return np.array(points), sales
    
    global XY, tree, sales
    points, sales = _make_data()
    assert np.sum(np.isnan(points)) == 0
    assert np.sum(np.isnan(sales.values)) == 0
    logger.info("#sample={}".format(len(points)))
    logger.info(points[:5])

    from scipy.spatial import cKDTree
    from multiprocessing import Pool

    tree = cKDTree(sales[['longitude', 'latitude']].values)
    logger.info("start to calc..., #data %d", len(points))
    i = 0
    XY = []
    while i < points.shape[0]:
        XY.append(points[i:i + 2000])
        i += 2000
    logger.info("#batch: " + str(len(XY)))
    if 1:
        p = Pool(10)
        params = list(range(len(XY)))
        print params
        ret = p.map(f, params)
        p.close()
        p.join()
    else:
        ret = [ f(k) for k in range(len(XY))]
    b = np.concatenate(ret)
    df = pd.DataFrame(b, columns=['x', 'y', 'beta1', 'beta2']).astype(np.float32)
    logger.info('\n%s', df.head())
    logger.info("max,min=%s,%s", np.max(df), np.min(df))
    
    outfile = os.path.join(config.get_state_year_folder(state, year), 'local_beta_100.parq')
    fastparquet.write(outfile, df, compression='SNAPPY')


def make_local_price(astate):
    if astate is None:
        states = config.STATES
    else:
        states = [astate]
    for state in states:
        for year in config.YEARS:
            try:            
                task = Task('make_local_price-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
                task()
            except:
                traceback.print_exc()
                raise
            

def main(state=None):
    make_local_price(state)

                                        
if __name__ == '__main__':
    state = None
    if len(sys.argv) == 2: 
        state = sys.argv[1]
    if len(sys.argv) <= 2:
        main(state)
    else:
        raise Exception("argv error")
