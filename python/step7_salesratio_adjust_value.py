'''
Created on Apr 6, 2018

@author: bo
'''
import dask.dataframe as dd 
import numpy as np 
import pandas as pd 
import os
import logging
import fastparquet
import gc
import cPickle as pickle 
import geopandas as gpd

from  zillow import config, utils, features, info
from zillow.task import Task


def make_salesratio_adjust_value():
    
    def _fun_(sr, state, year, logger):        
        df = features.get("scaledtotalassessedvalue", state, year)
        counties = features.make_county_dict(state) #get("county", state, year)['county'].to_dict()
        df['countyfips']=df.index.map(lambda u: counties[u])
        df['salesratio']=df['countyfips'].map(lambda u: sr[year][u]).astype(np.float32)
        df['av_sr_adj']=(df['scaledtotalassessedvalue']/df['salesratio']).astype(np.float32)
        logger.info ("len(df)=%s\n%s", len(df), df.head())
        df=df[['av_sr_adj','salesratio']]
        
        outfile = os.path.join(config.get_state_year_folder(state, year), 'av_sr_adj.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in ['19', '37', "39"]:
        years, sr = info.get_salesratio(state)
        print years,sr
        for year in sorted(years):
            task = Task('make_salesratio_adjust_value-{}-{}'.format(state, year), lambda u: _fun_(sr.copy(), state, year , u))
            task()

                                    
if __name__ == '__main__':
    if 1:        
        make_salesratio_adjust_value()        
        
