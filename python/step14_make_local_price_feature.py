'''
Created on Apr 6, 2018

@author: bo
'''
import numpy as np 
import os, sys
import fastparquet
import time

from  zillow import config, features
from zillow.task import Task
import traceback
import pandas as pd  
import dask.dataframe as dd 

params = dict(
    n_neighbors=10,
    d_half=1.0
    )


def f(j):
    t0 = time.time()
    global XY, tree, all_data, localdata
    print ("begin batch " + str(j))
    lst = XY[j]
    _, ret = tree.query(lst[['longitude', 'latitude']].values, k=params['n_neighbors'])
    distances = []
    for u, l in enumerate(ret):
        x, y, lot, building = lst.iloc[u].values[:4]
        neighbors = localdata.iloc[l]
        nx = neighbors['x'].values 
        ny = neighbors['y'].values 
        nbeta1 = neighbors['beta1'].values
        nbeta2 = neighbors['beta2'].values
        d2 = np.sqrt((nx - x) ** 2 + (ny - y) ** 2) * 1e3  # about 1km
        decay = np.log(0.5) / params['d_half']
        weights = np.exp(decay * d2) + 1e-7
        weights = weights / np.sum(weights)

        beta1 = np.sum(weights * nbeta1)
        beta2 = np.sum(weights * nbeta2)
        estimated = beta1 * lot + beta2 * building 
        
        tmpres = np.array([beta1, beta2, estimated])
        distances.append(tmpres)
        # print np.round(weights,3), np.round(tmpres,3) 
    t1 = time.time()
    print ("end batch {}, cost {} ".format(j, t1 - t0))
    features = np.array(distances).astype(np.float32)
    
    return features 


def _fun_(state, year, logger):

    def _make_data():
        localdata = dd.read_parquet(os.path.join(config.get_state_year_folder(state, year), 'local_beta_100.parq')).compute()
        assert np.sum(localdata.isnull().values) == 0
        logger.info("\n%s", localdata.head())

        if 0:
            all_data = features.get_features(['longi_lati', 'lotsizesquarefeet', 'finishedsquarefeet'], state, year)
        else:
            all_data = features.get_features(['longi_lati', 'lotsizesquarefeet', 'finishedsquarefeet', 'sale_price_tract'], state, year)
            all_data['price'] = all_data['yb_{}_tract_saleprice'.format(year)]
            all_data = all_data.drop('yb_{}_tract_saleprice'.format(year), axis=1)

        all_data = all_data[~all_data['longitude'].isnull()]
        all_data = all_data[~all_data['latitude'].isnull()]
        logger.info("\n%s", all_data.head())
        logger.info('length {},{}'.format(len(localdata), len(all_data)))
        logger.info("\n%s", all_data.isnull().mean())
        return localdata, all_data
        
    global XY, tree, all_data, localdata
    localdata, all_data = _make_data()

    from scipy.spatial import cKDTree
    from multiprocessing import Pool

    tree = cKDTree(localdata[['x', 'y']].values)
    logger.info("start to calc...")
    i = 0
    XY = []
    while i < all_data.shape[0]:
        XY.append(all_data[i:i + 20000])
        i += 20000
    logger.info("#batch: " + str(len(XY)))
    if 1:
        p = Pool(16)
        params = list(range(len(XY)))
        print params
        ret = p.map(f, params)
        p.close()
        p.join()
    else:
        ret = [ f(k) for k in range(len(XY))]
    b = np.concatenate(ret)
    df = pd.DataFrame(b, columns=['beta1', 'beta2', 'estprice'], index=all_data.index).astype(np.float32)
    logger.info('\n%s', df.head())
    logger.info("max,min=%s,%s", np.max(df), np.min(df))
    def g(u):
	v=np.nanmean(u)
        u[u>0.4]=0.4
        return np.nanmean(u),v
    logger.info("score = %s", str(g(np.abs(np.log(df['estprice']) - np.log(all_data['price'])))))
    logger.info("aaa   = %s", str(g(np.abs(np.log(all_data['price']) - np.nanmean(np.log(all_data['price']))))))
    
    outfile = os.path.join(config.get_state_year_folder(state, year), 'local_beta_100_feat.parq')
    fastparquet.write(outfile, df, compression='SNAPPY')


def make_local_price_features(astate, ayear):
    states = config.STATES if astate is None else [astate]
    years = config.YEARS if ayear is None else [ayear]    
    for state in states:
        for year in years :
            try:            
                task = Task('make_local_price_features-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
                task()
            except:
                # traceback.print_exc()
                raise
            

def main(state=None, year=None):
    make_local_price_features(state, year)

                                        
if __name__ == '__main__':
    state = None
    year = None 
    if len(sys.argv) >= 2: 
        state = sys.argv[1]
    if len(sys.argv) >= 3: 
        year = sys.argv[2]        
    if len(sys.argv) >= 4: 
        params['n_neighbors'] = int(sys.argv[3])
    if len(sys.argv) >= 5: 
        params['d_half'] = float(sys.argv[4])        
    if len(sys.argv) <= 5:
        main(state, year)
    else:
        raise Exception("argv error")

