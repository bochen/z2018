'''
Created on Apr 6, 2018

@author: bo
'''
import numpy as np 
import os
import fastparquet

from  zillow import config, utils, features, info
from zillow.task import Task


def _fun_(state, year, logger, r , colname): 
        from scipy.spatial import cKDTree       
        df=features.get('longi_lati',state,year)
        tree = cKDTree(df.values)
        logger.info("start to calc...")
        ret = tree.query_ball_tree(tree, r=r, p=1)
        logger.info("Finish calc...")
        b = np.array([len(u) for u in ret])
        b=np.log10(b)
        logger.info("max,min=%f,%f",np.max(b),np.min(b))
        df[colname] = (b / np.max(b)).astype(np.float32)
        df = df[[colname]]
        outfile = os.path.join(config.get_state_year_folder(state, year), colname + '.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')


XY=None
def f(jr):
	j,r=jr
	print j,r
	global XY, tree
	print ("begin batch "+str(j))
   	lst=XY[j]
       	ret = tree.query_ball_point(lst, r=r, p=1)
       	b = np.array([len(u) for u in ret])
      	print ("end batch "+str(j))
        return b
def _fun2_(state, year, logger, r , colname): 
        from scipy.spatial import cKDTree
        #from joblib import Parallel, delayed       
	from multiprocessing import Pool

	global XY,tree
        df=features.get('longi_lati',state,year)
        tree = cKDTree(df.values)
        logger.info("start to calc..., #data %d", len(df))
        i=0
        XY=[]
        while i<df.shape[0]:
            XY.append(df.iloc[i:i+20000])
            i+=20000
        logger.info("#batch: "+str(len(XY)))
        #ret=Parallel(n_jobs=16)(delayed(f)(j) for j in range(len(XY)))
        p = Pool(16)
	ret = p.map(f, enumerate( [r] *len(XY)))
        #p.join()
	p.close()

        b=np.concatenate(ret)
        logger.info("Finish calc...")
        b=np.log10(b)
        logger.info("max,min=%f,%f",np.max(b),np.min(b))
        df[colname] = (b / np.max(b)).astype(np.float32)
        df = df[[colname]]
        outfile = os.path.join(config.get_state_year_folder(state, year), colname + '.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')



def make_count_density():

    for state in config.STATES:
        for year in ['share']:
            task = Task('make_count_density-{}-{}'.format(state, year), lambda u: _fun_(state, year , u, r=0.01 / 10, colname='count_density'))
            task()
    for state in config.STATES:
        for year in ['share']:
            task = Task('make_count_density2-{}-{}'.format(state, year), lambda u: _fun_(state, year , u, r=0.01 / 3, colname='count_density2'))
            task()
    for state in config.STATES:
        for year in ['share']:
            task = Task('make_count_density3-{}-{}'.format(state, year), lambda u: _fun2_(state, year , u, r=0.01 / 1, colname='count_density3'))
            task()

def main():
    make_count_density()        

                                    
if __name__ == '__main__':
    main()
