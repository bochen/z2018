'''
Created on Apr 6, 2018

@author: bo
'''
import dask.dataframe as dd 
import numpy as np 
import pandas as pd 
import os
import logging
import fastparquet
import gc
import cPickle as pickle 
import geopandas as gpd

from  zillow import config, utils, features, info
from zillow.task import Task


def make_finished_sqft_feat():
    
    def _fun_(state, year, logger):
        filename = os.path.join(config.get_state_year_folder(state, year), 'home_attrib.parq')
        assert utils.file_exists(filename), filename
        cols = [
         'finishedsquarefeet',
         'finishedsquarefeet12',
         'finishedsquarefeet50',
         'finishedsquarefeet6',
         'finishedsquarefeet10',
         'finishedsquarefeet15',
         'finishedsquarefeet11',
         'finishedsquarefeet9',
         'finishedsquarefeet13',
         'finishedsquarefeet7',
         'finishedsquarefeet8',
         'finishedbasementsqft',
          ] 
            
        df = dd.read_parquet(filename, columns=cols).compute()
        logger.info("null mean/#all: %s/%s", df['finishedsquarefeet'].isnull().mean(), len(df))
        N = float(len(df))
        feat = df['finishedsquarefeet'].dropna()
        df = df[df['finishedsquarefeet'].isnull()]
        logger.info("col={}, %nonnil={}".format('finishedsquarefeet', len(feat) / N))
        for col in cols[1:]:
            v = df[col].dropna()
            if len(v) > 0:
                feat=pd.concat([feat,v])
                feat.name='finishedsquarefeet'
                logger.info("col={}, {}, %nonnil={}".format(col, len(v), len(feat) / N))
                df = df[df[col].isnull()]
                if len(df) == 0: 
                    break
        df = feat.to_frame()
        logger.info("\n%s", df.head())
        outfile = os.path.join(config.get_state_year_folder(state, year), 'finishedsquarefeet.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')
       
    for state in config.STATES:
    # for state in ['06']:
        for year in config.YEARS:
        # for year in [2017]:
            task = Task('make_finished_sqft_feat-{}-{}'.format(state, year), lambda u: _fun_(state, year, u))
            task()


def make_lotsizesquarefeet():
    
    def _fun_(state, year, logger):
        filename = os.path.join(config.get_state_year_folder(state, year), 'home_attrib.parq')
        assert utils.file_exists(filename), filename
        cols = [
         'lotsizesquarefeet',
         'lotsizeirregularity',
         'lotsizedepthfeet',
         'lotsizefrontagefeet',
          ] 
            
        df = dd.read_parquet(filename, columns=cols).compute()
        logger.info("null mean/#all: %s/%s", df['lotsizesquarefeet'].isnull().mean(), len(df))
        N = float(len(df))
        
        def g(u):
            if u == "nan":
                return np.nan
            u = u.lower()#.replace('irr', "")
            try:
                u = u.split("x")
                u = [v.strip() for v in u]
                u = [v for v in u if  len(v) > 0]
                if len(u) < 2: 
                    return np.nan        
                u = [float(v) for v in u]
            except:
                print u
                return np.nan
            assert(len(u) == 2)
            return u[0] * u[1]

        df['lotsizeirregularity_sqft'] = df['lotsizeirregularity'].map(g)
        df['depthXrontage_sqft'] = df['lotsizedepthfeet'] * df['lotsizefrontagefeet']
        finishsqft = features.get('finishedsquarefeet', state, year)['finishedsquarefeet'].to_dict()
        df['finishedsquarefeet'] = df.index.map(lambda u: np.nan if not u in finishsqft else finishsqft[u])
        df['lotsizesquarefeet']=df[['lotsizesquarefeet','finishedsquarefeet']].max(axis=1)
        df = df.drop([ 'lotsizeirregularity', 'lotsizedepthfeet', 'lotsizefrontagefeet'], axis=1)
        feat = df['lotsizesquarefeet'].dropna()
        df = df[df['lotsizesquarefeet'].isnull()]
        logger.info("col={}, %nonnil={}".format('lotsizesquarefeet', len(feat) / N))

        for col in ['depthXrontage_sqft', 'lotsizeirregularity_sqft']:
            v = df[col].dropna()
            if len(v) > 0:
                feat=pd.concat([feat,v])
                feat.name='lotsizesquarefeet'
                
                logger.info("col={}, {}, %nonnil={}".format(col, len(v), len(feat) / N))
                df = df[df[col].isnull()]
                if len(df) == 0: 
                    break
        df = feat.to_frame()
        logger.info("\n%s", df.head())
        outfile = os.path.join(config.get_state_year_folder(state, year), 'lotsizesquarefeet.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
    # for state in ['06']:
        for year in config.YEARS:
        # for year in [2017]:
            task = Task('make_lotsizesquarefeet-{}-{}'.format(state, year), lambda u: _fun_(state, year, u))
            task()


def make_yardbuildingsqft():
    
    def _fun_(state, year, logger):
        filename = os.path.join(config.get_state_year_folder(state, year), 'home_attrib.parq')
        assert utils.file_exists(filename), filename
        cols = ['yardbuildingsqft18',
             'yardbuildingsqft12',
             'yardbuildingsqft14',
             'yardbuildingsqft26',
             'yardbuildingsqft20',
             'yardbuildingsqft2',
             'yardbuildingsqft1',
             'yardbuildingsqft7',
             'yardbuildingsqft6',
             'yardbuildingsqft5',
             'yardbuildingsqft4',
             'yardbuildingsqft9',
             'yardbuildingsqft23',
             'yardbuildingsqft27',
             'yardbuildingsqft28',
             'yardbuildingsqft29',
             'yardbuildingsqft31',
             'yardbuildingsqft3',
             'yardbuildingsqft8',
             'yardbuildingsqft22',
             'yardbuildingsqft19',
             'yardbuildingsqft21',
             'yardbuildingsqft13',
             'yardbuildingsqft11',
             'yardbuildingsqft10',
             'yardbuildingsqft17',
             'yardbuildingsqft16',
             'yardbuildingsqft24',
             'yardbuildingsqft25',
             'yardbuildingsqft30']
            
        df = dd.read_parquet(filename, columns=cols).compute()
        df = df.sum(axis=1)
        df[df == 0] = np.nan 
        df.name = 'yardbuildingsqft'
        logger.info("null mean/#all: %s/%s", df.isnull().mean(), len(df))
        df = df.to_frame()
        logger.info("\n%s", df.head())
        outfile = os.path.join(config.get_state_year_folder(state, year), 'yardbuildingsqft.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
#     for state in ['06']:
#         for year in [2017]:
            task = Task('make_yardbuildingsqft-{}-{}'.format(state, year), lambda u: _fun_(state, year, u))
            task()


def make_garagesqft():
    
    def _fun_(state, year, logger):
        filename = os.path.join(config.get_state_year_folder(state, year), 'home_attrib.parq')
        assert utils.file_exists(filename), filename
        cols = ['garagedetachedsqft', 'garagetotalsqft', 'garagecarcnt', 'garageattachedsqft']
            
        df = dd.read_parquet(filename, columns=cols).compute()
        for col in cols:
            df.loc[df[col] == 0, col] = np.nan
        logger.info("null mean/#all: %s\n%s", len(df), df.isnull().mean().sort_values())
        logger.info("\n%s", df.head())
        outfile = os.path.join(config.get_state_year_folder(state, year), 'garagesqft.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
#     for state in ['06']:
#         for year in [2017]:
            task = Task('make_garagesqft-{}-{}'.format(state, year), lambda u: _fun_(state, year, u))
            task()


def make_buildingconditiontypeid():
    
    def _fun_(state, year, logger):
        filename = os.path.join(config.get_state_year_folder(state, year), 'home_attrib.parq')
        assert utils.file_exists(filename), filename
        cols = ['buildingconditiontypeid']
            
        df = dd.read_parquet(filename, columns=cols).compute()
        m = {
            2.0: 1,
            4.0:2,
            1.0:3,
            3.0:4,
            5.0:5,
            6.0:5
            }
        df.loc[~df['buildingconditiontypeid'].isin(m)] = np.nan 

        def g(u):
            if u in m:
                return m[u]
            else: 
                return u 

        df['buildingconditiontypeid'] = df['buildingconditiontypeid'].map(g).astype(np.float32)
        logger.info("null mean/#all: %s/%s", len(df), df['buildingconditiontypeid'].isnull().mean())
        logger.info("\n%s", df.head())
        outfile = os.path.join(config.get_state_year_folder(state, year), 'buildingconditiontypeid.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
#     for state in ['06']:
#         for year in [2017]:
            task = Task('make_buildingconditiontypeid-{}-{}'.format(state, year), lambda u: _fun_(state, year, u))
            task()


def make_other_attribs():
    
    def _fun_(state, year, logger):
        
        filename = os.path.join(config.get_state_year_folder(state, year), 'home_attrib.parq')
        assert utils.file_exists(filename), filename
        cols = [
        'poolsizesum',
        'poolcnt',
        
            'fullbathcnt',
        'bathroomcnt',
        'calculatedbathnbr',
        
        'effectiveyearbuilt',
         'yearbuilt',
         'yearremodelled',
         
         'roomcnt',
         'unitcnt',
         'garagecarcnt',
         'bedroomcnt',
         'fireplacecnt',
         
         'numberofstories',
   
        ] + [
        'poolmaintypeid',
        'lotsiteappeals',
        'architecturalstyletypeid',
         'roofstructuretypeid',
         'decktypeid',
         'heatingorsystemtypeid',
         'typeconstructiontypeid',
         'watertypeid',
         
         'airconditioningtypeid',
         'lotsizetopographytypeid',
         'foundationtypeid',
         'sewertypeid',
         'roofcovertypeid',
         'storytypeid',
    ]
            
        df = dd.read_parquet(filename, columns=cols).compute()
        for col in cols:
            r = df[col].isnull().mean()
            df.loc[df[col] == 0, col] = np.nan
            logger.info("%s: %s -> %s", col, r, df[col].isnull().mean()) 
        logger.info("\n%s", df.head())
        outfile = os.path.join(config.get_state_year_folder(state, year), 'attrib_others.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
#     for state in ['06']:
#         for year in [2017]:
            task = Task('make_other_attribs-{}-{}'.format(state, year), lambda u: _fun_(state, year, u))
            task()

                                                
if __name__ == '__main__':
    if 1:       
        make_other_attribs()
        make_buildingconditiontypeid()
        make_garagesqft()
        make_yardbuildingsqft() 
        make_finished_sqft_feat()        
        make_lotsizesquarefeet()
        
