'''
Created on Apr 6, 2018

@author: bo
'''
import dask.dataframe as dd 
import numpy as np 
import pandas as pd 
import os
import logging
import fastparquet
import gc
import cPickle as pickle 
import geopandas as gpd

from  zillow import config, utils, features, info
from zillow.task import Task


def make_tf_input():

    def _fun_(state, logger):
        lst = []
        for year in config.YEARS:
            df = features.get_features(['lotsizesquarefeet', 'finishedsquarefeet',
                           'garagetotalsqft', 'county', 'longi_lati', 'sale_price_zip5'], 
                                         state, year, reset_index=False)
            col='yb_{}_zip5_saleprice'.format(year)
            df['sale_price']=df[col]
            df['year'] = year
            df=df.drop(col,axis=1)
            lst.append(df)
        df = pd.concat(lst, axis=0)
        
        logger.info("\n%s", df.head())
        logger.info("\n%s", df['year'].value_counts())
        logger.info("#nonnull sales=%s", df['sale_price'].dropna().shape[0])
        outfile = os.path.join(config.get_state_year_folder(state, 'share'), 'tf_input.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')
       
    for state in config.STATES:
        task = Task('make_tf_input-{}'.format(state), lambda u: _fun_(state, u))
        task()

                                    
if __name__ == '__main__':
    if 1:        
        make_tf_input()        
        
