'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils, features
import os
from zillow.task import Task
import pandas as pd
import sys
import fastparquet

 
def show_all(df):
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(df)     


param_hashes = {'39':'52be05cfb4cf3ba47c6a2f8be8f09203cd2c3475',
                 '37': '2b046f925450ed7c2048c44235a970a154d66be2',
                 '36': '5b62669c75c287c89964c1325e64a8087734805c',
                 '19': '8eb0da1ad91804eda3e9c133947a81f27ef38a41',
                 '06':'5b62669c75c287c89964c1325e64a8087734805c'}


def get_prediction_result_files(state, year):
    lst = []
    for i in range(1, 6):
        a = 'lgbraw_{}-{}-{}.param-f{}.model'.format(state, year, param_hashes[state], i)
        fname = os.path.join(config.get_state_year_folder2(state, year), a)
        lst.append(fname)
    return lst 


def shell_run_and_wait(command):
    command = command.split(" ")
    import subprocess
    #process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process = subprocess.Popen(command)
    process.wait()
    return process.returncode


def run_lgb(state, year, logger):
    if "TOTAL_CORES" in os.environ:
        n_thread = int(os.environ["TOTAL_CORES"])-2
    else:
        n_thread = 24
    cmd = "python {}/python/zillow/lgb_run5.py --prefix=lgbraw --param={}.param --year={} --state={} --seed={} --nthread={} --nround={} --with_month --use_rawprice --save_model".format(
        config.HOME, param_hashes[state], year, state, int(state) * 10000 + year, n_thread, 3000)
    logger.info("cmd: %s", cmd)
    code = shell_run_and_wait(cmd)
    assert code == 0, (code, cmd)
    for fname in get_prediction_result_files(state, year):
        assert utils.file_exists(fname), fname


       
def run(state, year, logger):
    task = Task('make_lgbraw_model-{}-{}'.format(state, year), lambda u: run_lgb(state, year , u))
    task()

if __name__ == '__main__':
    assert len(sys.argv) == 2
    state = sys.argv[1]
    #for year in [2017,2018]:
    for year in [2018]:        
        run(state,year,None)
