
# hardware and software

* I have about ten small nodes, which are in various configuration 
* Hardware: from 4 cores to 16 cores, memory from 8G to 96G, all have SSD disks (two of them have TitanX gpu, but I finally decided not using my tensorflow models, so GPU is not required) 
* OS: Ubuntu 14 or 16. 
* Software: Anaconda Python 2.7 and R (refer to dockfile for details)

# How to train your model

refer to `entry_point.md`

# How to make predictions on a new test set
refer to FAQ in `entry_point.md`
   
# Important side effects of your code
No. 

# Key assumptions made by your code
No. The only thing is to put the zip files in the right folder as being posted in `entry_point.md`. All others are taken care of by code.

#Third party data

* FHFA HPI index: <https://www.fhfa.gov/DataTools/Downloads/Pages/House-Price-Index.aspx>

* sales ratio study: 
<https://tax.iowa.gov/reports> , <https://www.ncdor.gov/documents/sales-ratio-multiple-years> and <https://www.tax.ohio.gov/tax_analysis/tax_data_series/publications_tds_property.aspx>

* TIger/Line shape files: <https://www.census.gov/geo/maps-data/data/tiger-line.html>

* NYC avroll data: <https://www1.nyc.gov/site/finance/taxes/property-assessments.page>

* minor others: FIPS code mapping, zip code mapping etc  

# Known issues or caveats

* `step2` has bug that make 38443 parcels missed.
* The data preparation/cleaning is based on `v1` data. When I blindly updated it with `v4` data, there are a few of parcels that have no census tract information which do have with `v1` data. Definitely there are bugs somewhere. But I just hardcoded them in source file. see `features.py` and `info.py`   
* Codes not included in `run.sh`

	 * clean/extrapolate HPI(find them in 2_jupyter). It downloads latest data from HFHA website, rerunning it which will make the results unreproducible. 
	 * `4_censusblock_to_lati_longi` in `1_jupyter` is not included in `run.sh`. It fixes a few geometric features (only about a thousand, very minor) based on `home_attributes_2017_v1.csv`  
	 * The above two totally costs less than an hour to run.
 
 
* model files

	The main model is of lightgbm. 

	LGB model files are attached in the submission, which is a 12GB 7z file. 
	
	I also train a few of R gam models. They are finally only used by `step33`, which produce a minor feature,  to filter out a few parcels (Actually I can do this by lgb model. however since my submission is in that way, I have to include them). I tried saving the models to disk, but it costs 5GB for each (R saves train data, errors, matrix decomposition etc), totally requires 300+GB disk space. I googled it but cannot find a way to reduce the model size , so I decided not to save them.

    
 