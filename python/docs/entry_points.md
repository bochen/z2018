
# Prerequisite

* the code heavily use disk to store intermediate data between tasks. So a SSD disk saves life.
* check if `$ZILLOW_HOME` (set to the source folder) is set.
* put zillow's zip files to `$ZILLOW_HOME/raw_input`. On my notebook it looks like:

	-rw------- 1 bo bo 5311654171 Apr 29 01:05 input/raw_input/home_attributes_history.zip
	-rw------- 1 bo bo  613373684 Apr 29 01:06 input/raw_input/saleprice_v3.zip
	-rw------- 1 bo bo   32648845 Apr 29 01:06 input/raw_input/sample_submission.csv.zip
	-rw------- 1 bo bo 1725141130 Apr 29 01:06 input/raw_input/tax_history_2007to2017_v2.csv.zip
	-rw------- 1 bo bo       8024 Apr 29 01:06 input/raw_input/zillow_tax_data_dictionary_round2_v2.xlsx.zip
	-rw------- 1 bo bo      32273 Apr 29 01:06 input/raw_input/zillow_salesprice_data_dictionary_round2_v3.xlsx.zip
	-rw------- 1 bo bo      32874 Apr 29 01:06 input/raw_input/zillow_home_feature_data_dictionary_round2_v2.xlsx.zip
	-rw-rw-r-- 1 bo bo  665320907 Jul  9 14:36 input/raw_input/saleprice_v4.zip
	-rw-rw-r-- 1 bo bo 1109457342 Jul  9 14:54 input/raw_input/home_attributes_history_2017and2018.zip
	-rw-rw-r-- 1 bo bo 1843280416 Jul  9 16:15 input/raw_input/tax_history_2007to2018_v4.zip 
* clean task flags if existing

	rm $ZILLOW_HOME/input/task/*
	
# How to run (in the context of competition)

	# on host node
	# assume $ZILLOW_HOME is the source folder, $DOCKER_HOME is where the Dockerfile locates
	# assume host is linux
	
	#build docker
	cd $DOCKER_HOME; docker build -t this_image .
	
	#start docker
	# I prefer to mount the the source and data folders to the container instead of putting them inside the container 	
	sudo chown -R 1001:1001 $ZILLOW_HOME  
	CID=$(docker run -p 2222:22 -t -d --name mydev -v $ZILLOW_HOME:/zillow2018 this_image /run.sh)
	
	#login container and run 
	ssh -l bo -p 2222 localhost #password is same as user name
	#(inside container)
	cd /zillow2018/script; bash run.sh
	
	#logout and stop 
	docker stop $CID
	sudo chown -R $UID:$(id -g) $ZILLOW_HOME

The `run.sh` will run tasks of the process flow one by one. There are thousands of tasks to be finished (mine is 4217, but yours will be less, since I trimmed many of them that is unused in final submission).

Hopefully there is no error. Bad news is that since I don't have a 128-core node some parallel code is not verified thoroughly. I manually ran the steps and fix errors one by one during competition. I only partially tested docker version code (e.g. for several years). Good news is that the tasks is designed to be atomic and  when a task succeeds, it will skip running when invoking `run.sh` again.   

# FAQ

### How long will it run?
* I never ran the whole process together. But it should be less than 2 days, no more than 3 days. 
* One of the most time consuming steps, `step40`, needs 1272 cpu hours. 
* Another one, `step10`, needs 500 cpu hours (R with MKL) to finish; It may take 1000 cpu hours with the docker image since the docker uses OpenBLAS which is twice slower when I tested.  

### I have up-to-date data of 2018
* just put the updated zip file in `raw_input`
* however if the file names or paths in the zip files change, modify `step0a`.
* also check third part data if it is necessary to be updated.  
	
### I have new data beyond 2018, e.g. 2019	
* put the new zip file in `raw_input`
* modify `step0a`
* modify `config.py` to include the new years
* also check third part data if they need to be updated.  

### predict months other than Sep and Oct or other than 2018.
* check step30, step40 and step50. To save time, these steps only work on 2018 and predict for Sep and Oct.

### I want to predict on new dataset without training
* It is not easy. The code is not designed for this purpose. All the models are in a train-then-predict-then-save_prediction flavor.
* Although model files are provided, they probably only prove that the submissions are truly from models. Actually the model files are not necessary to be saved at all. 
* There are a long data preprosessing workflow and several model levels, which makes it very hard.  I preferred run all currently. 

### I want to include new states
* well, it is beyond the scope since models for any two states of the 5 states are not identical.
	
### re-run a certain task
* remove the task flag file in `input/task` folder, then run `run.sh`
* But notice that it does not handle task dependency. The down stream will not re-run unless their flag files are also removed. 	
	
	  
	
	
		
	
