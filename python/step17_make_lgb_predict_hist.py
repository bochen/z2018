'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils
import dask.dataframe as dd 
import numpy as np 
import os
import gc
from zillow.task import Task
import fastparquet
from zillow import features 
import pandas as pd
import traceback
from multiprocessing import Pool
import sys

 
def show_all(df):
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(df)     


def _fun_(state, year, logger, prefix):

    def read_predict(year):
        hashes = {'39':'52be05cfb4cf3ba47c6a2f8be8f09203cd2c3475',
                 '37': '2b046f925450ed7c2048c44235a970a154d66be2',
                 '36': '5b62669c75c287c89964c1325e64a8087734805c',
                 '19': '8eb0da1ad91804eda3e9c133947a81f27ef38a41',
                 '06':'5b62669c75c287c89964c1325e64a8087734805c'}
        df = []
        for i in range(1, 6):
            a = '{}_{}-{}-{}.param-f{}.parq'.format(prefix, state, year, hashes[state], i)
            fname = os.path.join(config.get_state_year_folder2(state, year), a)
            df.append(dd.read_parquet(fname).compute())
        lst = []
        for col in df[0].columns:
            s = pd.concat([u[col] for u in df], axis=1)
            s = s.mean(axis=1)
            s.name = col
            lst.append(s)
        ret = pd.concat(lst, axis=1)
        logger.info("head for year %d\n%s", year, ret.head())
        return ret 
        
    def get_lgb_predict_hist():
        years = [u for u in config.YEARS if u < year]
        years = sorted(list(years))
        print years
        df = []
        for y in years:
            tmpdf = read_predict(y)['pred'].to_frame()
            tmpdf.columns = ['{}_pred_{}'.format(prefix, y)]
            df.append(tmpdf)
        if len(df) == 0: return pd.DataFrame()
        df = pd.concat(df, axis=1)
        df2 = features.hpi_discount(df, state, years, year, 'tract')
        if len(years) > 1:
            for i in set([2, 3, 5, len(years)]):
                if i > len(years): continue            
                df['med{}_hpi_{}_predict'.format(i, prefix)] = df2[df2.columns[-i:]].median(axis=1)
                df['max{}_hpi_{}_predict'.format(i, prefix)] = df2[df2.columns[-i:]].max(axis=1)
                df['min{}_hpi_{}_predict'.format(i, prefix)] = df2[df2.columns[-i:]].min(axis=1)
        return pd.concat([df, df2], axis=1).astype(np.float32)
    
    df = get_lgb_predict_hist()
    logger.info("len(df)=%s\n%s", len(df), df.head())
    
    assert df.shape[1] == len(set(df.columns))
    
    print "df min-max"
    show_all(pd.concat([df.min(), df.max()], 1))
    print"df info"
    show_all(df.info(verbose=False))
    
    print "df null mean"
    show_all(df.isnull().mean())
    
    outfile = os.path.join(config.get_state_year_folder2(state, year), '{}_predict_hist.parq'.format(prefix))
    fastparquet.write(outfile, df, compression='SNAPPY')


def afun(prefix_state_year):
    prefix, state, year = prefix_state_year
    task = Task('make_{}_predict_hist-{}-{}'.format(prefix, state, year), lambda u: _fun_(state, year , u, prefix))
    try:
        task()
    except:
        traceback.print_exc()
        print 'error when run', state, year 
        raise

        
def make_lgb_predict_hist(prefix, states, years):
    lst = []
    for state in states:
        for year in years:
            lst.append([prefix, state, year])
    
    if len(lst) > 1:
        p = Pool(min(16, len(lst)))
        p.map(afun, lst)
        p.close()
        p.join()
    else:
        for u in lst:
            afun(u)

                  
if __name__ == '__main__':
    if len(sys.argv) == 4:
        states = [sys.argv[1]]
        years = [int(sys.argv[2])]
        prefix = sys.argv[3]
    else:
        states = config.STATES
        years = config.YEARS
        prefix = 'lgb' 
    make_lgb_predict_hist(prefix, states, years)
