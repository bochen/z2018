'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils
import os
from zillow.task import Task

def unzip_raw_data():
    import zipfile

    def _fun_(logger):
        files = [
            'home_attributes_history.zip',
             "zillow_home_feature_data_dictionary_round2_v2.xlsx.zip",
             "saleprice_v4.zip",
             "zillow_salesprice_data_dictionary_round2_v3.xlsx.zip",
             "sample_submission.csv.zip",
             "zillow_tax_data_dictionary_round2_v2.xlsx.zip",
             "tax_history_2007to2018_v4.zip",
             "home_attributes_history_2017and2018.zip"
             ]
        for fname in files:
            fname = os.path.join(config.RAW_INPUT_PATH, fname)
            assert utils.file_exists(fname), fname 
        
        for fname in files:
            if fname == "home_attributes_history_2017and2018.zip":
                is_special=True
            else:
                is_special=False

            fname = os.path.join(config.RAW_INPUT_PATH, fname)
            logger.info("unzip %s", fname)
            zip_ref = zipfile.ZipFile(fname, 'r')
            if is_special:
                zip_ref.extractall(os.path.join(config.RAW_INPUT_PATH, 'home_attributes_history'))
            else:
                zip_ref.extractall(config.RAW_INPUT_PATH)
            zip_ref.close()
        afile=os.path.join(config.RAW_INPUT_PATH,'home_attributes_history','home_attributes_2017_v1.csv')
        if utils.file_exists(afile): os.remove(afile)

    task = Task('unzip_raw_data', _fun_)
    task()
                        
            
if __name__ == '__main__':
    unzip_raw_data()
