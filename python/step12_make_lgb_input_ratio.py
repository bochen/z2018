'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config
import numpy as np 
import os
import gc
from zillow.task import Task
import fastparquet
from zillow import features 
import pandas as pd
import traceback
from zillow.pmean import col_median

 
def show_all(df):
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(df)     

         
def make_various_ratio_hist():
    
    def _fun_(state, year, logger):

        def make_ratio(col1, col2, col3):
	    ret=_make_ratio(col1,col2,col3)
            gc.collect()
	    return ret

        def _make_ratio(col1, col2, col3):
            logger.info("starts {}/{}={}".format(col1, col2, col3))
            years = [u for u in config.YEARS if u <= year]
            years = sorted(list(years))
            print years
            df = []
            names = [col1, col2]
            for y in years:
                tmpdf = features.get_features(names, state, y)
                tmpdf[col3 + str(y)] = tmpdf[col1] / tmpdf[col2]
                df.append(tmpdf[col3+str(y)])
            
            if len(df) == 0: return pd.DataFrame()        
            df = pd.concat(df, axis=1)

            if len(years) > 1:            
                tmpdf = df.copy()
                for i in set([2, 3, 5, len(years)]):
                    if i > len(years): continue
                    name = col3
                    df['med{}_{}'.format(i, name)] = col_median(tmpdf[tmpdf.columns[-i:]])
                    df['max{}_{}'.format(i, name)] = tmpdf[tmpdf.columns[-i:]].max(axis=1)
                    df['min{}_{}'.format(i, name)] = tmpdf[tmpdf.columns[-i:]].min(axis=1)
               
            df = df.astype(np.float32)
            logger.info("len(df)=%s\n%s", len(df), df.head())            
            return df

        lst = [
            make_ratio('landtaxvaluedollarcnt', 'unitcnt', "tlv_div_uc"),
            make_ratio('landtaxvaluedollarcnt', 'roomcnt', "tlv_div_rc"),
            make_ratio('landtaxvaluedollarcnt', 'finishedsquarefeet', "tlv_div_fsf"),
            make_ratio('landtaxvaluedollarcnt', 'lotsizesquarefeet', "tlv_div_lsf"),
            
            make_ratio('structuretaxvaluedollarcnt', 'roomcnt', "tsv_div_rc"),
            make_ratio('structuretaxvaluedollarcnt', 'bedroomcnt', "tsv_div_bc"),
            make_ratio('structuretaxvaluedollarcnt', 'finishedsquarefeet', "tsv_div_fsf"),
            
            make_ratio('taxvaluedollarcnt', 'unitcnt', "ttv_div_uc"),
            make_ratio('taxvaluedollarcnt', 'roomcnt', "ttv_div_rc"),
            make_ratio('taxvaluedollarcnt', 'lotsizesquarefeet', "ttv_div_lsf"),
            make_ratio('taxvaluedollarcnt', 'finishedsquarefeet', "ttv_div_fsf"),
            
            ]
        df = pd.concat(lst, axis=1).astype(np.float32)
        print "df null mean"
        dfmean = (df.isnull().mean())
        show_all(dfmean)
        df = df[df.columns[dfmean < 1]]
        
        print "df min-max"
        show_all(pd.concat([df.min(), df.max()], 1))
        print"df info"
        show_all(df.info(verbose=False))
        
        outfile = os.path.join(config.get_state_year_folder2(state, year), 'varous_ratio_hist.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_various_ratio_hist-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()            

                                    
def run(f):
    try:
        f()
    except:
        traceback.print_exc()
        raise


def run_null():
    pass 

                  
if __name__ == '__main__':

    functions = [
        make_various_ratio_hist,
        run_null,
        run_null,
        run_null,
        run_null,
        run_null,
        run_null] 
    
    [run(f) for f in functions]
