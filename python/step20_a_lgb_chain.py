'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils
import os
from zillow.task import Task
import pandas as pd
import sys

 
def show_all(df):
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        print(df)     


param_hashes = {'39':'52be05cfb4cf3ba47c6a2f8be8f09203cd2c3475',
                 '37': '2b046f925450ed7c2048c44235a970a154d66be2',
                 '36': '5b62669c75c287c89964c1325e64a8087734805c',
                 '19': '8eb0da1ad91804eda3e9c133947a81f27ef38a41',
                 '06':'5b62669c75c287c89964c1325e64a8087734805c'}


def get_prediction_result_files(state, year):
    lst = []
    for i in range(1, 6):
        a = 'lgbchain_{}-{}-{}.param-f{}.parq'.format(state, year, param_hashes[state], i)
        fname = os.path.join(config.get_state_year_folder2(state, year), a)
        lst.append(fname)
    return lst 


def shell_run_and_wait(command):
    command = command.split(" ")
    import subprocess
    #process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process = subprocess.Popen(command)
    process.wait()
    return process.returncode


def run_lgb(state, year, logger):
    cmd = "python {}/python/zillow/lgb_run5.py --prefix=lgbchain --param={}.param --year={} --state={} --seed={} --nthread={} --nround={} --save_model --make_predict".format(
        config.HOME, param_hashes[state], year, state, int(state) * 10000 + year, 24, 3000)
    logger.info("cmd: %s", cmd)
    code = shell_run_and_wait(cmd)
    assert code == 0, (code, cmd)
    for fname in get_prediction_result_files(state, year):
        assert utils.file_exists(fname), fname


def run_make_hist(state, year, logger):
    cmd = "python {}/python/step17_make_lgb_predict_hist.py {} {} lgbchain".format(config.HOME, state, year)
    logger.info("cmd: %s", cmd)
    code = shell_run_and_wait(cmd)
    assert code == 0, (code, cmd)
    histfile = os.path.join(config.get_state_year_folder2(state, year), 'lgbchain_predict_hist.parq')
    assert utils.file_exists(histfile)

        
def run(state, year, logger):
    if year > 2007:
        histfile = os.path.join(config.get_state_year_folder2(state, year), 'lgbchain_predict_hist.parq')
        assert utils.file_exists(histfile), histfile

    task = Task('train_lgb_chain_model-{}-{}'.format(state, year), lambda u: run_lgb(state, year , u))
    task()
    if year<2018:
        task = Task('make_lgb_chain_hist-{}-{}'.format(state, year+1), lambda u: run_make_hist(state, year+1 , u))
        task()
                                    
if __name__ == '__main__':
    assert len(sys.argv) == 2
    state = sys.argv[1]
    for year in config.YEARS:
        task = Task('make_lgb_chain_model-{}-{}'.format(state, year), lambda u: run(state, year , u))
        task()

