'''
Created on Apr 11, 2018

@author: bo
'''
import numpy as np 


def cubic_basis2(x, knots):

    def _cubic_basis(z):
        f = ((z - 0.5) ** 2 - 1.0 / 12) * ((x - 0.5) ** 2 - 1.0 / 12) / 4.0
        f = f - ((np.abs(x - z) - 1.0 / 2) ** 4 - 0.5 * (np.abs(x - z) - 1.0 / 2) ** 2 + 7.0 / 240) / 24.0
        return f

    return np.array([_cubic_basis(u) for u in knots], np.float64).T


def kronecker_product2(mat1, mat2):
  """Computes the Kronecker product two matrices."""
  m1, n1 = mat1.shape
  mat1_rsh = np.reshape(mat1, [m1, 1, n1, 1])
  m2, n2 = mat2.shape
  mat2_rsh = np.reshape(mat2, [1, m2, 1, n2])
  return np.reshape(mat1_rsh * mat2_rsh, [m1 * m2, n1 * n2])

def kronecker_product3(mat1, mat2):
  assert len(mat1) == len(mat2)
  m, n1 = mat1.shape
  mat1_rsh = np.reshape(mat1, [m, n1, 1])
  m, n2 = mat2.shape
  mat2_rsh = np.reshape(mat2, [m, 1, n2])
  return np.reshape(mat1_rsh * mat2_rsh, [m, n1 * n2])

  
def make_X(x, knots):
    a = cubic_basis2(x, knots)
    a0 = np.reshape(np.ones_like(x), [-1, 1])
    a1 = np.reshape(x, [-1, 1])
    return np.concatenate([a0, a1, a], axis=-1)


def make_S(knots):
    S = np.zeros(shape=[len(knots) + 2] * 2, dtype=np.float64)
    Y = cubic_basis2(knots, knots)
    Y = Y / np.sum(np.abs(Y))
    S[2:, 2:] = Y
    return S


def make_inv_gamma(knots):
    n = len(knots)
    x = (np.arange(n + 2) / float(n + 2))[1:-1]
    G = cubic_basis2(x, knots).T
    G = np.linalg.pinv(G)
    return G / np.sum(np.abs(G))

    
def make_tp(xlist, knots_lst):
    assert len(xlist) == len(knots_lst)
    Xlist = [make_X(xlist[j], knots_lst[j]) for j in  range(len(xlist))]
    X = reduce(lambda u, v: kronecker_product3(u, v), Xlist)
    
    assert X.dtype == np.float64, X.dtype
    if 1:
        Slist = []
        Ilst = [np.eye(len(knots) + 2) for knots in knots_lst]
        for i in range(len(knots_lst)):
            knots = knots_lst[i]
            inv_gamma = make_inv_gamma(knots)
            S = make_S(knots)
            s2 = np.matmul(np.matmul(inv_gamma.T, S[2:, 2:]), inv_gamma)
            s2 = s2 / np.sum(np.abs(s2))
            S[2:, 2:] = s2
            this_Ilist = list(Ilst)
            this_Ilist[i] = S
            S = reduce(lambda u, v: kronecker_product2(u, v), this_Ilist)
            Slist.append(S.astype(np.float64))
    else:
        Slist = None 
    return X, Slist


class TsItem(object):

    def __init__(self, name, xlist, knots_list, smooth, return_S):
        assert len(xlist) == len(knots_list)
        if isinstance(smooth, (int, long)):
            smooth = np.array([smooth] * len(xlist), np.float64)
        else:
            assert len(smooth) == len(xlist)
        for knots in knots_list:
            assert np.max(knots) < 1 and np.min(knots) > 0
        self.name = name
        self.smooth = smooth      
        self.xlist = xlist
        self.knots_list = knots_list
        self.return_S = return_S
        self.make_model()
    
    def make_model(self):
        smooth = self.smooth
        if 1: 
            xlist = self.xlist
            self.var_X, self.Slist = make_tp(xlist, self.knots_list)
            self.n = self.var_X.shape[1]
            # print "{} has {} variables".format(self.name, self.n)
            # self.beta = tf.Variable(tf.random_normal([self.n, 1]), name=self.name)
            if self.return_S:
                # self.X=tf.stack(xlist,axis=1)
                sum_S = 0
                for i in range(len(smooth)):
                    sum_S = smooth[i] * self.Slist[i]
                self.sum_S = sum_S

