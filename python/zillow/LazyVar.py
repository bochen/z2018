'''
Created on Jun 20, 2018

@author: bo
'''

class LazyVar(object):
    '''
    classdocs
    '''


    def __init__(self, create_fun):
        '''
        Constructor
        '''
        self.create_fun=create_fun
        self.value=None 
    def get(self):
        if self.value is None:
            self.value=self.create_fun()
        assert self.value is not None 
        return self.value 
        