'''
Created on Apr 11, 2018

@author: bo
'''

import tensorflow as tf

from tfgam import *
import datetime
import time


def do_shuffle(X, y):
    idx = np.random.permutation(range(len(X)))
    return X[idx], y[idx]


class CubicSpline(object):

    def __init__(self, name, batchsize, lot_knots_list, building_knots_list, lot_smooth=0, building_smooth=0, losstype=1):
        self.name = name 
        assert 4 == len(lot_knots_list)
        assert 4 == len(building_knots_list)
        if isinstance(lot_smooth, (int, long, float)):
            lot_smooth = np.array([lot_smooth] * 4, np.float64)
        assert len(lot_smooth) == 4
        if isinstance(building_smooth, (int, long, float)):
            building_smooth = np.array([building_smooth] * 4, np.float64)
        assert len(building_smooth) == 4
                        
        for knots in lot_knots_list:
            assert np.max(knots) < 1 and np.min(knots) > 0
        for knots in building_knots_list:
            assert np.max(knots) < 1 and np.min(knots) > 0
            
        self.batchsize = batchsize
        self.losstype = losstype
        
        self.make_model(lot_knots_list, building_knots_list, lot_smooth, building_smooth)
    
    def make_model(self, lot_knots_list, building_knots_list, lot_smooth, building_smooth):
        self.graph = tf.Graph()
        losstype = self.losstype
        batchsize = self.batchsize
        with self.graph.as_default(): 
            xlist = {name:tf.placeholder(dtype=tf.float64, shape=[batchsize, ]) for name in ['x', 'y', 't', 'lot', 'building', 'garage']}
            y = tf.placeholder(dtype=tf.float64, shape=[batchsize, ])
            self.y = y
            self.xlist = xlist

            y = tf.reshape(y, [-1, 1])
            
            item1 = TsItem("lot_beta" , [xlist[u] for u in ['x', 'y', 't', 'lot']], batchsize, lot_knots_list, smooth=lot_smooth)
            item2 = TsItem("building_beta" , [xlist[u] for u in ['x', 'y', 't', 'building']], batchsize, building_knots_list, smooth=building_smooth)
            # garage_beta = tf.Variable(tf.random_normal([1, ]), name='garage_beta')
            garage_beta = tf.Variable(tf.constant(0, dtype=tf.float64, shape=[1, ]), name='garage_beta')
            
            self.prediction = item1.prediction + item2.prediction + garage_beta * xlist['garage']

            if 1:
                e = tf.abs((y - self.prediction))
                Ie = tf.cast(e < 0.4, tf.float64)
                e = Ie * e + (1 - Ie) * 0.4
                self.metrics = tf.reduce_mean(e)
                            
            if losstype == 0:
                self.loss1 = tf.reduce_mean(tf.pow((y - self.prediction), 2))
            elif  losstype == 1:
                self.loss1 = tf.reduce_mean(tf.abs(y - self.prediction))
            elif  losstype == 2:
                self.loss1 = tf.constant(0.5, dtype=tf.float64) * (tf.reduce_mean(tf.abs(y - self.prediction)) + self.metrics)
            else:
                raise Exception("unknown " + str(losstype))


            self.loss2 = item1.loss2 + item2.loss2
            self.loss = self.loss1 + self.loss2
          
            with tf.variable_scope('saver'):
                    self.saver = tf.train.Saver()

            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.variable_scope('optimizer'):
                with tf.control_dependencies(update_ops):
                    #self.optimizer = tf.train.AdagradOptimizer(0.1)
                    self.optimizer = tf.train.AdamOptimizer(learning_rate=0.01)
                    #self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01)
                    
                    gvs = self.optimizer.compute_gradients(self.loss)
                    capped_gvs = [(tf.clip_by_value(grad, -100., 100.), var) for grad, var in gvs]
                    self.train_op = self.optimizer.apply_gradients(capped_gvs)
        self._session = None 

    def save(self, ckpt=1):
        checkpoint_name = '{}/model_{}.tfmodel'.format(self.name, self.name)
        self.saver.save(self.session, checkpoint_name, global_step=ckpt)

    def load(self, ckpt=1):
        self.saver.restore(self.session, '{}/model_{}.tfmodel'.format(self.name, self.name, ckpt))
        
    @property
    def session(self): 
        if self._session is None:
            config = tf.ConfigProto(log_device_placement=True)
            # config.gpu_options.per_process_gpu_memory_fraction = 0.5  # don't hog all vRAM
            self._session = tf.Session(graph=self.graph, config=config)
        return self._session
    
    def close_session(self):
        if self._session is not None:
            self._session.close()
            self._session = None

    def initialize(self, session=None):
        with self.graph.as_default():
            init_op = tf.global_variables_initializer()        
        self.session.run(init_op) 
        return self.session

    def pad(self, bx):
        if len(bx) < self.batchsize:
            newbx = np.zeros([self.batchsize, bx.shape[1]], dtype=np.float64)
            newbx[:len(bx)] = bx
            return newbx
        else:
            return bx

    def predict(self, X):
        batchsize = self.batchsize
        n = int(np.ceil(len(X) / float(self.batchsize)))
        lst = []
        for i in range(n):
            bx = X[i * batchsize:(i + 1) * batchsize]
            bx = self.pad(bx)
            feed_dict = {self.xlist[name]: bx[:, j] for j, name in enumerate(['x', 'y', 't', 'lot', 'building', 'garage'])}        
            yhat = self.session.run(self.prediction, feed_dict=feed_dict)
            lst.append(yhat)
        yhat = np.concatenate(lst)[:, 0]
        assert len(yhat) >= len(X)
        return yhat[:len(X)]

    def score(self, X, y):
        pred = self.predict(X)
        e = np.abs(pred - y)
        e[e > 0.4] = 0.4       
        return np.mean(e)
        
    def fit(self, rawX, rawy, n_epoch=10, validXY=None, n_wait=10):
        assert (np.sum(np.isnan(rawX))) == 0
        assert (np.sum(np.isnan(rawy))) == 0
        batchsize = self.batchsize
        if validXY is not None:
            validX, validy = validXY
        assert(rawX.shape[1] == len(self.xlist))
        self.initialize()
        
        best_valid = (0, np.inf)
        for epoch in range(n_epoch):
            X, y = do_shuffle(rawX, rawy)
            losses = []
            N = int(len(X) / batchsize)
            for i in range(N):
                bx, by = X[i * batchsize:(i + 1) * batchsize], y[i * batchsize:(i + 1) * batchsize]
                feed_dict = {self.xlist[name]: bx[:, j] for j, name in enumerate(['x', 'y', 't', 'lot', 'building', 'garage'])}
                feed_dict.update({
                    self.y:by
                })
                _, a, aa, aaa = self.session.run([self.train_op, self.loss, self.loss1, self.metrics],
                                                feed_dict=feed_dict)
                losses.append([a, aa, aaa])
                if i % 100 == 0:
                    ts = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    print "{} - iteration {}/{}. losses={}".format(ts, i, N, losses[-1])
                
            if validXY is not None:
                validloss = self.score(validX, validy)
                if validloss < best_valid[1]:
                    best_valid = (i, validloss)
                    print "best loss found:", best_valid
                if(i - best_valid[0]) > n_wait:
                    print "stop!", best_valid
                    self.best_valid = best_valid
                    break
            else:
                validloss = None
            self.save(ckpt=epoch)
            print "Epoch {}, valid loss={}, train loss={}".format(epoch, validloss, np.mean(losses, 0))
        
