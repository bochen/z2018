#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=final --param=7df16dffd7bdd23bca2d73078b5d820f76cb3c01.param --year=2018 --state=39 --seed=424151 --nthread=$LGB_CORES --nround=10000 --uid e890cc1e527b0e0095b63a9f64b2ea2acd20cc65 --make_predict

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

