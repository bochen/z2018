#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=final --param=90e14ddfbc08c6fd9ccf35d1b79290f5b0aa3204.param --year=2017 --state=39 --seed=424150 --nthread=$LGB_CORES --nround=10000 --uid 5b62be2f2f4747636f0e71fbc2d4a2ee614aeea0 --make_predict

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

