#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=final --param=e7b5dde03a0c3dc4c6bafadaf5d11e7afc4ca8e0.param --year=2017 --state=36 --seed=394150 --nthread=$LGB_CORES --nround=10000 --uid adca455f463a2d08bc05d941070a9f4eea7c19bd --make_predict

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

