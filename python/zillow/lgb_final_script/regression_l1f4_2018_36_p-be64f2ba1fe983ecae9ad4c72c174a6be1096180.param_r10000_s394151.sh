#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=final --param=be64f2ba1fe983ecae9ad4c72c174a6be1096180.param --year=2018 --state=36 --seed=394151 --nthread=$LGB_CORES --nround=10000 --uid b20dca2b3f5cedb5d277e3fc5a085b9dbb2416dc --make_predict

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

