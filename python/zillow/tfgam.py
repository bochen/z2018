'''
Created on Apr 11, 2018

@author: bo
'''
import numpy as np 

import tensorflow as tf
from tensorflow.python.ops import array_ops


def cubic_basis(x, knots):

    def _cubic_basis(z):
        f = ((z - 0.5) ** 2 - 1.0 / 12) * ((x - 0.5) ** 2 - 1.0 / 12) / 4.0
        f = f - ((tf.abs(x - z) - 1.0 / 2) ** 4 - 0.5 * (tf.abs(x - z) - 1.0 / 2) ** 2 + 7.0 / 240) / 24.0
        return f

    return tf.cast(tf.constant(1,tf.float64)* tf.transpose(tf.map_fn(_cubic_basis, knots)), tf.float64)


def cubic_basis2(x, knots):

    def _cubic_basis(z):
        f = ((z - 0.5) ** 2 - 1.0 / 12) * ((x - 0.5) ** 2 - 1.0 / 12) / 4.0
        f = f - ((np.abs(x - z) - 1.0 / 2) ** 4 - 0.5 * (np.abs(x - z) - 1.0 / 2) ** 2 + 7.0 / 240) / 24.0
        return f

    return np.array([_cubic_basis(u) for u in knots], np.float64).T


def kronecker_product(mat1, mat2):
  """Computes the Kronecker product two matrices."""
  m1, n1 = mat1.get_shape().as_list()
  mat1_rsh = array_ops.reshape(mat1, [m1, 1, n1, 1])
  m2, n2 = mat2.get_shape().as_list()
  mat2_rsh = array_ops.reshape(mat2, [1, m2, 1, n2])
  return array_ops.reshape(mat1_rsh * mat2_rsh, [m1 * m2, n1 * n2])


def kronecker_product2(mat1, mat2):
  """Computes the Kronecker product two matrices."""
  m1, n1 = mat1.shape
  mat1_rsh = np.reshape(mat1, [m1, 1, n1, 1])
  m2, n2 = mat2.shape
  mat2_rsh = np.reshape(mat2, [1, m2, 1, n2])
  return np.reshape(mat1_rsh * mat2_rsh, [m1 * m2, n1 * n2])

  
def make_X(x, knots):
    a = cubic_basis(x, knots)
    a0 = tf.reshape(tf.ones_like(x), [-1, 1])
    a1 = tf.reshape(x, [-1, 1])
    return tf.concat([a0, a1, a], axis=-1)


def make_S(knots):
    S = np.zeros(shape=[len(knots) + 2] * 2, dtype=np.float64)
    Y = cubic_basis2(knots, knots)
    Y = Y / np.sum(np.abs(Y))
    S[2:, 2:] = Y
    return S


def make_inv_gamma(knots):
    n = len(knots)
    x = (np.arange(n + 2) / float(n + 2))[1:-1]
    G = cubic_basis2(x, knots).T
    G = np.linalg.pinv(G)
    return G / np.sum(np.abs(G))

    
def make_tp(xlist, knots_lst, batchsize):
    assert len(xlist) == len(knots_lst)
    Xlist = [make_X(xlist[j], knots_lst[j]) for j in  range(len(xlist))]
    X = []
    for i in range(batchsize):
        A = [tf.reshape(u[i], [1, -1]) for u in Xlist]
        #print "AAA",  [u.shape for u in A]
        X.append(reduce(lambda u, v: kronecker_product(u, v), A))
    X = tf.concat(X, 0)
    assert X.shape[0] == batchsize, X.shape
    assert X.dtype == tf.float64, X.dtype
    if 0:
        Slist = []
        Ilst = [np.eye(len(knots) + 2) for knots in knots_lst]
        for i in range(len(knots_lst)):
            knots = knots_lst[i]
            inv_gamma = make_inv_gamma(knots)
            S = make_S(knots)
            s2 = np.matmul(np.matmul(inv_gamma.T, S[2:, 2:]), inv_gamma)
            s2 = s2 / np.sum(np.abs(s2))
            S[2:, 2:] = s2
            this_Ilist = list(Ilst)
            this_Ilist[i] = S
            S = reduce(lambda u, v: kronecker_product2(u, v), this_Ilist)
            Slist.append(S.astype(np.float64))
    else:
        Slist = None 
    return X, Slist


class TsItem(object):

    def __init__(self, name, xlist, batchsize, knots_list, smooth):
        assert len(xlist) == len(knots_list)
        if isinstance(smooth, (int, long)):
            smooth = np.array([smooth] * len(xlist), np.float64)
        else:
            assert len(smooth) == len(xlist)
        for knots in knots_list:
            assert np.max(knots) < 1 and np.min(knots) > 0
        self.name = name
        self.smooth = smooth      
        self.batchsize = batchsize
        self.xlist = xlist
        self.knots_list = knots_list
        self.make_model()
    
    def make_model(self):
        smooth = self.smooth
        batchsize = self.batchsize
        if 1: 
            xlist = self.xlist
            self.var_X, self.Slist = make_tp(xlist, self.knots_list, batchsize)
            self.n = int(self.var_X.get_shape()[1])
            print "{} has {} variables".format(self.name, self.n)
            # self.beta = tf.Variable(tf.random_normal([self.n, 1]), name=self.name)
            self.beta = tf.Variable(tf.constant(0, dtype=tf.float64, shape=[self.n, 1]), name=self.name)
            self.prediction = tf.matmul(self.var_X, self.beta)

            if 0:
                # self.X=tf.stack(xlist,axis=1)
                sum_S = 0
                for i in range(len(smooth)):
                    sum_S = smooth[i] * self.Slist[i]
                self.sum_S = tf.constant(sum_S, tf.float64)
                self.loss2 = tf.reduce_sum(tf.matmul(tf.matmul(tf.transpose(self.beta), self.sum_S), self.beta))
            else:
                self.loss2 = smooth[0] * tf.reduce_sum(self.beta * self.beta)
