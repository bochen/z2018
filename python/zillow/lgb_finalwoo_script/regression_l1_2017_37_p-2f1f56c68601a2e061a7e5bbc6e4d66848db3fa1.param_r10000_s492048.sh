#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=woofinal --param=2f1f56c68601a2e061a7e5bbc6e4d66848db3fa1.param --year=2017 --state=37 --seed=492048 --nthread=$LGB_CORES --nround=10000 --uid 3e759c456656f2366a4e01d2e5c459a4d6a81182 --make_predict --without_outlier

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

