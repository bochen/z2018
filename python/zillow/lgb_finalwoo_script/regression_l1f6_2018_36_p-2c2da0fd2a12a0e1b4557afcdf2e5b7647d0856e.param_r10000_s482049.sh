#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=woofinal --param=2c2da0fd2a12a0e1b4557afcdf2e5b7647d0856e.param --year=2018 --state=36 --seed=482049 --nthread=$LGB_CORES --nround=10000 --uid d0feb7d4ad44cdc6c6be84be9f073c7e425e4651 --make_predict --without_outlier

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

