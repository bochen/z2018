#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=woofinal --param=10df9a41e16b8478f049d850b0dbcacea463d1e6.param --year=2018 --state=37 --seed=492049 --nthread=$LGB_CORES --nround=10000 --uid e5b237840e43f7c3f0e5287e2aa4c6a0acb8a1ee --make_predict --without_outlier

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

