#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=woofinal --param=97f48cd77a9ec583df9c7df4e7d15e5e2c78ff1e.param --year=2018 --state=06 --seed=182049 --nthread=$LGB_CORES --nround=10000 --uid d614ccf9da0803f7526cb8e5afb928a2b2f9d560 --make_predict --without_outlier

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

