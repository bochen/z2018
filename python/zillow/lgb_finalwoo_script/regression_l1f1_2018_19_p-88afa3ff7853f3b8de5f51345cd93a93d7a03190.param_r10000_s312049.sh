#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=woofinal --param=88afa3ff7853f3b8de5f51345cd93a93d7a03190.param --year=2018 --state=19 --seed=312049 --nthread=$LGB_CORES --nround=10000 --uid 5788aeb2f06ae35efc07a3cef5bf7bd6b07d1bea --make_predict --without_outlier

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

