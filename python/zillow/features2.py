'''
Created on Apr 11, 2018

@author: bo
'''
from zillow import config, utils, info
import os
import dask.dataframe as dd 
import numpy as np 
import pandas as pd 
from zillow import features  

NUM_FEATS = [
    'poolsizesum',
    'poolcnt',
    
    'fullbathcnt',
    'bathroomcnt',
    'calculatedbathnbr',
    
    'effectiveyearbuilt',
    'yearbuilt',
    'yearremodelled',
    
    'roomcnt',
    'unitcnt',
    'garagecarcnt',
    'bedroomcnt',
    'fireplacecnt',
    
    'numberofstories',
    
    'buildingconditiontypeid',
   
     u'sales_count',
     u'longitude',
     u'latitude',
     u'county',
     u'zip5',
     u'block_x',
     u'block_y',
     u'block_area',
     u'tract_x',
     u'tract_y',
     u'tract_area',
     u'zip5_x',
     u'zip5_y',
     u'zip5_area',
     u'zip3_x',
     u'zip3_y',
     u'zip3_area',
     u'tract',
     u'xplusy',
     u'xminusy',
     u'block_xplusy',
     u'block_xminusy',
     u'tract_xplusy',
     u'tract_xminusy',
     u'zip5_xplusy',
     u'zip5_xminusy',
     u'zip3_xplusy',
     u'zip3_xminusy',
     u'finishedsquarefeet',
     u'lotsizesquarefeet',
     u'yardbuildingsqft',
     u'garagetotalsqft',
     u'garagedetachedsqft',
     u'garageattachedsqft',
     u'totalmarketvalue',
     u'totalassessedvalue',
     u'scaledtotalassessedvalue',
     u'tax_rate'
        
    ]

CAT_FEATS = [
    'poolmaintypeid',
    'lotsiteappeals',
    'architecturalstyletypeid',
    'roofstructuretypeid',
    'decktypeid',
    'heatingorsystemtypeid',
    'typeconstructiontypeid',
    'watertypeid',
    
    'airconditioningtypeid',
    'lotsizetopographytypeid',
    'foundationtypeid',
    'sewertypeid',
    'roofcovertypeid',
    'storytypeid',
    ]

FEATHERFILES = [
    "av_big_change_hist.parq",
    # "gam5_pred_hist.parq",
    "lgb_mv_big_change_hist.parq",
    "yb_sale_price_hist.parq",
    #"gam1_pred_hist.parq",
    "lgb_av_sr_adj.parq",
    "lgb_raw_features.parq",
     'change_ratio_hist.parq',
    'tax_rate_hist.parq',
    'tax_value_hist.parq',
    'local_price_hist.parq',
    'lgbchain_predict_hist.parq',
    'lgb_predict_hist.parq',
    # 'varous_ratio_hist.parq',
    ]


def get_feature_files(state, year, with_gam=True):
    files = FEATHERFILES
    if not with_gam:
        files = [u for u in files if not 'gam' in u]
    if state == '06' or (state == "19" and year in {2007, 2008}):
        files = [u for u in files if u <> 'lgb_mv_big_change_hist.parq']
       
    paths = []
    for fname in files:
        path = os.path.join(config.get_state_year_folder2(state, year), fname)
        if not utils.file_exists(path):
            if fname == 'lgbchain_predict_hist.parq' or fname == 'lgb_predict_hist.parq':
                print "warning '{}' does not exists, ignore!".format(fname)
            else:
                assert False, path
        else:
            paths.append(path)
    return paths   


def get_categories(columns):
    cats = []
    for col in columns:
        if col in CAT_FEATS:
            cats.append(col)
        else:
            if col not in NUM_FEATS and col not in {'fold'} and 'gam' not in col and 'ratio' not in col \
                and 'price' not in col and 'bigchange' not in col and not col.startswith('av_') \
                and not col.startswith('mv_') and not 'av_sr_adj' in col and not '_div_' in col and  not col[:3] in ['min', 'med', 'max'] \
                and not col.startswith('landtaxvaluedollarcnt') and not col.startswith('taxvaluedollarcnt') and not col.startswith('structuretaxvaluedollarcnt') :
                print "WARINING! assume {} to be float".format(col)
    return cats 


def extend_features(df, year):
    df['mv_div_av'] = df['totalmarketvalue'] / df['totalassessedvalue']
    df['mv_div_fsf'] = df['totalmarketvalue'] / df['finishedsquarefeet']
    df['mv_div_lsf'] = df['totalmarketvalue'] / df['lotsizesquarefeet']

    df['mv_div_rc'] = df['totalmarketvalue'] / df['roomcnt']
    df['mv_div_uc'] = df['totalmarketvalue'] / df['unitcnt']
    df['mv_div_bc'] = df['totalmarketvalue'] / df['bedroomcnt']
    
    df['av_div_rc'] = df['totalassessedvalue'] / df['roomcnt']
    df['av_div_uc'] = df['totalassessedvalue'] / df['unitcnt']    
    df['av_div_bc'] = df['totalassessedvalue'] / df['bedroomcnt']
    df['av_div_lsf'] = df['totalassessedvalue'] / df['lotsizesquarefeet']
    
    df['rc_div_uc'] = df['roomcnt'] / df['unitcnt'] 
    df['rc_div_bc'] = df['roomcnt'] / df['bedroomcnt'] 
    
    df['fsf_div_rc'] = df['finishedsquarefeet'] / df['roomcnt']
    df['fsf_div_uc'] = df['finishedsquarefeet'] / df['unitcnt']
    df['fsf_div_bc'] = df['finishedsquarefeet'] / df['bedroomcnt']   
    df['fsf_div_story'] = df['finishedsquarefeet'] / df['numberofstories']       
    
    df['lsf_div_rc'] = df['lotsizesquarefeet'] / df['roomcnt']
    df['lsf_div_uc'] = df['lotsizesquarefeet'] / df['unitcnt']
    df['lsf_div_bc'] = df['lotsizesquarefeet'] / df['bedroomcnt']       
    
    col = "gam4_pred_{}".format(year)
    if col in df.columns:
        df['gam4_div_mv'] = df[col] / df['totalmarketvalue']       
        df['gam4_div_av'] = df[col] / df['totalassessedvalue']       
        df['gam4_div_rc'] = df[col] / df['roomcnt']
        df['gam4_div_uc'] = df[col] / df['unitcnt']    
        df['gam4_div_bc'] = df[col] / df['bedroomcnt'] 
    col = "lgb5_pred_{}".format(year)
    if col in df.columns:
        df['lgb5_div_mv'] = df[col] / df['totalmarketvalue']       
        df['lgb5_div_av'] = df[col] / df['totalassessedvalue']       
        df['lgb5_div_rc'] = df[col] / df['roomcnt']
        df['lgb5_div_uc'] = df[col] / df['unitcnt']    
        df['lgb5_div_bc'] = df[col] / df['bedroomcnt']
    if 'avroll_fullval' in df.columns:
        df['avroll_mv_div_av'] = df['avroll_fullval'] / df['avroll_avtot']         
        df['avroll_mv_div_rc'] = df['avroll_fullval'] / df['roomcnt']
        df['avroll_mv_div_uc'] = df['avroll_fullval'] / df['unitcnt']
        df['avroll_mv_div_bc'] = df['avroll_fullval'] / df['bedroomcnt']
        df['avroll_mv_div_fsf'] = df['avroll_fullval'] / df['finishedsquarefeet']
        df['avroll_mv_div_lsf'] = df['avroll_fullval'] / df['lotsizesquarefeet']

        df['avroll_av_div_rc'] = df['avroll_avtot'] / df['roomcnt']
        df['avroll_av_div_uc'] = df['avroll_avtot'] / df['unitcnt']
        df['avroll_av_div_bc'] = df['avroll_avtot'] / df['bedroomcnt']
        df['avroll_av_div_fsf'] = df['avroll_avtot'] / df['finishedsquarefeet']
        df['avroll_av_div_lsf'] = df['avroll_avtot'] / df['lotsizesquarefeet']
        df['avroll_avland_div_lsf'] = df['avroll_avland'] / df['lotsizesquarefeet']
        df['avroll_land_ratio'] = df['avroll_avland'] / df['avroll_avtot']
    return df


def get_features(state, year, n_fold=None, random_state=None, with_gam=False, with_month=False, use_rawprice=False,use_modelprice=False):
    files = get_feature_files(state, year, with_gam=with_gam)
    lst = []
    for fname in files: 
        df = dd.read_parquet(fname).compute()
        if len(df) == 0:
            print "warning! {} has no data".format(fname)
        print fname, "Shapes", df.shape             
        lst.append(df)
    extra_cols = ['count_density', 'count_density2', 'count_density3', 'area_density100', 'area_density1000', 'area_density50']
    extra_cols += ['place']
    if with_month:
        extra_cols += ['month']
    if state == '06': extra_cols += ['elsd', 'scsd']
        
    for u in extra_cols:
        tmp = features.get(u, state, year)
        lst.append(tmp)
    if state == '36' and year == 2018:
        tmp = features.get('avroll', state, year)
        lst.append(tmp)
    if use_rawprice:
        price = features.get("raw_price", state, year)
    elif use_modelprice:
        price = features.get("sale_price_modelhpi", state, year)['yb_{}_model_saleprice'.format(year)].to_frame()
    else:
        price = features.get("sale_price_zip5", state, year)['yb_{}_zip5_saleprice'.format(year)].to_frame()
    price.columns = ['saleprice']
    lst.append(price)
    df = pd.concat(lst, axis=1)
    df = extend_features(df, year)
    print "data shape", df.shape, '#sales', len(df['saleprice'].dropna())
    assert len(set(df.columns)) == df.shape[1]
    for col in df.columns: 
	if df[col].dtype <> np.float32: 
	    print "Warning {} has type {}".format(col, df[col].dtype)
	    df[col] = df[col].astype(np.float32)
    if n_fold is not None:
        if random_state is not None: np.random.seed(random_state)
        df['fold'] = np.random.permutation(range(len(df)))
        df['fold'] = df['fold'].map(lambda u: u % n_fold + 1).astype(np.int8)
    return df 

