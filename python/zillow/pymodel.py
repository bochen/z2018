'''
Created on Apr 11, 2018

@author: bo
'''

from pygam import *
import datetime
import time


def do_shuffle(X, y):
    idx = np.random.permutation(range(len(X)))
    return X[idx], y[idx]


class CubicSpline(object):

    def __init__(self, name, lot_knots_list, building_knots_list, lot_smooth=0, building_smooth=0):
        self.name = name
        m=len(lot_knots_list) 
        assert m == len(lot_knots_list)
        assert m == len(building_knots_list)
        if isinstance(lot_smooth, (int, long, float)):
            lot_smooth = np.array([lot_smooth] * m, np.float64)
        assert len(lot_smooth) == m
        if isinstance(building_smooth, (int, long, float)):
            building_smooth = np.array([building_smooth] * m, np.float64)
        assert len(building_smooth) == m
                        
        for knots in lot_knots_list:
            assert np.max(knots) < 1 and np.min(knots) > 0
        for knots in building_knots_list:
            assert np.max(knots) < 1 and np.min(knots) > 0
        self.lot_knots_list = lot_knots_list
        self.building_knots_list = building_knots_list 
        self.lot_smooth = lot_smooth
        self.building_smooth = building_smooth
        
    def predict(self, X):
        X = self.make_X(X, return_S=False)
        return np.matmul(X, self.beta)

    def score(self, X, y):
        pred = self.predict(X)
        return self._score(pred, y)
    
    def _score(self, pred, y):
        e = np.abs(pred - y)
        e2 = e ** 2
        e[e > 0.4] = 0.4       
        return np.mean(e), np.sqrt(np.mean(e2))
    
    def make_X(self, rawX, return_S):
        x, y, t, lot, building, garage = rawX.T
        item1 = TsItem("lot_beta" , [x, y, t, lot], self.lot_knots_list, smooth=self.lot_smooth, return_S=return_S)
        item2 = TsItem("building_beta" , [x, y, t, building], self.building_knots_list, smooth=self.building_smooth, return_S=return_S)
        garage=garage.reshape([-1, 1])
        X = np.concatenate([item1.var_X, item2.var_X, garage, np.ones_like(garage)], axis=1)
        if return_S:
            from scipy.linalg import block_diag
            S = block_diag(item1.sum_S, item2.sum_S, 0,0)
            return X, S 
        else:
            return X           

    def fit(self, rawX, rawy):
        assert (np.sum(np.isnan(rawX))) == 0
        assert (np.sum(np.isnan(rawy))) == 0
        X, S = self.make_X(rawX, return_S=True)
        print "X.shape=", X.shape, "y.shape=", rawy.shape
        if 0: 
            beta = np.linalg.lstsq(X, rawy)[0]
        else:
            XX = np.matmul(X.T, X)
            XX += S
            beta = np.matmul(np.linalg.pinv(XX), np.matmul(X.T, rawy))
        self.beta = beta 
        print beta     
        self.train_score = self._score(np.matmul(X, self.beta), rawy)
        
