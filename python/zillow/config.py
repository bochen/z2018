'''
Created on Apr 6, 2018

@author: bo
'''

import os
import utils

if 'ZILLOW_HOME' not in os.environ:
    HOME=os.path.join(os.environ['HOME'],'mydev','z2018')
else:
    HOME = os.environ['ZILLOW_HOME']

assert HOME is not None 

INPUT_PATH = os.path.join(HOME, "input")
RAW_INPUT_PATH = os.path.join(INPUT_PATH, "raw_input")
PARQUET_INPUT_PATH = os.path.join(INPUT_PATH, 'parquet')
TASK_PATH = os.path.join(INPUT_PATH, 'task')
INFO_PATH = os.path.join(INPUT_PATH, 'info')
STATE_INPUT_PATH = os.path.join(INPUT_PATH, 'split')
[utils.create_dir_if_not_exists(directory) for directory in [ PARQUET_INPUT_PATH, TASK_PATH, INFO_PATH, STATE_INPUT_PATH]]

#SALESPRICE_CSV_FILE = os.path.join(RAW_INPUT_PATH, "saleprice", 'saleprice_2007-01-01to2018-06-30_v4.csv') 
SALESPRICE_CSV_FILE = os.path.join(RAW_INPUT_PATH, 'saleprice_2007-01-01to2018-06-30_v4.csv')

TAXHIST_CSV_FILE = os.path.join(RAW_INPUT_PATH, 'tax_history_2007to2018_v4.csv') 

PARCEL_STATE_MAPPING_FILE = os.path.join(INFO_PATH, 'parcel_to_state.pkl') 

HOME_ATTRIB_CSV_FILES = [ os.path.join(RAW_INPUT_PATH, "home_attributes_history", u) for u in 
    ['home_attributes_2007_v1.csv',
 'home_attributes_2008_v1.csv',
 'home_attributes_2009_v1.csv',
 'home_attributes_2010_v1.csv',
 'home_attributes_2011_v1.csv',
 'home_attributes_2012_v1.csv',
 'home_attributes_2013_v1.csv',
 'home_attributes_2014_v1.csv',
 'home_attributes_2015_v1.csv',
 'home_attributes_2016_v1.csv',
 'home_attributes_2017_v4.csv',
 'home_attributes_2018_v4.csv']
                       ]
HOME_ATTRIB_PARQ_FILES = [ os.path.join(PARQUET_INPUT_PATH, utils.basename(u).replace(".csv", ".parq")) for u in  HOME_ATTRIB_CSV_FILES]
SALESPRICE_PARQ_FILE = os.path.join(PARQUET_INPUT_PATH, utils.basename(SALESPRICE_CSV_FILE).replace(".csv", ".parq"))
TAXHIST_PARQ_FILE = os.path.join(PARQUET_INPUT_PATH, utils.basename(TAXHIST_CSV_FILE).replace(".csv", ".parq"))



LATEST_HOME_ATTRIB_PARQ_FB_FILE = HOME_ATTRIB_PARQ_FILES[-1] + ".fb"

if 'ECLIPSE_IDE' in os.environ:
    YEARS = [2015, 2016, 2017]
    STATES = ['06']
    STATES_NAME = ['CA']    
else:
    YEARS = [2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,2018]
    STATES = ['06', '19', '36', '37', '39']
    STATES_NAME = ['CA', 'IA', 'NY', 'NC', 'OH']

def get_state_year_folder(state, year):
    return os.path.join(STATE_INPUT_PATH, state, str(year))

def get_state_year_folder2(state, year):
    return os.path.join(INPUT_PATH, 'split2', state, str(year))


def get_home_attrib_fb(year):
    if year <2017:
        return os.path.join(PARQUET_INPUT_PATH, 'home_attributes_{}_v1.parq.fb'.format(year))
    else:
        return os.path.join(PARQUET_INPUT_PATH, 'home_attributes_{}_v4.parq.fb'.format(year))


