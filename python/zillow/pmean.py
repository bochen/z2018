'''
Created on Jul 20, 2018

@author: bo
'''

import numpy as np 
from multiprocessing import Pool
X = None


def f(j):
    global X
    print ("begin batch " + str(j))
    ret = np.median(X[j],  axis=1)
    print ("end batch " + str(j))
    return ret 


def col_median(df, n_jobs=16):
    global X
    X = []
    i = 0
    while i < df.shape[0]:
        X.append(df.iloc[i:i + 20000].values)
        i += 20000
    print ("#batch: " + str(len(X)))
    p = Pool(n_jobs)
    ret = p.map(f, range(len(X)))
    p.close()
    p.join()
    b = np.concatenate(ret)
    print ("Finish calc...")
    return b 
