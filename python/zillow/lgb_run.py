'''
Created on Jul 11, 2018

@author: bo
'''

import config 
import os
from zillow import features2
from zillow.utils import get_logger
import lightgbm as lgb 
import numpy as np 
import pandas as pd 
import fastparquet


def _score(pred, y):
    pred = np.log(pred)
    y = np.log(y)
    e = np.abs(pred - y)
    l1e = np.mean(e)
    e2 = e ** 2
    e[e > 0.4] = 0.4       
    ret = [np.mean(e), l1e, np.sqrt(np.mean(e2))]
    return list(np.round(ret, 6))


def exclude_columns(cols,exclude_cols):
    import fnmatch
    lst=[]
    for col in cols:
        matched=False 
        for pattern in exclude_cols:
            if fnmatch.fnmatch(col, pattern):
                matched=True 
                break
        if not matched:
            lst.append(col)
    return lst
            

def run(opt, param, logger):
    state = opt.state
    year = opt.year 
    n_fold = opt.fold 
    nseed = opt.seed 
    params = {
                'task': 'train',
                'boosting_type': 'gbdt',
                'metric': {'zl1', 'l1'},
                'bagging_freq': 5,
                'verbose': 0,
                'metric_freq': 50,
                'num_threads': opt.nthread,
                "feature_fraction_seed": nseed + 11,
                'bagging_seed':  nseed + 19,
                'data_random_seed':  nseed + 39,
                'drop_seed':  nseed + 53,
                
            }
    params.update(param)
    logger.info("making data ...")
    df = features2.get_features(state, year, 5, random_state=nseed, with_gam=opt.with_gam)
    logger.info("all data shape: %s", str(df.shape))
    if opt.without is not None:
        df=df[exclude_columns(df.columns,opt.without.split(","))]

    
    if 1:
        from sklearn import preprocessing
        all_cols = list([u for u in df.columns if u not in ['saleprice','fold']])
        cat_cols = [all_cols.index(u) for u in features2.get_categories(all_cols)]
        for col in cat_cols:
            col=all_cols[col]
	    print "transform", col
            le = preprocessing.LabelEncoder()
            df[col]=le.fit_transform(df[col].values)
    else:
        cat_cols=[]
        
    data = df[~df['saleprice'].isnull()]
    logger.info("trainable data shape: %s", str(data.shape))
    
    X_train = data[data['fold'] <> n_fold].drop(['saleprice', 'fold'], axis=1)
    y_train = data[data['fold'] <> n_fold]['saleprice']
    X_test = data[data['fold'] == n_fold].drop(['saleprice', 'fold'], axis=1)
    y_test = data[data['fold'] == n_fold]['saleprice']
    print str(sorted(list(X_train.columns)))
    logger.info("train data shape: %s", str(X_train.shape))
    logger.info("test data shape: %s", str(X_test.shape))
    

    print zip(cat_cols, X_train.columns[cat_cols])
    d_train = lgb.Dataset(X_train.values, label=np.log(y_train.values).astype(np.float32), categorical_feature=cat_cols)
    d_valid = lgb.Dataset(X_test.values, label=np.log(y_test.values).astype(np.float32), categorical_feature=cat_cols)

    logger.info("training model...")
    gbm = lgb.train(params,
                    d_train,
                    num_boost_round=opt.nround,
                    valid_sets=[d_valid, d_train],
                    early_stopping_rounds=50, categorical_feature=cat_cols)

    logger.info("train best iteration: %d, best score: %s", gbm.best_iteration, str(gbm.best_score))
    importance = gbm.feature_importance(iteration=gbm.best_iteration)
    impdf = pd.DataFrame({'s':importance, 'name':X_train.columns})
    with pd.option_context('display.max_rows', None, 'display.max_columns', None):
    	print impdf.sort_values(by='s',ascending=False)
    if opt.save_model:
        gbm.save_model(get_output_modelpath(opt), gbm.best_iteration)
    if opt.make_predict:
        logger.info("making prediction")
        all_X = df.drop(['saleprice', 'fold'], axis=1).values 
        pred = gbm.predict(all_X, num_iteration=gbm.best_iteration)
        df['pred'] = np.exp(pred)

        test_pred = df.loc[X_test.index, 'pred'] 
        logger.info("test scores: %s", _score(test_pred, y_test.values))
        
        df.iloc[X_train.index, 'pred'] = np.nan
        df['error'] = np.log(df['pred']) - np.log(df['saleprice'])
        preddf = df[['pred', 'error']].astype(np.float32)
        fastparquet.write(get_output_filepath(opt), preddf, compression='SNAPPY')
    else:
        test_pred = np.exp(gbm.predict(X_test.values))#, num_iteration=gbm.best_iteration))
        logger.info("test scores: %s", _score(test_pred, y_test.values))


def get_name(opt):
    return "{}-{}-{}".format(opt.state, opt.year, opt.param)    


def get_output_filepath(opt):
    a = 'lgb_' + get_name(opt) + ".parq"
    return os.path.join(config.get_state_year_folder2(opt.state, opt.year), a)


def get_output_modelpath(opt):
    a = 'lgb_' + get_name(opt) + ".model"
    return os.path.join(config.get_state_year_folder2(opt.state, opt.year), a)


if __name__ == '__main__':
    import argparse
    import json
    parser = argparse.ArgumentParser()
    parser.add_argument('--state', action="store", dest="state", type=str, required=True)
    parser.add_argument('--year', action="store", dest="year", type=int, required=True)
    parser.add_argument('--param', action="store", dest="param", type=str, required=True)   
    parser.add_argument('--seed', action="store", dest="seed", type=int, required=True)
    parser.add_argument('--fold', action="store", dest="fold", type=int, required=True)
    parser.add_argument('--nround', action="store", dest="nround", type=int, required=True)
    parser.add_argument('--nthread', action="store", dest="nthread", type=int, required=True)
    parser.add_argument('--save_model', action="store_true", dest="save_model", required=False)
    parser.add_argument('--make_predict', action="store_true", dest="make_predict", required=False)
    parser.add_argument('--with_gam', action="store_true", dest="with_gam", required=False)
    parser.add_argument('--without', action="store", dest="without", required=False, default=None)
    
    opt = parser.parse_args()
    assert opt.fold >= 1 and opt.fold <= 5, opt.fold 
    logger = get_logger(get_name(opt))
    logger.info("argument: %s", str(opt))        
    
    parampath = "{}/python/zillow/lgb_param/{}".format(config.HOME, opt.param)        
    with open(parampath) as f :
        param = json.load(f)
    logger.info("parameters: %s", str(param))
    
    run(opt, param, logger)
