'''
Created on Jul 11, 2018

@author: bo
'''

from tfmodel import * 
import config 
import os
import dask.dataframe as dd
import pandas as pd  
from sklearn import preprocessing


def is_outlier(points, thresh=5):
    median = points.median()
    diff = (points - median) ** 2
    diff = np.sqrt(diff)
    med_abs_deviation = diff.median()
    modified_z_score = 0.6745 * diff / med_abs_deviation
    return modified_z_score > thresh


def check1(df):
    print '#########check 1#############'
    mini_max = {}
    for col in ['lotsizesquarefeet', 'finishedsquarefeet', 'longitude', 'latitude']:
        outliers = is_outlier(df[col])
        if np.sum(outliers) > 0:
            print "WARN. {} has {}% outliers".format(col, np.mean(outliers) * 100)
        a = df[col][~outliers]
        mini_max[col] = (a.min(), a.max())
        print col, mini_max[col], a.max() - a.min(), df[col].max() - df[col].min() , df[col][outliers].min(), df[col][outliers].max()
        min_max_scaler = preprocessing.MinMaxScaler()
        v = df[col]
        x_scaled = min_max_scaler.fit_transform(v[~v.isnull()].values.reshape([-1, 1]))
        df.loc[~v.isnull(), col] = x_scaled[:, 0]
    
    data = df[~df['sale_price'].isnull()]
    a = {}
    for col in ['lotsizesquarefeet', 'finishedsquarefeet', 'longitude', 'latitude']:
        a[col] = np.sum(df[col] > data[col].max()) + np.sum(df[col] < data[col].min())
        # a[col]=a[col]/len(df)

    print "len(df)=", len(df), len(data)
    print pd.concat([data.max() - data.min(), df.max() - df.min(), pd.Series(a)], axis=1)
    print '#########end check 1#############'     

    
def check2(df):
    print '#########check 2#############'
    data = df[~df['sale_price'].isnull()]
    for col in ['lotsizesquarefeet', 'finishedsquarefeet', 'longitude', 'latitude', 'year']:
        min_max_scaler = preprocessing.MinMaxScaler()
        v = df[col]
        x_scaled = min_max_scaler.fit(data[col].dropna().values.reshape([-1, 1])).transform(
            v[~v.isnull()].values.reshape([-1, 1]))
        x_scaled[x_scaled > 1] = 1
        x_scaled[x_scaled < 0] = 0
        df.loc[~v.isnull(), col] = x_scaled[:, 0] * 0.8 + 0.1
    
    data = df[~df['sale_price'].isnull()]
    a = {}
    for col in ['lotsizesquarefeet', 'finishedsquarefeet', 'longitude', 'latitude']:
        a[col] = np.sum(df[col] > data[col].max()) + np.sum(df[col] < data[col].min())
        a[col] = a[col] / len(df)

    print "len(df)=", len(df), len(data)
    print pd.concat([data.max() - data.min(), df.max() - df.min(), pd.Series(a)], axis=1)
    print '#########end check 2#############'
    return df 


def clean(df):
    if 1:
        df = df[~df['longitude'].isnull()]
        df = df[~df['latitude'].isnull()]
        df.loc[df['garagetotalsqft'].isnull(), 'garagetotalsqft'] = 0
    data = df[~df['sale_price'].isnull()]
    data = data.dropna()
    print "len(df)=", len(df), len(data)
    return df, data 


def make_knots1(v, n, delta):
    name = v.name 
    print "make for ", v.name 
    v = v.dropna().values
    if name == 'year':
        knots = sorted(list(set(v)))
    else:
        assert (len(v) > 100), len(v)
        assert(delta > 0)
        v = (v / delta).astype(np.int) * delta
        v = sorted(list(set(v)))
        q = (np.arange(n) * 100. / (n - 1)).astype(np.int)
        print "len(v)=", len(v), q
        knots = np.percentile(v, q)
        knots = np.array(sorted(list(set(knots)))) 
    knots = np.array(knots).astype(np.float64)
    print "knots=", knots 
    return knots


def make_knots2(v, n, delta):
    name = v.name 
    print "make for ", v.name 
    v = v.dropna().values
    if name == 'year':
        knots = sorted(list(set(v)))
    else:
        assert (len(v) > 100), len(v)
        assert(delta > 0)
        q = (np.arange(n) * 100. / (n - 1)).astype(np.int)
        print "len(v)=", len(v), q
        knots = np.percentile(v, q)
        knots = np.array(sorted(list(set(knots)))) 
    knots = np.array(knots).astype(np.float64)
    print "knots=", knots 
    return knots


def run(opt):
    state = opt.state

    batchsize = opt.batchsize
    if opt.knot_method > 0:    
        make_knots = make_knots1
    else:
        make_knots = make_knots2
    filename = os.path.join(config.get_state_year_folder(state, 'share'), 'tf_input.parq')
    df = dd.read_parquet(filename, columns=[u'lotsizesquarefeet', u'finishedsquarefeet', u'garagetotalsqft', u'longitude', u'latitude', u'sale_price', u'year']
).compute()
    if not opt.year is None:
        df=df[df['year']==opt.year]
    df['lotsizesquarefeet'] = np.log(df['lotsizesquarefeet'])
    df['finishedsquarefeet'] = np.log(df['finishedsquarefeet'])
    df['garagetotalsqft'] = np.log(df['garagetotalsqft'])
    df['sale_price'] = np.log(df['sale_price'])
    df['year'] = df['year'].astype(np.float64)
    print "df.isnull().mean()"
    print df.isnull().mean()
    
    check1(df)
    df = check2(df)
    df, data = clean(df)
    df = df.astype(np.float64)
    data = data.astype(np.float64)

    cols = ['longitude', 'latitude', 'year', 'lotsizesquarefeet']
    nknots = dict(zip(cols, [opt.knot1, opt.knot1, None, opt.knot1b]))
    deltas = dict(zip(cols, [0.001, 0.001, None, 0.005]))
    lot_knots_list = [make_knots(df[u], nknots[u], deltas[u]) for u in cols]
    
    cols = ['longitude', 'latitude', 'year', 'finishedsquarefeet']
    nknots = dict(zip(cols, [opt.knot2, opt.knot2, None, opt.knot2b]))
    deltas = dict(zip(cols, [0.001, 0.001, None, 0.005]))
    building_knots_list = [make_knots(df[u], nknots[u], deltas[u]) for u in cols]    

    # ['x', 'y', 't', 'lot', 'building', 'garage']
    print data.dtypes 
    
    from sklearn.model_selection import KFold
    X = data[['longitude', 'latitude', 'year', 'lotsizesquarefeet', 'finishedsquarefeet',
                      'garagetotalsqft']].values
    y = data['sale_price'].values
    from sklearn.model_selection import KFold
    kf = KFold(n_splits=5)
    for i, (train, test) in enumerate(kf.split(X)):
        X_train, X_test, y_train, y_test = X[train], X[test], y[train], y[test]
        cs = CubicSpline("tf" + state, batchsize, lot_knots_list, building_knots_list,
                    lot_smooth=opt.s1, building_smooth=opt.s2, losstype=opt.loss)
        cs.fit(rawX=X_train, rawy=y_train,
                          n_epoch=opt.epoch, validXY=(X_test, y_test), n_wait=10)
        break


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--state', action="store", dest="state", type=str, required=True)
    parser.add_argument('--batchsize', action="store", dest="batchsize", type=int, required=True)    
    parser.add_argument('--knot1', action="store", dest="knot1", type=int, required=True)
    parser.add_argument('--knot2', action="store", dest="knot2", type=int, required=True)
    parser.add_argument('--knot1b', action="store", dest="knot1b", type=int, required=True)
    parser.add_argument('--knot2b', action="store", dest="knot2b", type=int, required=True)
    parser.add_argument('--s1', action="store", dest="s1", type=float, required=True)
    parser.add_argument('--s2', action="store", dest="s2", type=float, required=True)
    parser.add_argument('--knot_method', action="store", dest="knot_method", type=int, required=True)
    parser.add_argument('--loss', action="store", dest="loss", type=int, required=True)
    parser.add_argument('--year', action="store", dest="year", type=int, required=False)
    parser.add_argument('--epoch', action="store", dest="epoch", type=int, default= 100, required=False)
    
    opt = parser.parse_args()
    print "argument:", str(opt)
    
    run(opt)
