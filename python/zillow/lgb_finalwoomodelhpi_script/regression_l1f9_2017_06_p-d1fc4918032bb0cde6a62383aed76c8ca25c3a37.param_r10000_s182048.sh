#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=woomodelhpifinal --param=d1fc4918032bb0cde6a62383aed76c8ca25c3a37.param --year=2017 --state=06 --seed=182048 --nthread=$LGB_CORES --nround=10000 --uid 113c3c391c9cd0c0e6c1f2527b906de78c22b45a --make_predict --without_outlier --use_modelprice

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

