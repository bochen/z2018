#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=woomodelhpifinal --param=4f6c1c65c3b68b00f40d4a3f1f7a6d2788c8b68e.param --year=2018 --state=06 --seed=182049 --nthread=$LGB_CORES --nround=10000 --uid 62c24c98cf2cb03416f5fa54e963b81d88a9792e --make_predict --without_outlier --use_modelprice

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

