#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=woomodelhpifinal --param=400432e84fb7e8d4940bcac444a33b5de32332d8.param --year=2018 --state=36 --seed=482049 --nthread=$LGB_CORES --nround=10000 --uid 2b2df1d088805a78ad78b44aff860cf54df4e62d --make_predict --without_outlier --use_modelprice

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

