#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=woomodelhpifinal --param=4b69e54c4c93022a0ac145077292e1abc5ccef80.param --year=2017 --state=36 --seed=482048 --nthread=$LGB_CORES --nround=10000 --uid 50e6132a936da609c9e7812b21d7518678c6902b --make_predict --without_outlier --use_modelprice

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

