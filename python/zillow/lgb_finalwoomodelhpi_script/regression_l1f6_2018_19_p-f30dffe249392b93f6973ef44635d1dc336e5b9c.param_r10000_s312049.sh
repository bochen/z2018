#!/bin/bash


if [[ -v ZILLOW_HOME ]]; then
    echo $ZILLOW_HOME
else
    echo "ZILLOW_HOME is not set"
    exit -1
fi
cd $ZILLOW_HOME/python/zillow

sleep $[ ( $RANDOM % 60 )  + 1 ]

echo Date: `date`
t1=`date +%s`

python lgb_run5.py --prefix=woomodelhpifinal --param=f30dffe249392b93f6973ef44635d1dc336e5b9c.param --year=2018 --state=19 --seed=312049 --nthread=$LGB_CORES --nround=10000 --uid 704cb1dc5f4acfa345784ee45eb972e1be0d460c --make_predict --without_outlier --use_modelprice

[ $? -eq 0 ] || echo 'JOB FAILURE: $?'
echo Date: `date`
t2=`date +%s`
tdiff=`echo 'scale=3;('$t2'-'$t1')/3600' | bc`
echo 'Total time:  '$tdiff' hours'

