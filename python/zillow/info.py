'''
Created on Apr 12, 2018

@author: bo
'''
from zillow import config
import os
import pandas as pd 
import numpy as np 
import cPickle as pickle 
from LazyVar import LazyVar


def get_county_fs04ctst():
    filename = os.path.join(config.INPUT_PATH, "third", 'fs04ctst.csv')
    df = pd.read_csv(filename, skiprows=27, dtype={0:np.str}, usecols=range(4))
    df.columns = ['county_fips', 'msa', 'state_name', 'county_name']
    df = df.drop('msa', axis=1).set_index('county_fips')
    df = df[df['state_name'].isin(set(config.STATES_NAME))]
    return df 


def get_tax_rates(state, year):
    if state <> "36":
        with open(os.path.join(config.INFO_PATH, '{}_{}_tax_rate.pkl'.format(state, year))) as f:
            taxrates = pickle.load(f)
            return taxrates 
    else:
        df = pd.read_csv(os.path.join(config.INPUT_PATH, 'third2', 'nyc_tax.csv'), index_col=0)
        return df['CLASS 1'].to_dict(), df['CLASS 2'].to_dict()


def _get_hpi_by_zip5():
    filename = os.path.join(config.INPUT_PATH, "third2", 'HPI_at_zip5_monthly.csv')

    df = pd.read_csv(filename, parse_dates=['Month'])
    df['Month'] = df['Month'].map(lambda u: u.year * 100 + u.month)
    df = df.set_index('Month')
    assert df.isnull().sum().sum() == 0
    return df.to_dict()          


HPI_ZIP5 = LazyVar(_get_hpi_by_zip5)


def get_hpi_by_zip5():
    return HPI_ZIP5.get()


def _get_hpi_by_zip5_arima():
    filename = os.path.join(config.INPUT_PATH, "third2", 'HPI_at_zip5_monthly_arima.csv')

    df = pd.read_csv(filename, parse_dates=['Month'])
    df['Month'] = df['Month'].map(lambda u: u.year * 100 + u.month)
    df = df.set_index('Month')
    assert df.isnull().sum().sum() == 0
    return df.to_dict()          


HPI_ZIP5_arima = LazyVar(_get_hpi_by_zip5_arima)


def get_hpi_by_zip5_arima():
    return HPI_ZIP5_arima.get()

def _get_hpi_by_zip3():
    filename = os.path.join(config.INPUT_PATH, "third2", 'HPI_at_zip3_monthly.csv')

    df = pd.read_csv(filename, parse_dates=['Month'])
    df['Month'] = df['Month'].map(lambda u: u.year * 100 + u.month)
    df = df.set_index('Month')
    assert df.isnull().sum().sum() == 0
    return df.to_dict()


HPI_ZIP3 = LazyVar(_get_hpi_by_zip3)


def get_hpi_by_zip3():
    return HPI_ZIP3.get()


def _get_hpi_by_zip3_arima():
    filename = os.path.join(config.INPUT_PATH, "third2", 'HPI_at_zip3_monthly_arima.csv')

    df = pd.read_csv(filename, parse_dates=['Month'])
    df['Month'] = df['Month'].map(lambda u: u.year * 100 + u.month)
    df = df.set_index('Month')
    assert df.isnull().sum().sum() == 0
    return df.to_dict()


HPI_ZIP3_arima = LazyVar(_get_hpi_by_zip3_arima)


def get_hpi_by_zip3_arima():
    return HPI_ZIP3_arima.get()

def get_hpi_by_state():
    filename = os.path.join(config.INPUT_PATH, "third2", 'HPI_po_state_monthly.csv')
    df = pd.read_csv(filename, parse_dates=['Month']) 
    m = {u'Month': 'Month', u'CA': '06', u'IA': '19', u'NC': '37', u'NY': '36', u'OH': '39'}
    df.columns = [m[u] for u in df.columns]
    df['Month'] = df['Month'].map(lambda u: u.year * 100 + u.month)
    df = df.set_index('Month')
    assert df.isnull().sum().sum() == 0
    return df.to_dict()


def _get_hpi_by_county():
    filename = os.path.join(config.INPUT_PATH, "third2", 'HPI_at_county_monthly.csv')

    df = pd.read_csv(filename, parse_dates=['Month'])
    df['Month'] = df['Month'].map(lambda u: u.year * 100 + u.month)
    df = df.set_index('Month')
    assert df.isnull().sum().sum() == 0
    return df.to_dict()


HPI_COUNTY = LazyVar(_get_hpi_by_county)


def get_hpi_by_county():
    return HPI_COUNTY.get()


def _get_hpi_by_county_arima():
    filename = os.path.join(config.INPUT_PATH, "third2", 'HPI_at_county_monthly_arima.csv')

    df = pd.read_csv(filename, parse_dates=['Month'])
    df['Month'] = df['Month'].map(lambda u: u.year * 100 + u.month)
    df = df.set_index('Month')
    assert df.isnull().sum().sum() == 0
    return df.to_dict()


HPI_COUNTY_arima = LazyVar(_get_hpi_by_county_arima)


def get_hpi_by_county_arima():
    return HPI_COUNTY_arima.get()


def _get_hpi_by_tract():
    filename = os.path.join(config.INPUT_PATH, "third2", 'HPI_at_tract_monthly.csv')

    df = pd.read_csv(filename, parse_dates=['Month'])
    df['Month'] = df['Month'].map(lambda u: u.year * 100 + u.month)
    df = df.set_index('Month')
    assert df.isnull().sum().sum() == 0
    return df.to_dict()


HPI_TRACT = LazyVar(_get_hpi_by_tract)


def get_hpi_by_tract():
    return HPI_TRACT.get()


def _get_modelhpi_by_block():

    yearhpi2017 = {}
    for state in config.STATES:
        year = 2017
        filename = os.path.join(config.get_state_year_folder(state, year), 'year_model_hpi.pkl')
        d = pickle.load(open(filename))
        d = {year * 100 + month:v for month, v in d.items()}
        yearhpi2017[state] = d

    yearhpi2018 = {}
    for state in config.STATES:
        year = 2018
        filename = os.path.join(config.get_state_year_folder(state, year), 'year_model_hpi.pkl')
        d = pickle.load(open(filename))
        d = {year * 100 + month:v for month, v in d.items() if month <= 6}
        yearhpi2018[state] = d
    
    hpi = {2017: yearhpi2017, 2018:yearhpi2018}
    return hpi


MODELHPI_BLOCK = LazyVar(_get_modelhpi_by_block)
def get_modelhpi_by_block():
    return MODELHPI_BLOCK.get()


def _get_modelhpiext_by_block():

    filename = os.path.join(config.INFO_PATH, 'hpi_model_extrapolate.pkl')
    d = pickle.load(open(filename))
    return d


MODELHPIEXT_BLOCK = LazyVar(_get_modelhpiext_by_block)



def get_modelhpiext_by_block():
    return MODELHPIEXT_BLOCK.get()


def _get_salesratio_NC():
    filename = os.path.join(config.INPUT_PATH, "third2", 'sr_nc.csv')
    df = pd.read_csv(filename, dtype={'countyfips':np.str})
    df = df.drop(['county', 'state'], axis=1)
    df = df.set_index("countyfips")
    df.columns = [int(u) for u in df.columns ]
    df = df[[u for u in df.columns if u >= 2007]]
    years = set(df.columns)
    assert df.isnull().sum().sum() == 0
    return years, df.to_dict()


def _get_salesratio_IA():
    filename = os.path.join(config.INPUT_PATH, "third2", 'sr_ia.csv')
    df = pd.read_csv(filename, dtype={'countyfips':np.str})
    years = set(df['year'])
    df = pd.pivot_table(df, values=['sr'], index='countyfips', columns=['year'])
    df.columns = df.columns.droplevel()
    assert df.isnull().sum().sum() == 0
    return years, df.to_dict()


def _get_salesratio_OH():
    filename = os.path.join(config.INPUT_PATH, "third2", 'sr_oh.csv')
    df = pd.read_csv(filename, dtype={'countyfips':np.str})
    df = df[df['Year'] >= 2007]    
    years = set(df['Year'])
    df = pd.pivot_table(df, values=['sr'], index='countyfips', columns=['Year'])
    df.columns = df.columns.droplevel()
    assert df.isnull().sum().sum() == 0
    return years, df.to_dict()


def get_salesratio(state):
    if state == '19':
        return   _get_salesratio_IA()
    if state == '37':
        return   _get_salesratio_NC()
    if state == '39':
        return   _get_salesratio_OH()
    raise Exception("unknown " + state)

        
class HpiDiscount():

    def __init__(self, hpitype):
        self.hpitract = get_hpi_by_tract()
        self.hpicounty = get_hpi_by_county()
        self.hpicounty_arima = get_hpi_by_county_arima()
        self.hpistate = get_hpi_by_state()
        self.hpi5 = get_hpi_by_zip5()
        self.hpi3 = get_hpi_by_zip3()
        self.hpi5_arima = get_hpi_by_zip5_arima()
        self.hpi3_arima = get_hpi_by_zip3_arima()
        
        self.modelhpi = get_modelhpi_by_block()
        self.modelhpiext = get_modelhpiext_by_block()
        self.hpitype = hpitype 

    def f_county(self, state, c, m):
        if c in self.hpicounty:
            return self.hpicounty[c][m]
        else:
            return self.hpistate[state][m]        

    def f_tract(self, state, c, tract, m):
        if tract in self.hpitract:
            return self.hpitract[tract][m]
        elif c in self.hpicounty:
            return self.hpicounty[c][m]
        else:
            return self.hpistate[state][m]

    def f_zip(self, state, c, zip3, zip5, m):
        if zip5 in self.hpi5:
            return self.hpi5[zip5][m]
        elif zip3 in self.hpi3:
            return self.hpi3[zip3][m]            
        else:
            return self.hpicounty[c][m]

    def f_zip_arima(self, state, c, zip3, zip5, m):
        if zip5 in self.hpi5_arima:
            return self.hpi5_arima[zip5][m]
        elif zip3 in self.hpi3_arima:
            return self.hpi3_arima[zip3][m]            
        else:
            return self.hpicounty_arima[c][m]
        
    def f_model(self, state, month, block):
        year = int(month/100)
        state=block[:2]
        modelhpi=self.modelhpi[year][state][month]
        if block in modelhpi:
            return modelhpi[block]
        elif block[:11] in self.modelhpi:
            return modelhpi[block[:11]]
        elif block[:5] in self.modelhpi:
            return modelhpi[block[:5]]
        else:
            return modelhpi[block[:2]]

    def f_modelext(self, state, month, block):
        year = int(month/100)
        state=block[:2]
        modelhpi=self.modelhpiext[year][state][month]
        if block in modelhpi:
            return modelhpi[block]
        elif block[:11] in self.modelhpi:
            return modelhpi[block[:11]]
        elif block[:5] in self.modelhpi:
            return modelhpi[block[:5]]
        else:
            return modelhpi[block[:2]]

        
    def ratio(self, month0, month1, state, county, zip3, zip5, block):
        if self.hpitype == 'zip5':
            v0 = self.f_zip(state, county, zip3, zip5, month0) 
            v1 = self.f_zip(state, county, zip3, zip5, month1)
            return v1 / v0
        elif self.hpitype == 'zip5_arima':
            v0 = self.f_zip_arima(state, county, zip3, zip5, month0) 
            v1 = self.f_zip_arima(state, county, zip3, zip5, month1)
            return v1 / v0
        elif self.hpitype == 'tract':
            tract = block[:11]
            v0 = self.f_tract(state, county, tract, month0) 
            v1 = self.f_tract(state, county, tract, month1)
            return v1 / v0
        elif self.hpitype == 'county':
            v0 = self.f_county(state, county, month0) 
            v1 = self.f_county(state, county, month1)
            return v1 / v0
        elif self.hpitype == 'model':
            v0 = self.f_model(state, month0, block) 
            v1 = self.f_model(state, month1, block)
        elif self.hpitype == 'modelext':
            v0 = self.f_modelext(state, month0, block) 
            v1 = self.f_modelext(state, month1, block)
            return v1 / v0        
        else:
            raise Exception("unknown " + self.hpitype)

    
if __name__ == '__main__':
    if 0:
        df = get_county_fs04ctst()
        print df.head()
        print df.shape 
    if 1:
        df = get_hpi_by_zip5()
        for i in df.keys()[:3]:
            print i, df[i]

