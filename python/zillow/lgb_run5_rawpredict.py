'''
Created on Jul 11, 2018

@author: bo
'''

import config 
import os
from zillow import features2, features
from zillow.utils import get_logger
import lightgbm as lgb 
import numpy as np 
import pandas as pd 
import fastparquet
import cPickle as pickle 
import sys


def exclude_columns(cols, exclude_cols):
    import fnmatch
    lst = []
    for col in cols:
        matched = False 
        for pattern in exclude_cols:
            if fnmatch.fnmatch(col, pattern):
                matched = True 
                break
        if not matched:
            lst.append(col)
    return lst
            

def run_fold(opt, data, n_fold, state, year, month):
    data['month'] = float(year * 100 + month)
    X = data.drop(['saleprice', 'fold'], axis=1).values 
    global models
    gbm = models[n_fold]
    pred = gbm.predict(X, num_iteration=gbm.best_iteration)
    df = pd.Series(pred, index=data.index)
    df.name='pred'
    assert df.isnull().sum()==0
    return df.to_frame() 


def run_month(opt, data, state, year, month):    
    print "runing for month", month
    lst = []    
    for n_fold in range(1, 6):
        tmp = run_fold(opt, data, n_fold, state, year, month)
        lst.append(tmp)
    pred = reduce(lambda u, v: u + v, lst) / len(lst)
    pred['pred']=np.exp(pred['pred'])
    pred ['month'] = month
    print pred.shape
    print pred.head()
    return pred 


def run(opt, logger):
    state = opt.state
    year = opt.year 
    nseed = opt.seed 
    logger.info("making data ...")
    df = features2.get_features(state, year, 5, random_state=nseed, 
                                with_gam=opt.with_gam, with_month=opt.with_month, 
                                use_rawprice=opt.use_rawprice, 
                                use_modelprice=opt.use_modelprice)
    logger.info("all data shape: %s", str(df.shape))
    if opt.without is not None:
        df = df[exclude_columns(df.columns, opt.without.split(","))]
    
    if 1:
        from sklearn import preprocessing
        all_cols = list([u for u in df.columns if u not in ['saleprice', 'fold']])
        cat_cols = [all_cols.index(u) for u in features2.get_categories(all_cols)]
        for col in cat_cols:
            col = all_cols[col]
            print "transform", col
            le = preprocessing.LabelEncoder()
            df[col] = le.fit_transform(df[col].values)
    else:
        cat_cols = []
        
    data = df[df['saleprice'].isnull()]
    logger.info("test data shape: %s", str(data.shape))

    # get blocks:
    blocks = features.make_block_dict(state)

    assert (np.array([len(u) for u in blocks.values()]) == 15).all() 
    data = data[data.index.isin(set(blocks.keys()))]


    if 1:
        global models
        models={}
        for n_fold  in range(1,6):	
            model_file = '{}_{}-{}-{}.param-f{}.model'.format(opt.prefix,state,year,opt.param, n_fold)
            model_file = os.path.join(config.get_state_year_folder2(state, year), model_file)
            gbm = lgb.Booster(model_file=model_file)
            models[n_fold]=gbm

    if year==2018:
        pred = {month: run_month(opt, data, state, year, month) for month in range(1, 7)}
    else:
        pred = {month: run_month(opt, data, state, year, month) for month in range(1, 13)} 

    
    hpi = make_hpi(pred, blocks)
    print "len(hpi)=", len(hpi)
    outfile = os.path.join(config.get_state_year_folder(state, year), 'year_model_hpi.pkl')
    pickle.dump(hpi, open(outfile, 'wb'))

        
def make_hpi(pred, blocks):
    unique_blocks = set(blocks)
    logger.info("#unique_blocks=" + str(len(unique_blocks)))
    pred = {k:v / pred[1] for k, v in pred.items()}
    hpi = {} 
    for month in pred.keys():
        hpi[month] = {}
    for i in [15, 11, 5, 2]:
        subblocks = {u:v[:i] for u,v in blocks.items()}
        for month in pred.keys():
            tmpdf = pred[month]['pred'].to_frame() 
            tmpdf['key'] = tmpdf.index.map(lambda u: subblocks[u])
            tmpdf = tmpdf.groupby('key').aggregate(['count', 'median'])
            tmpdf.columns = tmpdf.columns.droplevel()
            print "for month ", month
            print tmpdf.head()
            tmpdf = tmpdf[tmpdf['count'] > 30] 
            hpi[month].update(tmpdf['median'].to_dict())
    return hpi 


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--state', action="store", dest="state", type=str, required=True)
    parser.add_argument('--year', action="store", dest="year", type=int, required=True)
    parser.add_argument('--param', action="store", dest="param", type=str, required=True)   
    parser.add_argument('--seed', action="store", dest="seed", type=int, required=True)
    parser.add_argument('--prefix', action="store", dest="prefix", type=str, required=False, default='lgb')
    parser.add_argument('--with_gam', action="store_true", dest="with_gam", required=False)
    parser.add_argument('--without', action="store", dest="without", required=False, default=None)
    parser.add_argument('--with_month', action="store_true", dest="with_month", required=False, default=False)
    parser.add_argument('--use_rawprice', action="store_true", dest="use_rawprice", required=False, default=False)
    parser.add_argument('--use_modelprice', action="store_true", dest="use_modelprice", required=False, default=False)
    
    opt = parser.parse_args()
    logger = get_logger(sys.argv[0])
    logger.info("argument: %s", str(opt))        
    assert not(opt.use_rawprice and opt.use_modelprice)
    run(opt, logger)
