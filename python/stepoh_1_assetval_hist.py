'''
Created on Apr 6, 2018

@author: bo
'''
import dask.dataframe as dd 
import numpy as np 
import pandas as pd 
import os
import logging
import fastparquet
import gc
import cPickle as pickle 
import geopandas as gpd

from  zillow import config, utils, features, info
from zillow.task import Task


def make_change_ratio_mv():
    
    def _fun_(state, year, logger):        
        this_av = features.get("totalmarketvalue", state, year)
        prior_av = features.get("totalmarketvalue", state, year - 1)
        df = pd.concat([this_av, prior_av], axis=1)
        
        df.columns = ['this_av', 'prior_av']
        df['ratio'] = df['this_av'] / df['prior_av'] - 1
        df = df[~df['ratio'].isnull()]
        df = df[df['ratio'] <> 0]
        med = df['ratio'].median()
        assert not np.isnan(med)
        logger.info ("median ratio is %s, len(df)=%s", med, len(df))
        #df['ratio_median'] = float(med)
        #df['ratio_median'] = df['ratio_median'].astype(np.float32) 
        #df['big_change'] = (df['ratio'] > max(0.02, med + 0.01)) | (df['ratio'] < min(0.0, med - 0.01))
        df['big_change']=np.abs(df['ratio'])>0.005
        logger.info("$bigchange=%s\n%s", df['big_change'].mean(), df.head())
        
        outfile = os.path.join(config.get_state_year_folder(state, year), 'mv_change_ratio.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in ['39']:
        for year in config.YEARS[1:]:
            task = Task('make_change_ratio_mv-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()

def make_change_ratio_av():

    def _fun_(state, year, logger):        
        this_av = features.get("scaledtotalassessedvalue", state, year)
        prior_av = features.get("scaledtotalassessedvalue", state, year - 1)
        df = pd.concat([this_av, prior_av], axis=1)
        
        df.columns = ['this_av', 'prior_av']
        df['ratio'] = df['this_av'] / df['prior_av'] - 1
        df = df[~df['ratio'].isnull()]
        logger.info("non-zero=%s", (df['ratio'] <> 0).mean() )
        df = df[df['ratio'] <> 0]
        med = df['ratio'].median()
        assert not np.isnan(med)
        logger.info ("median ratio is %s, len(df)=%s", med, len(df))
        #df['ratio_median'] = float(med)
        #df['ratio_median'] = df['ratio_median'].astype(np.float32) 
        #df['big_change'] = (df['ratio'] > max(0.02, med + 0.01)) | (df['ratio'] < min(0.0, med - 0.01))
        df['big_change']=np.abs(df['ratio'])>0.005
        logger.info("$bigchange=%s\n%s", df['big_change'].mean(), df.head())
        
        outfile = os.path.join(config.get_state_year_folder(state, year), 'av_change_ratio.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')


    for state in ['39']:
        for year in config.YEARS[1:]:
            task = Task('make_change_ratio_av-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()
                                    

        
def main():
    make_change_ratio_mv()
    make_change_ratio_av()                                    
if __name__ == '__main__':
    main()