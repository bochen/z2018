'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils, features
import os
import sys
import glob
import numpy as np
from multiprocessing import Pool
import hashlib
from zillow.task import Task

 
def shell_run_and_wait(command):
    command = command.split(" ")
    import subprocess
    # process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process = subprocess.Popen(command)
    process.wait()
    return process.returncode


def run_lgb_single(cmd):

    def f(cmd, logger):
        logger.info(cmd)
        code = shell_run_and_wait(cmd)
        assert code == 0, (code, cmd)

    hashid = hashlib.sha1(cmd).hexdigest()
    task = Task('run_lgb_final_single-{}'.format(hashid), lambda u: f(cmd, u))
    task()

    
def run_lgb(logger):
    assert 'ZILLOW_HOME' in os.environ
    assert 'LGB_CORES' in os. environ 
    files = glob.glob(os.path.join(config.HOME, 'python', 'zillow', 'lgb_finalwoo_script') + '/*_2018_*.sh')
    files += glob.glob(os.path.join(config.HOME, 'python', 'zillow', 'lgb_finalwoomodelhpi_script') + '/*_2018_*.sh')
    np.random.shuffle(files)
    logger.info("going run %d scripts", len(files))
    if 0: #seq run 
        for cmd in files:
            logger.info("cmd: %s", cmd)
            run_lgb_single(cmd)
    else:
        assert 'TOTAL_CORES' in os. environ
        n_jobs = int(int(os.environ['TOTAL_CORES']) / int(os.environ['LGB_CORES']))
        logger.info("use %d jobs", n_jobs) 
        p = Pool(n_jobs)
        p.map(run_lgb_single, files)
        p.close()
        p.join()

       
def run():
    task = Task('run_lgb_final-all', lambda u: run_lgb(u))
    task()


if __name__ == '__main__':
        run()
