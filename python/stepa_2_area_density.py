'''
Created on Apr 6, 2018

@author: bo
'''
import numpy as np 
import os
import fastparquet

from  zillow import config, utils, features, info
from zillow.task import Task

XY = None
tree = None 
df = None 


def f(jk):
    j, k = jk
    print j, k
    global XY, tree, df
    print ("begin batch " + str(j))
    lst = XY[j]
    _, ret = tree.query(lst, k=k)
    distances = []
    for i, l in enumerate(ret):
        xy = lst[i].reshape([1, -1])
        points = df.iloc[l].values
        ds = np.sqrt(np.sum((points - xy) ** 2, axis=1))
        distances.append(np.median(ds))
    b = np.log10(np.array(distances)+1e-15).astype(np.float32)
    print ("end batch " + str(j))
    return b


def _fun2_(state, year, logger, k , colname): 
    from scipy.spatial import cKDTree
    from multiprocessing import Pool

    global XY, tree, df
    df = features.get('longi_lati', state, year)
    tree = cKDTree(df.values)
    logger.info("start to calc..., #data %d", len(df))
    i = 0
    XY = []
    while i < df.shape[0]:
        XY.append(df.iloc[i:i + 20000].values)
        i += 20000
    logger.info("#batch: " + str(len(XY)))
    # ret=Parallel(n_jobs=16)(delayed(f)(j) for j in range(len(XY)))
    p = Pool(16)
    params= list(enumerate([k] * len(XY)))
    print params
    ret = p.map(f, params)
    # p.join()
    p.close()

    b = np.concatenate(ret)
    df[colname]=b.astype(np.float32)
    logger.info("Finish calc...")
    logger.info("max,min=%f,%f", np.max(b), np.min(b))
    df = df[[colname]]
    outfile = os.path.join(config.get_state_year_folder(state, year), colname + '.parq')
    fastparquet.write(outfile, df, compression='SNAPPY')


def make_count_density():

    for state in config.STATES:
        for year in ['share']:
            task = Task('make_area_density50-{}-{}'.format(state, year), lambda u: _fun2_(state, year , u, k=50, colname='area_density50'))
            task()
            
    for state in config.STATES:
        for year in ['share']:
            task = Task('make_area_density100-{}-{}'.format(state, year), lambda u: _fun2_(state, year , u, k=100, colname='area_density100'))
            task()
    for state in config.STATES:
        for year in ['share']:
            task = Task('make_area_density1000-{}-{}'.format(state, year), lambda u: _fun2_(state, year , u, k=1000, colname='area_density1000'))
            task()


def main():
    make_count_density()        

                                    
if __name__ == '__main__':
    main()
