'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config, utils, features
from multiprocessing import Pool
import hashlib
from zillow.task import Task

 
def shell_run_and_wait(command):
    command = command.split(" ")
    import subprocess
    # process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process = subprocess.Popen(command)
    process.wait()
    return process.returncode


def run_single(cmd):

    def f(cmd, logger):
	logger.info(cmd)
        code = shell_run_and_wait(cmd)
        assert code == 0, (code, cmd)

    hashid = hashlib.sha1(cmd).hexdigest()
    task = Task('run_lgb_chain_4_state-{}'.format(hashid), lambda u: f(cmd, u))
    task()

    
def run_lgb_chain(logger):
    states=config.STATES
    if 0: #seq run 
        for state in states:
            cmd = 'python {}/python/step20_a_lgb_chain.py {}'.format(config.HOME, state)
            logger.info("cmd: %s", cmd)
            run_single(cmd)
    else:
        cmds = ['python {}/python/step20_a_lgb_chain.py {}'.format(config.HOME, state) for state in states]
        n_jobs=  len(states)
        logger.info("use %d jobs", n_jobs) 
        p = Pool(n_jobs)
        p.map(run_single, cmds)
        p.close()
        p.join()

       
def run():
    task = Task('run_lgb_chain-all', lambda u: run_lgb_chain(u))
    task()


if __name__ == '__main__':
        run()
