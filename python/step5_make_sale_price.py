'''
Created on Apr 6, 2018

@author: bo
'''
import dask.dataframe as dd 
import numpy as np 
import pandas as pd 
import os
import logging
import fastparquet
import gc
import cPickle as pickle 
import geopandas as gpd

from  zillow import config, utils, features, info
from zillow.task import Task


def make_sale_price_by_zip5():
    
    def _fun_(state, year, logger):        
        df = features.get_yb_sales(state, year, month_cut=None , hpi='zip5')
        logger.info("len(df)=%s\n%s", len(df), df.head())

        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        outfile = os.path.join(config.get_state_year_folder(state, year), 'sale_price_zip5.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_sale_price_by_zip5-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def make_sale_price_by_tract():
    
    def _fun_(state, year, logger):        
        df = features.get_yb_sales(state, year, month_cut=None , hpi='tract')
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        outfile = os.path.join(config.get_state_year_folder(state, year), 'sale_price_tract.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_sale_price_by_tract-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()


def make_sales_date():

    def _fun_(state, year, logger):
        filename = os.path.join(config.get_state_year_folder(state, year), "sales_price.parq")
        assert utils.file_exists(filename), filename 
        df = dd.read_parquet(filename, columns=['parcelid', 'inclusionruleidzestimate', 'recordingdate','saleprice']).compute()
        logger.info("len(df)=%s, vaid ratio=%s", len(df), (df['inclusionruleidzestimate'] > 0).mean())
        df = df[df['inclusionruleidzestimate'] > 0]
        df['year'] = df['recordingdate'].map(lambda u: u.year)
        assert len(set(df['year'])) == 1 and df.iloc[0]['year'] == year 
        df['month'] = df['recordingdate'].map(lambda u: u.year * 100 + u.month)
        df = df[['parcelid', 'year', 'month','saleprice']]
        df = df.groupby('parcelid').aggregate({'month':'mean', 'saleprice':'median'}).astype(np.float32)
	assert (df['saleprice']==0).sum()==0
        logger.info("len(df)=%s\n%s", len(df), df.head())
        
        outfile = os.path.join(config.get_state_year_folder(state, year), 'sale_date.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')

    for state in config.STATES:
        for year in config.YEARS:
            task = Task('make_sales_date-{}-{}'.format(state, year), lambda u: _fun_(state, year , u))
            task()

                                    
if __name__ == '__main__':
    if 1:     
        make_sales_date()   
        make_sale_price_by_zip5()
        make_sale_price_by_tract()        
        
