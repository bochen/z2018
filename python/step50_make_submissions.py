'''
Created on Apr 6, 2018

@author: bo
'''
from  zillow import config
import os
from zillow.task import Task

 
def shell_run_and_wait(command):
    command = command.split(" ")
    import subprocess
    # process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process = subprocess.Popen(command)
    process.wait()
    return process.returncode

    
def run_jupyter(fname, logger):
    fname = os.path.join(config.HOME, '3_jupyter', fname)
    assert os.path.exists(fname), fname
    cmd = "jupyter nbconvert --execute --to notebook {}".format(fname)
    logger.info("cmd: %s", cmd)
    code = shell_run_and_wait(cmd)
    assert code == 0, (fname, code)

       
def run():
    files = ['0_test_run_notebook.ipynb', '1_merge_finals.ipynb', '2_extrapolate_model_hpi_2018.ipynb', '3_make_prediction.ipynb']
    for fname in files:
        task = Task('run_jupyter_' + fname, lambda u: run_jupyter(fname, u))
        task()


if __name__ == '__main__':
        run()
