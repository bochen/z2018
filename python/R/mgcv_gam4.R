# TODO: Add comment
# 
# Author: bo
###############################################################################


library("optparse")
library("rlist")

printf <- function(...) invisible(print(sprintf(...)))


parse_arguments <- function() {
	option_list = list(
			make_option(c( "--year"), type="character", default=NULL, 
					help="year" ),
			make_option(c("--state"), type="character", default=NULL, 
					help="state" ),
			make_option(c("--basis"), type="character", default='cs', 
					help="state" ),
			
	make_option(c("--knot1"), type="integer", default=NULL, help="knot1" ),
	make_option(c("--knot2"), type="integer", default=NULL,	help="knot2" ),
	make_option(c("--knot3"), type="integer", default=NULL,	help="knot3" ),	
	make_option(c("--knot1b"), type="integer", default=NULL, help="knot1b" ),
	make_option(c("--knot2b"), type="integer", default=NULL,	help="knot2b" ),
	make_option(c("--knot3b"), type="integer", default=NULL,	help="knot3b" ),	
	make_option(c("--delta1"), type="double", default=0,	help="delta1" ),
	make_option(c("--delta2"), type="double", default=0,	help="delta2" ),
	make_option(c("--delta3"), type="double", default=0,	help="delta2" ),
	make_option(c("--nthread"), type="integer", default=NULL, 
			help="nthread" ),
	make_option(c("--nseed"), type="integer", default=NULL, 
			help="nseed" )
	); 
	
	
	opt_parser = OptionParser(option_list=option_list);
	opt = parse_args(opt_parser);
	if (  is.null(opt$state)|| is.null(opt$knot1)|| is.null(opt$knot2)
			|| is.null(opt$knot1b)|| is.null(opt$knot2b)){
		print_help(opt_parser)
		stop("not all required arguments have been supplied", call.=FALSE)
	}
	return (opt)
}

get_zillow_home <- function() {
	ZILLOW_HOME <- Sys.getenv('ZILLOW_HOME')
	if (ZILLOW_HOME==""){
		stop("$ZILLOW_HOME is not set", call.=FALSE)
	}

	use_condaenv("python27")
	sys <- import('sys')
	py_run_string(sprintf("sys.path.insert(0,'%s/python')", ZILLOW_HOME))
	
	return (ZILLOW_HOME)
}


opt=parse_arguments()
printf('arguments: %s',toString(opt))
print (opt)
year=opt$year 
state=opt$state
knot1=opt$knot1
knot2=opt$knot2
knot3=opt$knot3
knot1b=opt$knot1b
knot2b=opt$knot2b
knot3b=opt$knot3b
delta1=opt$delta1
delta2=opt$delta2
delta3=opt$delta3
basis=opt$basis

library('parallel')
library('mgcv')
require(caret)
library(reticulate)
use_condaenv("python27")

nthread=opt$nthread 
if (is.null(nthread)) {
	nthread = min(32,detectCores()-1)
}

nseed=opt$nseed


evaluate<-function(m3,test_data) {
	test_pred <- predict(m3,test_data)
	test_data[,'pred']=test_pred
	a <- abs(test_data[,'pred']-log(test_data[,'sale_price']))
	a[a>0.4]=0.4
	b <- mean(a,na.rm=TRUE)
	return(b)
}

make_knots=function(v,n,delta1){
	stopifnot(delta1>0)
	stopifnot(n>1)
	v <- v[!is.na(v)]
	v <-  as.integer(v/delta1)*delta1
	knots <- as.vector(quantile(unique(v), probs = seq(0, 1, by= 1.0/(n-1))))
	return (knots)
}


ZILLOW_HOME <- get_zillow_home()
print (sprintf("use %d threads", nthread))

feats <- import('zillow.features', 'feats')
rdf <- feats$get_features4(state,year,reset_index=TRUE, random_state=nseed)
rdf[is.na(rdf['lotsizesquarefeet']),'lotsizesquarefeet'] <- 1
rdf[is.na(rdf['finishedsquarefeet']),'finishedsquarefeet'] <- 1
rdf['no_garage']= !is.na(rdf['garagetotalsqft'])
rdf[is.na(rdf['garagetotalsqft']),'garagetotalsqft'] <- 1
rdf['lotsizesquarefeet'] <- log(rdf['lotsizesquarefeet'])
rdf['finishedsquarefeet'] <- log(rdf['finishedsquarefeet'])

rdf <- rdf[!is.na(rdf['longitude']),]
rdf <- rdf[!is.na(rdf['latitude']),]
if (state=='36'){
   rdf <- rdf[rdf$longitude>-80,]
}
rdf['longitude2'] <- rdf['longitude']
rdf['latitude2'] <- rdf['latitude']
rdf['longitude3'] <- rdf['longitude']
rdf['latitude3'] <- rdf['latitude']
#rdf['year'] <- lapply(rdf['year'],factor)
data <- subset(rdf, !is.nan(rdf[,'sale_price']))
y <- data[,'sale_price']

print (sprintf("start with df.shape %s and data.shape %s", toString(dim(rdf)), toString(dim(data)) ))

if (!is.null(nseed)) {
	set.seed(nseed)
}
train_data <-  data[data$fold!=1,]
test_data  <-  data[data$fold==1,]
print (sprintf("train/test: %s/%s", toString(dim(train_data)), toString(dim(test_data)) ))


k1=list()
if (delta1>0){
	k1 <- list.append(k1,
			longitude=make_knots(data$longitude, knot1,delta1), 
			latitude=make_knots(data$latitude,knot1,delta1),
			longitude2=make_knots(data$longitude2, knot2,delta1), 
			latitude2=make_knots(data$latitude2,knot2,delta1),
			longitude3=make_knots(data$longitude3, knot3,delta1), 
			latitude3=make_knots(data$latitude3,knot3,delta1)
			
	)
}
if (delta2>0){
	k1 <- list.append(k1,
			lotsizesquarefeet=make_knots(data$lotsizesquarefeet, knot1b,delta2), 
			finishedsquarefeet=make_knots(data$finishedsquarefeet, knot2b,delta2)
	)
}

if (delta3>0){
	k1 <- list.append(k1,
			year=make_knots(data$year, knot3b,delta3)
	)
}

printf("k1=%s", toString(k1))

timecost <- system.time(
		m3 <- bam(log(sale_price) ~ 1  
						+te(longitude,latitude,lotsizesquarefeet, bs=c(basis,basis,basis),k=c(knot1,knot1,knot1b)) 
						+te(longitude2,latitude2, finishedsquarefeet, bs=c(basis,basis,basis),k=c(knot2,knot2,knot2b)) 
						+te(longitude3,latitude3, year, bs=c(basis,basis,basis),k=c(knot3,knot3,knot3b)) 
						+						 log(garagetotalsqft) + no_garage, 
				knots=k1,
				discrete=TRUE,
				nthreads=nthread,
				, data=train_data)
)
printf("time cost: %s", toString(timecost))
print(timecost)

if (False){
	timecost <- system.time(
		print(summary(m3))
	)
	printf("time cost: %s", toString(timecost))
}

timecost <- system.time(
	print(sprintf("test score: %f, train score: %f",evaluate(m3,test_data),evaluate(m3,train_data)))
)
printf("time cost: %s", toString(timecost))


