'''
Created on Apr 6, 2018

@author: bo
'''
import numpy as np 
import os
import fastparquet
import geopandas as gpd
from shapely.geometry import Point

from  zillow import config, features
from zillow.task import Task

XY = None
tree = None 
gdf = None 


def f(jk):
    bj, k = jk
    print jk
    global XY, tree, df
    print ("begin batch " + str(bj))
    lst = XY[bj]
    _, ret = tree.query(lst, k=k)
    targets = []
    for i, l in enumerate(ret):
        point = Point(lst[i])
        findedIdx = None
        for j in l:
            poly = gdf.iloc[j]['geometry']
            if poly.contains(point):
                findedIdx = j
                break
        targets.append(findedIdx)
    print ("end batch " + str(bj))
    return targets 


def make_place_feat():

    def _fun_(state, logger):
        filename = os.path.join(config.INPUT_PATH, 'third', 'place', state)
        global XY, tree, gdf
        gdf = gpd.read_file(filename)
        gdf['INTPTLON'] = gdf['INTPTLON'].astype(np.float)
        gdf['INTPTLAT'] = gdf['INTPTLAT'].astype(np.float)
        gdfxy = gdf[['INTPTLON', 'INTPTLAT']].astype(np.float).values
        
        from scipy.spatial import cKDTree
        from multiprocessing import Pool

        df = features.get('longi_lati', state, 2017)

        tree = cKDTree(gdfxy)
        logger.info("start to calc..., #data %d", len(df))
        i = 0
        XY = []
        while i < df.shape[0]:
            XY.append(df.iloc[i:i + 20000].values)
            i += 20000
        logger.info("#batch: " + str(len(XY)))
        p = Pool(16)
        ret = p.map(f, enumerate([5] * len(XY)))
        # p.join()
        p.close()
    
        b = np.concatenate(ret)
        logger.info("Finish calc...")
                
        df['foundIdx'] = b
	def h(col):
		d=dict(enumerate(gdf[col]))
		return df['foundIdx'].map(d)
        df['place_x'] = h('INTPTLON') 
        df['place_y'] = h('INTPTLAT') 
        df['place_type'] = h('LSAD') 
        df['place_area'] = h('ALAND') 
        df = df[['place_x', 'place_y', 'place_type', 'place_area']]
        df['place_x-y'] = df['place_x'] - df['place_y']
        df['place_x+y'] = df['place_x'] + df['place_y']
        df = df.astype(np.float32)
        logger.info("\n%s", df.head())
        logger.info("\n%s", df.isnull().mean())
        
        outfile = os.path.join(config.get_state_year_folder(state, 'share'), 'tl_place.parq')
        fastparquet.write(outfile, df, compression='SNAPPY')
         
    for state in config.STATES:
        task = Task('make_place_feat-{}'.format(state), lambda u: _fun_(state, u))
        task()


if __name__ == '__main__':

    if 1:
        make_place_feat()        
